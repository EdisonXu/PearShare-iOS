//
//  UpdateSingalAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface UpdateSingalAPI : BaseRequestAPI
@property (nonatomic,strong)UIImage * fileData;
- (id)initWithImage:(UIImage *)image;
@end

//
//  BaseTableViewController.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController
-(void) picClick;
@end



#import "BaseRequestAPI.h"

@implementation BaseRequestAPI

-(instancetype)init{
    self = [super init];
    if (self) {
        self.isOpenAES = NO;
    }
    return self;
}

#pragma mark ————— 自定义数据 —————
- (NSString *)message {
    if (self.error) {
        return self.error.localizedDescription;
    }
    NSString *message = [NSString stringWithFormat:@"%@",[self.result stringForKey:@"errMsg"]];
    return message;
}
- (NSString *)code {
    NSString *code = [NSString stringWithFormat:@"%@",[self.result stringForKey:@"errCode"]];
    return code;
}

- (BOOL)isSuccess {
    BOOL isSuccess = [self.result boolForKey:@"success"];
    return  isSuccess;
}

#pragma mark ————— 定义返回数据格式，若是加密要用HTTP接受 —————
-(YTKResponseSerializerType)responseSerializerType {
    if (self.isOpenAES) {
        return YTKResponseSerializerTypeHTTP;
    }
    return YTKResponseSerializerTypeJSON;
}

#pragma mark ————— 默认请求方式 POST —————
-(YTKRequestMethod)requestMethod{
    return YTKRequestMethodPOST;
}

#pragma mark ————— 请求失败过滤器 —————
-(void)requestFailedFilter{
    //失败处理器
}

#pragma mark ————— 请求成功过滤器 —————
-(void)requestCompleteFilter{
        self.result = self.responseJSONObject;
}


#pragma mark ————— 非加密时也要传输的头部内容 也可能不需要，暂时保留 —————
-(NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary{
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    NSDictionary * userDic = (NSDictionary *)[cache objectForKey:KUserModelCache];
//    UserInfo*user = [UserInfo modelWithJSON:userDic];
    
    NSMutableDictionary * header = [NSMutableDictionary dictionary];
//    [header setString:user.token forKey:@"token"];
//    [header setString:user.userId forKey:@"userId"];
    
    return header;
}
@end

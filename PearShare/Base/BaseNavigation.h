//
//  BaseNavigation.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigation : UINavigationController


-(void)hideNavLine:(BOOL)hide;
@end

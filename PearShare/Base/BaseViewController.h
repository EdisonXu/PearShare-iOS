//
//  BaseViewController.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
-(NSMutableArray*)setModelList:(NSArray*)list;
@end

//
//  MainUITabBarController.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/10.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MainUITabBarController.h"
#import "HomeManager.h"
#import "BaseNavigation.h"
#import "UserCenter.h"
#import "DesireManager.h"
@interface MainUITabBarController ()

@end

@implementation MainUITabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTableBar];
}

-(void)setUpTableBar
{
    HomeManager *home = [[HomeManager alloc] init];
    BaseNavigation * homeNav = [[BaseNavigation alloc]initWithRootViewController:home];
    [homeNav hideNavLine:NO];
    homeNav.tabBarItem.image = [UIImage imageNamed:@"home_normal"];
    homeNav.tabBarItem.selectedImage =[UIImage imageNamed:@"home_select"];
    homeNav.tabBarItem.title = @"首页";
    
    UIStoryboard *deSB = [UIStoryboard storyboardWithName:@"DesireStoryboard" bundle:nil];
    DesireManager *desisre =[deSB instantiateViewControllerWithIdentifier:@"DesireManager"];
    BaseNavigation * desisreNav = [[BaseNavigation alloc]initWithRootViewController:desisre];
    desisreNav.tabBarItem.image = [UIImage imageNamed:@"desire_normal"];
    desisreNav.tabBarItem.selectedImage =[UIImage imageNamed:@"desire_select"];
    desisreNav.tabBarItem.title = @"想买";
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    UserCenter *user = [sb instantiateViewControllerWithIdentifier:@"UserCenter"];
    BaseNavigation * userNav = [[BaseNavigation alloc]initWithRootViewController:user];
    userNav.tabBarItem.image = [UIImage imageNamed:@"user_normal"];
    userNav.tabBarItem.selectedImage =[UIImage imageNamed:@"user_select"];
    userNav.tabBarItem.title = @"我";
    
    self.viewControllers = @[homeNav,desisreNav,userNav];
    [self configTabbarStyle];
}

- (void)configTabbarStyle {
    
    UITabBarController *tabController= self;
    UIView *bgView = [[UIView alloc] initWithFrame:tabController.tabBar.bounds];
    bgView.backgroundColor = [UIColor whiteColor];
    [tabController.tabBar insertSubview:bgView atIndex:0];
       tabController.tabBar.opaque = YES;
    
    //设置选中时的背景色
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithHexString:@"FED500"] } forState:UIControlStateSelected];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f] }
                                             forState:UIControlStateNormal];
    
    for (UITabBarItem *tbi in tabController.tabBar.items) {
        tbi.image = [tbi.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tbi.selectedImage = [tbi.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
}


@end

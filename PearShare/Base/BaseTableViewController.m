//
//  BaseTableViewController.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 点击头像
-(void) picClick{
    UIAlertController* actionSheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* weiboAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* vc = [UIImagePickerController new];
        vc.delegate = self;
        vc.allowsEditing = YES;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    UIAlertAction* weixinAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* vc = [UIImagePickerController new];
        vc.allowsEditing = YES;
        vc.delegate = self;
        // 选择照片数据源 --- 默认是相册
        vc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [actionSheetController addAction:weiboAction];
    [actionSheetController addAction:weixinAction];
    [actionSheetController addAction:cancelAction];
    
    [cancelAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [self presentViewController:actionSheetController animated:YES completion:nil];
}

@end

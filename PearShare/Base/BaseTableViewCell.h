//
//  BaseTableViewCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

- (NSString *)getDateStringWithTimeStr:(NSString *)str;

- (NSString *)formatFloat:(NSString*)price;

-(NSArray *)getNowTimeWithString:(NSString *)aTimeString;

-(NSInteger)getDayNumberWithYear:(NSInteger )y month:(NSInteger )m;
@end

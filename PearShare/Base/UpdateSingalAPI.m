//
//  UpdateSingalAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "UpdateSingalAPI.h"
#import "AFURLRequestSerialization.h"
@interface UpdateSingalAPI()
@property (nonatomic,strong)UIImage * image;
@end
@implementation UpdateSingalAPI
-(NSString *)requestUrl{
    return @"storge/uploadFile/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)initWithImage:(UIImage *)image {
    self = [super init];
    if (self) {
        _image = image;
    }
    return self;
}

- (AFConstructingBlock)constructingBodyBlock {
    return ^(id<AFMultipartFormData> formData) {
        NSData *data = UIImageJPEGRepresentation(_image, 0.9);
        NSString *name = @"file";
        NSString *formKey = @"file";
        NSString *type = @"image/jpeg";
        [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
    };
}
@end

//
//  BaseViewController.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewController.h"
#import "GoodsModel.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(NSMutableArray*)setModelList:(NSArray*)list
{
   NSMutableArray * modelList = [[NSMutableArray alloc] init];
    for (int i =0; i<list.count; i++) {
        GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[list dictionaryWithIndex:i] error:nil];
        [modelList addObject:model];
    }
    return modelList;
}

@end

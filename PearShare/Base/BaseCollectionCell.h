//
//  BaseCollectionCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/5/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCollectionCell : UICollectionViewCell
- (UIViewController *)getCurrentVC;

- (NSString *)formatFloat:(NSString*)price;

- (NSMutableAttributedString *)AttributedString:(NSString *)name content:(NSString *)content;
@end

//
//  MountingViewController.m
//  PearShare
//
//  Created by Destiny on 2018/7/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MountingViewController.h"
#import "ImageModel.h"
@interface MountingViewController ()

@end

@implementation MountingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (NSString *)formatFloat:(NSString*)price
{
    float f = [price floatValue];
    f = f/100;
    
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}


-(NSMutableArray*)setUpHeader:(NSDictionary*)data
{
    NSArray* imageUrlList = [NSArray array];
    if ([data hasKey:@"cover"]) {
         imageUrlList =[[data stringForKey:@"cover"] componentsSeparatedByString:@","];
    }else{
          imageUrlList =[[data stringForKey:@"imgs"] componentsSeparatedByString:@","];
    }
    
    NSMutableArray *imageSizeLsit = [NSMutableArray array];
    [imageUrlList enumerateObjectsUsingBlock:^(NSString* urlStr, NSUInteger idx, BOOL * _Nonnull stop) {
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]];
        UIImage *showimage = [UIImage imageWithData:data];
        NSString * str = [NSString stringWithFormat:@"%f%@%f",showimage.size.width,@",",showimage.size.height];
        [imageSizeLsit addString:str];
    }];
    
    NSMutableArray *imageModelArr = [NSMutableArray array];
    for (NSInteger i = 0; i < imageUrlList.count; i++) {
        ImageModel *model = [[ImageModel alloc] init];
        model.imgName = imageUrlList[i];
        model.imgSize = imageSizeLsit[i];
        [imageModelArr addObject:model];
    }
    return imageModelArr;
}


@end

//
//  BaseNavigation.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseNavigation.h"

@interface BaseNavigation ()

@end

@implementation BaseNavigation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count==1) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

-(void)hideNavLine:(BOOL)hide
{
    if (hide) {
        [self.navigationBar setShadowImage:[UIImage new]];
        [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    }
}

@end

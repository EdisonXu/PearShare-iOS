//
//  MountingViewController.h
//  PearShare
//
//  Created by Destiny on 2018/7/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MountingViewController : UIViewController
-(NSMutableArray*)setUpHeader:(NSDictionary*)data;
- (NSString *)formatFloat:(NSString*)price;
- (UIImage *)getScreenShotImageFromVideoURL:(NSString *)fileurl;
@end

//
//  BaseViewModel.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseViewModel : NSObject
@property (nonatomic,strong)NSMutableDictionary * param;
@property (nonatomic,assign) NSInteger        page;
@property (nonatomic,assign) NSString*        pageSize;
@property (nonatomic,strong)UIViewController * controller;


-(instancetype)initWith:(UIViewController*)controller;

- (UIViewController *)getCurrentVC;

-(void)addPage;
@end

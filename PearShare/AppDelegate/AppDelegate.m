//
//  AppDelegate.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AppDelegate.h"
#import "BaseNavigation.h"
#import "MallManager.h"
#import <UMCommon/UMCommon.h>
#import <UMPush/UMessage.h>
#import <UserNotifications/UserNotifications.h>
#import "FirstStartView.h"
#import <AlibcTradeBiz/AlibcTradeBiz.h>
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <AlibabaAuthSDK/albbsdk.h>
#import "NSString+StringCommon.h"
#import "MessageCenter.h"
#import "SureGuideView.h"
UIColor *MainNavBarColor = nil;
UIColor *MainViewColor = nil;
@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate

//com.PearShare.PomeloChoose

//com.lfxtd.lfx
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //初始化API
    [self NetWorkConfig];
    //初始化window
    [self initWindow];
    //友盟配置
    [self initUMeng];
    //生成项目管理器
    [PearShareManager sharedInstance];
    
    
    //导航栏配置

    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-1000, 0) forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithHexString:@"333333"]];
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithHexString:@"333333"], NSForegroundColorAttributeName, nil, NSFontAttributeName, nil]];

    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    
    [self setFristSatrt];
    
    if (kFirstStart) {
        [self guide];
        kUserDataRemove;
    }

    
    //友盟配置
    [UMConfigure setLogEnabled:true];
    
     [UMConfigure initWithAppkey:[NSString stringWithUTF8String:"5a5343d38f4a9d11ab0000a2"] channel: [NSString stringWithUTF8String:"App Store"]];
    
    
    //此函数在UMCommon.framework版本1.4.2及以上版本，在UMConfigure.h的头文件中加入。
    //如果用户用组件化SDK,需要升级最新的UMCommon.framework版本。
    NSString* deviceID =  [UMConfigure deviceIDForIntegration];
    NSLog(@"集成测试的deviceID:%@",deviceID);
    
    
   
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionSound|UMessageAuthorizationOptionAlert;
    if (@available(iOS 10.0, *)) {
        [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    } else {
        // Fallback on earlier versions
    }
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity     completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            
        }else{
            
        }
    }];
    
    // 百川平台基础SDK初始化，加载并初始化各个业务能力插件
    [[AlibcTradeSDK sharedInstance] asyncInitWithSuccess:^{

    } failure:^(NSError *error) {
        NSLog(@"Init failed: %@", error.description);
    }];

    // 开发阶段打开日志开关，方便排查错误信息
    //默认调试模式打开日志,release关闭,可以不调用下面的函数
    [[AlibcTradeSDK sharedInstance] setDebugLogOpen:YES];

    // 配置全局的淘客参数
    //如果没有阿里妈妈的淘客账号,setTaokeParams函数需要调用
    AlibcTradeTaokeParams *taokeParams = [[AlibcTradeTaokeParams alloc] init];
    taokeParams.pid = @"mm_XXXXX"; //mm_XXXXX为你自己申请的阿里妈妈淘客pid
    [[AlibcTradeSDK sharedInstance] setTaokeParams:taokeParams];

    //设置全局的app标识，在电商模块里等同于isv_code
    //没有申请过isv_code的接入方,默认不需要调用该函数
    [[AlibcTradeSDK sharedInstance] setISVCode:@"your_isv_code"];

    // 设置全局配置，是否强制使用h5
    [[AlibcTradeSDK sharedInstance] setIsForceH5:NO];
    return YES;
}


-(void)guide
{
    FirstStartView * fs = [[NSBundle mainBundle] loadNibNamed:@"FirstStartView" owner:self options:nil][0];
    UIWindow * win = [[UIApplication sharedApplication].delegate window];
    [win addSubview:fs];
    [fs mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(win);
    }];
    
    //    [kDefaults setObject:@NO forKey:kFirstStart];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    // 如果百川处理过会返回YES
    if (![[AlibcTradeSDK sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) {
        // 处理其他app跳转到自己的app
    }
    return YES;
}

-(void)setFristSatrt
{
    NSString * Version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString * appv = [kDefaults stringForKey:kAPPVersion];
    if ([NSString isBlankString:appv]) {
        kFirstStartSet(YES);
    }else{
        if (![appv isEqualToString:Version]) {
            kFirstStartSet(YES);
            
        }else{
            kFirstStartSet(NO) ;
        }
    }
    [kDefaults setObject:Version forKey:kAPPVersion];
}


//IOS9.0 系统新的处理openURL 的API
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {


    __unused BOOL isHandledByALBBSDK=[[AlibcTradeSDK sharedInstance] application:application openURL:url options:options];//处理其他app跳转到自己的app，如果百川处理过会返回YES

    return YES;


}



//iOS10以下使用这两个方法接收通知，
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [UMessage setAutoAlert:NO];
    if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
        [UMessage didReceiveRemoteNotification:userInfo];
        completionHandler(UIBackgroundFetchResultNewData);

    }
}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler API_AVAILABLE(ios(10.0)){
    NSDictionary * userInfo = notification.request.content.userInfo;
    if (@available(iOS 10.0, *)) {
        if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            //应用处于前台时的远程推送接受
            //关闭U-Push自带的弹出框
            [UMessage setAutoAlert:NO];
            //必须加这句代码
            [UMessage didReceiveRemoteNotification:userInfo];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
            MessageCenter *vc = [sb instantiateViewControllerWithIdentifier:@"MessageCenter"];
            UITabBarController *tab =(UITabBarController *) [UIApplication sharedApplication].windows.firstObject.rootViewController;
            UINavigationController * nav = tab.selectedViewController;
            [nav pushViewController:vc animated:YES];
        }else{
            //应用处于前台时的本地推送接受
        }
    } else {
        // Fallback on earlier versions
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    if (@available(iOS 10.0, *)) {
        completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
    } else {
        // Fallback on earlier versions
    }
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler API_AVAILABLE(ios(10.0)){
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if (@available(iOS 10.0, *)) {
        if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
            //应用处于后台时的远程推送接受
            //必须加这句代码
            [UMessage didReceiveRemoteNotification:userInfo];
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
            MessageCenter *vc = [sb instantiateViewControllerWithIdentifier:@"MessageCenter"];
            UITabBarController *tab =(UITabBarController *) [UIApplication sharedApplication].windows.firstObject.rootViewController;
            UINavigationController * nav = tab.selectedViewController;
            [nav pushViewController:vc animated:YES];
            
        }else{
            //应用处于后台时的本地推送接受
        }
    } else {
        // Fallback on earlier versions
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString * pushToken = [NSString stringWithFormat:@"%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                                              stringByReplacingOccurrencesOfString: @">" withString: @""]
                                                             stringByReplacingOccurrencesOfString: @" " withString: @""]];
    NSLog(@"%@",pushToken);
    if (pushToken.length>0) {
        [kDefaults setObject:pushToken forKey:@"pushToken"];
    }
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end

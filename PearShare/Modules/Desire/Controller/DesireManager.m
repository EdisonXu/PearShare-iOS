//
//  DesireManager.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/5/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DesireManager.h"
#import "LoginVC.h"
#import "DesireCell.h"
#import "DesireShareCell.h"
#import "BannerCell.h"
#import "DesireEmptyCell.h"
#import "DesireVM.h"
#import "GoodsDetail.h"
#import "ArticleDetail.h"
#import "FavoriteAPI.h"
#import "DesireManagerHeader.h"
#import "DetailMangager.h"
@interface DesireManager ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)DesireVM * viewModel;
@property (nonatomic,strong)NSMutableArray * desireList;
@property (nonatomic,strong)NSMutableArray * bannerList;
@property(nonatomic, strong) NSMutableArray *deleteArr;//删除数据的数组
@property(nonatomic, strong) NSMutableArray *markArr;//标记数据的数组
@property(nonatomic, strong) NSMutableArray *selectedRows;
@property (nonatomic,assign) CGFloat managerHeight;
@end

@implementation DesireManager

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"想买";
    self.managerHeight = 0;
    self.viewModel = [[DesireVM alloc] init];
    self.deleteArr = [NSMutableArray array];
    self.markArr = [NSMutableArray array];
    [self setUpTable];
    [MobClick event:@"0002"];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (kDefaultLoginAlready) {
        [self setUpData];
        [self setUpBanner];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        vc.form = @"desire";
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.navigationController presentViewController:navi animated:NO completion:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.tableView.editing = NO;
}

-(void)setUpTable
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.editing = NO;
    self.tableView.tableFooterView = [[UIView alloc] init];
    /** 注册cell可重用ID */
    [self.tableView registerNib:DesireCell.nib forCellReuseIdentifier:@"DesireCell"];
    [self.tableView registerNib:BannerCell.nib forCellReuseIdentifier:@"BannerCell"];
    [self.tableView registerNib:DesireEmptyCell.nib forCellReuseIdentifier:@"DesireEmptyCell"];
    [self.tableView registerNib:DesireShareCell.nib forCellReuseIdentifier:@"DesireShareCell"];
    

    
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(setUpData)];
    
    // 设置header
    self.tableView.mj_header = header;
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.hidden = YES;
    
    self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
//        @strongify(self);
        [self moreData];
    }];
    
}

-(void)setUpData
{
        self.viewModel.page= 1;
        __weak typeof(self) weakSelf = self;
        RACSignal * signal = [self.viewModel DesireCommand];
        [signal subscribeNext:^(NSArray * list) {
            weakSelf.desireList =[[NSMutableArray arrayWithArray:list] mutableCopy];
            if (weakSelf.desireList.count) {
                weakSelf.managerHeight= 40;
            }else{
               weakSelf.managerHeight= 0;
            }
            [self.tableView.mj_header endRefreshing];
            [self.tableView reloadData];
        }];
}

-(void)moreData
{
    [self.viewModel addPage];
    RACSignal * signal = [self.viewModel DesireCommand];
    [signal subscribeNext:^(NSArray * list) {
        [self.tableView.mj_footer endRefreshing];
        NSMutableArray * moreList =  [[NSMutableArray alloc] initWithArray:list];
        if (moreList.count>0) {
            [self.desireList addObjectsFromArray:moreList];
            [self.tableView reloadData];
        }
    }];
}

-(void)setUpBanner
{
    RACSignal * bansignal = [self.viewModel BannerCommand];
    [bansignal subscribeNext:^(NSArray * list) {
        self.bannerList =[[NSMutableArray arrayWithArray:list] mutableCopy];
        [self.tableView reloadData];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return 1;
    }else{
        return self.desireList.count?self.desireList.count:1;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        BannerCell *cell =[tableView dequeueReusableCellWithIdentifier:@"BannerCell" forIndexPath:indexPath];
        cell.bannerList = self.bannerList;
        return cell;
    }else{
        if (self.desireList.count) {
            if ([[self.desireList dictionaryWithIndex:indexPath.row] boolForKey:@"groupStatus"]) {
                DesireCell *cell =[tableView dequeueReusableCellWithIdentifier:@"DesireCell" forIndexPath:indexPath];
                cell.model = [self.desireList dictionaryWithIndex:indexPath.row];
                return cell;
            }else{
                DesireShareCell *cell =[tableView dequeueReusableCellWithIdentifier:@"DesireShareCell" forIndexPath:indexPath];
                cell.model = [self.desireList dictionaryWithIndex:indexPath.row];
                return cell;
            }
        }else{
            DesireEmptyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"DesireEmptyCell" forIndexPath:indexPath];
            return cell;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 160;
    }else{
        return self.desireList.count?160:self.tableView.bounds.size.height-160;
    }
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        DesireManagerHeader * header = [[NSBundle mainBundle] loadNibNamed: @"DesireManagerHeader" owner:self options:nil].firstObject;
        header.managerBlcok = ^(BOOL select){
            [self manager];
        };
        
        header.allBlcok = ^(BOOL select){
            [self allcommand:select];
        };
        
        header.deleteBlcok = ^(BOOL select){
            [self deleCommand];
        };
        return header;
    }else{
        return  nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return self.managerHeight;
    }else{
        return 0;
    }
}

-(void)manager
{
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.tableView.editing = !self.tableView.editing;
    if (self.tableView.editing) {
        self.managerHeight = 80;
    }else{
        self.managerHeight = 40;
    }
    
    [self.tableView reloadData];
}

- (void)allcommand:(BOOL)select {

    if (select) {
        for (int i = 0; i < self.desireList.count; i ++) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:1];
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        }
        [self.deleteArr addObjectsFromArray:self.desireList];
        NSLog(@"self.deleteArr:%@", self.deleteArr);
    }else{
        for (int i = 0; i < self.desireList.count; i ++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:1];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        [self.deleteArr removeAllObjects];
    }
}


- (void)deleCommand {
    if (self.tableView.editing&&self.deleteArr.count) {
        //删除
        NSString * title =[NSString stringWithFormat:@"是否删除"];
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * signAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self delectFav:self.deleteArr];
            self.tableView.editing = NO;
        }];
        UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:signAction];
        [alertController addAction:cancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else return;
}

//是否可以编辑  默认的时YES
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        return NO;
    }
    return YES;
}

//选择编辑的方式,按照选择的方式对表进行处理
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray * list = [NSMutableArray arrayWithObject:[self.desireList objectAtIndex:indexPath.row]];
        [self delectFav:list];
    }
}
//选择你要对表进行处理的方式  默认是删除方式
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return  UITableViewCellEditingStyleDelete ;
}



//选中时将选中行的在self.dataArray 中的数据添加到删除数组self.deleteArr中
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.desireList.count) {
        if (self.tableView.editing) {
            [self.deleteArr addObject:[self.desireList objectAtIndex:indexPath.row]];
        }else{
            if (indexPath.section==1) {
                if ([[self.desireList dictionaryWithIndex:indexPath.row] boolForKey:@"groupStatus"]) {
                    DetailMangager* vc = [[DetailMangager alloc] init];
                    vc.shareId =[[self.desireList dictionaryWithIndex:indexPath.row] stringForKey:@"articleId"];
                    vc.goodsId =[[self.desireList dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                    vc.type = @"2";
                    [self.navigationController pushViewController:vc animated:YES];
                }else{
                    DetailMangager* vc = [[DetailMangager alloc] init];
                    vc.shareId =[[self.desireList dictionaryWithIndex:indexPath.row] stringForKey:@"articleId"];
                    vc.goodsId =[[self.desireList dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                    vc.type = @"1";
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            }
        }
    }
}
//取消选中时 将存放在self.deleteArr中的数据移除
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath  {
    [self.deleteArr removeObject:[self.desireList objectAtIndex:indexPath.row]];
}


-(void)delectFav:(NSArray*)list
{
    NSMutableArray * array =[NSMutableArray array];
    __weak typeof(self) weakSelf = self;
    [list enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [array addObject:[obj stringForKey:@"articleId"]];
    }];
    
    FavoriteAPI *req = [FavoriteAPI new];
    req.collectionType =   @"2";
    req.articleIdList =[array componentsJoinedByString:@","];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [req startWithCompletionBlockWithSuccess:^(FavoriteAPI* request) {
         [hud hideAnimated:YES];
        if ([request isSuccess]) {
            [weakSelf setUpData];
            [self.desireList removeObjectsInArray:self.deleteArr];
            [weakSelf.deleteArr removeAllObjects];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [hud hideAnimated:YES];
    }];
}

-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath{
    return @"删除";
}



@end

//
//  BannerAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BannerAPI.h"

@implementation BannerAPI
-(NSString *)requestUrl{
    return  @"article/getAppBanner";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end

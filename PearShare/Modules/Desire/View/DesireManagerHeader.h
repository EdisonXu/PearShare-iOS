//
//  DesireManagerHeader.h
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DesireManagerHeader : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnManager;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnAll;

typedef void(^ManagerBlcok) (BOOL select);

@property (nonatomic,copy)ManagerBlcok managerBlcok;//管理block

typedef void(^AllBlcok) (BOOL select);

@property (nonatomic,copy)AllBlcok allBlcok;//管理block

typedef void(^DeleteBlcok) (BOOL select);

@property (nonatomic,copy)DeleteBlcok deleteBlcok;//管理block
@end

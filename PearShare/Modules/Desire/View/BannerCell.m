//
//  BannerCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BannerCell.h"

@implementation BannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setBannerList:(NSArray *)bannerList
{
    NSMutableArray * list =[NSMutableArray array];
    
    [bannerList enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [list addObject:[obj stringForKey:@"image"]];
    }];
            self.bannerView.placeholderImage = [UIImage new];
            self.bannerView .imageURLStringsGroup = list;
            self.bannerView.showPageControl = NO;
}
@end

//
//  DesireVM.m
//  PearShare
//
//  Created by Destiny on 2018/7/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DesireVM.h"
#import "DesireAPI.h"
#import "BannerAPI.h"
@implementation DesireVM
-(RACSignal*)DesireCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        DesireAPI *req = [DesireAPI new];
        req.pageSize =self.pageSize;
        req.page =self.page;
        
        [req startWithCompletionBlockWithSuccess:^(DesireAPI * request) {
            [subscriber sendNext:[request.result arrayForKey:@"data"]];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)BannerCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        BannerAPI *req = [BannerAPI new];
        
        [req startWithCompletionBlockWithSuccess:^(BannerAPI * request) {
            [subscriber sendNext:[request.result arrayForKey:@"data"]];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

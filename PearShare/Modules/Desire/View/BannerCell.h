//
//  BannerCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "SDCycleScrollView.h"
@interface BannerCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *bannerView;
@property (nonatomic,strong)NSArray* bannerList;
@end

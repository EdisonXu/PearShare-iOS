//
//  DesireShareCell.m
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DesireShareCell.h"

@implementation DesireShareCell

- (NSMutableAttributedString *)AttributedString:(NSString *)name content:(NSString *)content
{
    // 富文本技术：
    // 1.图文混排
    // 2.随意修改文字样式
    
    //拿到整体的字符串
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",name,content]];
    
    // 创建图片图片附件
    NSTextAttachment *attach = [[NSTextAttachment alloc] init];
    attach.image = [UIImage imageNamed:@"begin_sale"];
    attach.bounds = CGRectMake(0, -2, 30, 15);
    NSAttributedString *attachString = [NSAttributedString attributedStringWithAttachment:attach];
    //将图片插入到合适的位置
    [string insertAttributedString:attachString atIndex:0];
    return string;
}


-(void)setModel:(NSDictionary *)model
{
    self.lblName.text = [model stringForKey:@"nickname"];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"avatar"]]];
    self.lblTime.text =[NSString stringWithFormat:@"%@",[self getDateStringWithTimeStr:[model stringForKey:@"createTime"]]];
    self.lblTitle.text = [model stringForKey:@"title"];
    [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"cover"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    self.lblCount.text =[NSString stringWithFormat:@"%@",[model stringForKey:@"collectionsNum"]];

}

- (void)awakeFromNib {
    
    // 这三句代码可以代替- (void)setSelected:(BOOL)selected animated:(BOOL)animated
    UIView *view = [[UIView alloc] initWithFrame:self.multipleSelectionBackgroundView.bounds];
    view.backgroundColor = [UIColor whiteColor];
    self.selectedBackgroundView = view;
    // 这个属性是编辑的时候最右边的accessory样式
    //    self.editingAccessoryType = UITableViewCellAccessoryCheckmark;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if (self.editing) {
        if (selected) {
            // 取消多选时cell成蓝色
            //            self.contentView.backgroundColor = [UIColor whiteColor];
            //            self.backgroundView.backgroundColor = [UIColor whiteColor];
            
        }else{
            
        }
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    if (editing) {
        for (UIControl *control in self.subviews){
            if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
                for (UIView *v in control.subviews)
                {
                    if ([v isKindOfClass: [UIImageView class]]) {
                        UIImageView *img=(UIImageView *)v;
                        
                        img.image = [UIImage imageNamed:@"未选中"];
                    }
                }
            }
        }
    }
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    for (UIControl *control in self.subviews){
        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
            for (UIView *v in control.subviews)
            {
                if ([v isKindOfClass: [UIImageView class]]) {
                    UIImageView *img=(UIImageView *)v;
                    
                    if (self.selected) {
                        img.image=[UIImage imageNamed:@"选中"];
                    }else
                    {
                        img.image=[UIImage imageNamed:@"未选中"];
                    }
                }
            }
        }
    }
    
}


@end

//
//  DesireVM.h
//  PearShare
//
//  Created by Destiny on 2018/7/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface DesireVM : BaseViewModel
-(RACSignal*)DesireCommand;

-(RACSignal*)BannerCommand;
@end

//
//  DesireAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface DesireAPI : BaseRequestAPI
@property (nonatomic,assign) NSInteger        page;
@property (nonatomic,assign) NSString*        pageSize;
@end

//
//  DesireManagerHeader.m
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DesireManagerHeader.h"

@implementation DesireManagerHeader

- (IBAction)managerCommand:(id)sender
{
    self.btnManager.selected=!self.btnManager.selected;
    self.managerBlcok(self.btnManager.selected);
}
- (IBAction)deleteCommand:(id)sender
{
    self.btnDelete.selected=!self.btnDelete.selected;
    self.deleteBlcok(self.btnDelete.selected);
}
- (IBAction)allCommand:(id)sender
{
    self.btnAll.selected=!self.btnAll.selected;
    self.allBlcok(self.btnAll.selected);
}
@end

//
//  DesireAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DesireAPI.h"

@implementation DesireAPI
-(NSString *)requestUrl{
    return  @"article/findCollection";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.pageSize forKey:@"pageSize"];
    [param setString:[NSString stringWithFormat:@"%ld",self.page] forKey:@"pageNum"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}

-(NSString*)pageSize
{
    if (!_pageSize) {
        _pageSize = [NSString stringWithFormat:@"20"];
    }
    return _pageSize;
}


-(NSInteger)page
{
    if (!_page) {
        _page =1;
    }
    return _page;
}

-(void)addPage
{
    self.page = ++self.page;
}
@end

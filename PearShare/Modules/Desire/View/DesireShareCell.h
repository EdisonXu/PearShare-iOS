//
//  DesireShareCell.h
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DesireShareCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@end

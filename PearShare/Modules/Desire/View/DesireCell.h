//
//  DesireCell.h
//  PearShare
//
//  Created by Destiny on 2018/5/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
#import "UICustomLineLabel.h"
@interface DesireCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnWant;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblOld;
@end

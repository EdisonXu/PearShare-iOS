//
//  SingleCommentAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SingleCommentAPI.h"

@implementation SingleCommentAPI
-(NSString *)requestUrl{
    return  @"comm/comment";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:self.commentId forKey:@"commentId"];
    [param setString:self.pageSize forKey:@"pageSize"];
    [param setString:[NSString stringWithFormat:@"%ld",self.page] forKey:@"pageNum"];
    return param;
}

-(NSString*)pageSize
{
    if (!_pageSize) {
        _pageSize = [NSString stringWithFormat:@"20"];
    }
    return _pageSize;
}


-(NSInteger)page
{
    if (!_page) {
        _page =1;
    }
    return _page;
}

-(void)addPage
{
    self.page = ++self.page;
}
@end

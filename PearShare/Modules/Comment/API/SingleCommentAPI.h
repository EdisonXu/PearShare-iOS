//
//  SingleCommentAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface SingleCommentAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * commentId;
@property (nonatomic,assign) NSInteger        page;
@property (nonatomic,assign) NSString*        pageSize;

-(void)addPage;
@end

//
//  CommentAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CommentAPI.h"

@implementation CommentAPI
-(NSString *)requestUrl{
    return  @"comm/list";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.shareId forKey:@"shareId"];
    if (kDefaultLoginAlready) {
        [param setString:kDefaultUserId forKey:@"userId"];
    }
    [param setString:self.type forKey:@"type"];
    if ([self.type isEqualToString:@"0"]) {
        [param setString:@"20" forKey:@"pageSize"];
        [param setString:[NSString stringWithFormat:@"%ld",(long)self.pageNum] forKey:@"pageNum"];
    }
    NSLog(@"%@",kDefaultUserId);
    return param;
}


@end

//
//  CommentAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface CommentAPI : BaseRequestAPI
@property (nonatomic,strong)NSString* type;
@property (nonatomic,strong)NSString* shareId;
@property (nonatomic,assign)NSInteger pageNum;


@end

//
//  SendReplayAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface SendReplayAPI : BaseRequestAPI
@property (nonatomic,strong)NSString* type;
@property (nonatomic,assign)NSString* targetId;
@property (nonatomic,strong)NSString* toUserId;
@property (nonatomic,assign)NSString* comment;
@property (nonatomic,assign)NSString* commentId;
@end

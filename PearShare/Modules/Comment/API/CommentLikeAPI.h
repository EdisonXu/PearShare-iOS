//
//  CommentLikeAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface CommentLikeAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * action;
@property (nonatomic,strong)NSString * type;
@property (nonatomic,strong)NSString * targetId;
@end

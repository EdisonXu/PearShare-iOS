//
//  SendCommentAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface SendCommentAPI : BaseRequestAPI
@property (nonatomic,strong)NSString* shareId;
@property (nonatomic,assign)NSString* comment;
@end

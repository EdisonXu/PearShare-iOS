
//
//  SendCommentAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SendCommentAPI.h"

@implementation SendCommentAPI
-(NSString *)requestUrl{
    return @"comm/publishComment";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.shareId forKey:@"shareId"];
    [param setString:self.comment forKey:@"comment"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

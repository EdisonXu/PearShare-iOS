//
//  SendReplayAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SendReplayAPI.h"

@implementation SendReplayAPI
-(NSString *)requestUrl{
    return @"comm/reply";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.type forKey:@"type"];
    [param setString:self.targetId forKey:@"targetId"];
    [param setString:self.toUserId forKey:@"toUserId"];
    [param setString:self.comment forKey:@"comment"];
    [param setString:self.commentId forKey:@"commentId"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

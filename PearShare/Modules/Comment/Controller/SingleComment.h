//
//  SingleComment.h
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSTextView.h"
@interface SingleComment : UIViewController
@property (nonatomic,strong)NSString * commentId;
@property (nonatomic,strong)NSString * shareId;
@property (nonatomic,assign)NSInteger  type;
@property (weak, nonatomic) IBOutlet WSTextView *commentText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottom;
@end

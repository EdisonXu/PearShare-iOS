//
//  SingleComment.m
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SingleComment.h"
#import "SingleCommentAPI.h"
#import "ReplyTopCell.h"
#import "CommentReplyCell.h"
#import "SendReplayAPI.h"
@interface SingleComment ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    NSUInteger _page;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray* commendList;
@property (nonatomic,strong)NSMutableArray* replyList;
@property (nonatomic,strong)NSDictionary *data;
@property (nonatomic,strong)NSDictionary *replyData;
@end

@implementation SingleComment

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpView];
    [self setUpdata];
    if (self.type==1) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.commentText becomeFirstResponder];
        });
    }
}

#pragma mark_ 配置初始化页面
-(void)setUpView
{
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    [self.tableView registerNib:ReplyTopCell.nib forCellReuseIdentifier:@"ReplyTopCell"];
    [self.tableView registerNib:CommentReplyCell.nib forCellReuseIdentifier:@"CommentReplyCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    @weakify(self);
    self.tableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpdata];
    }];

    self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self moreData];
    }];
    
//    self.commentText.returnKeyType = UIReturnKeySend;//变为搜索按钮
    self.commentText.delegate = self;//设置代理
    
    //评论键盘
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = NO;
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    //设置最大行数
    self.commentText.maxNumberOfLines = 4;
    //设置提示内容
    self.commentText.placeHolder = @"说点什么吧～";
    self.commentText.placeHolderColor = [UIColor lightGrayColor];
    //高度改变的回调
    __weak typeof(self) wself = self;
    self.commentText.ws_textHeightChangeHandle = ^(NSString *text, CGFloat height){
        //20是textView上下边距的和
        wself.bottomViewH.constant = height + 16;
        [UIView animateWithDuration:0.25 animations:^{
            [wself.view layoutIfNeeded];
        }];
    };
}


-(void)setUpdata
{
    SingleCommentAPI *api = [SingleCommentAPI new];
    api.commentId = self.commentId;
    _page = 1;
    api.page = _page;
    [api startWithCompletionBlockWithSuccess:^(SingleCommentAPI * request) {
        [self.tableView.mj_header endRefreshing];
        if ([api isSuccess]) {
            self.data = [NSDictionary dictionaryWithDictionary:[[[request.result dictionaryForKey:@"data"]arrayForKey:@"commentDto"]dictionaryWithIndex:0]];
            self.commendList = [[NSMutableArray arrayWithArray:[[request.result dictionaryForKey:@"data"]arrayForKey: @"commentDto"]] mutableCopy];
            self.replyList = [[NSMutableArray arrayWithArray:[[self.commendList dictionaryWithIndex:0]arrayForKey:@"commentReplys"]] mutableCopy];
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header endRefreshing];
    }];
}

-(void)moreData
{
    SingleCommentAPI *api = [SingleCommentAPI new];
    _page = _page+1;
    api.page = _page;
    api.commentId = self.commentId;
    [api startWithCompletionBlockWithSuccess:^(SingleCommentAPI * request) {
        [self.tableView.mj_footer endRefreshing];
        if ([api isSuccess]) {
            NSMutableArray* list = [[NSMutableArray arrayWithArray:[[request.result dictionaryForKey:@"data"]arrayForKey: @"commentDto"]] mutableCopy];
            NSMutableArray * moreList =[[NSMutableArray arrayWithArray:[[list dictionaryWithIndex:0]arrayForKey:@"commentReplys"]] mutableCopy];
            if (moreList.count>0) {
                [self.replyList addObjectsFromArray:moreList];
                [self.tableView reloadData];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_footer endRefreshing];
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self.commendList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.replyList.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0) {
        ReplyTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReplyTopCell" forIndexPath:indexPath];
        cell.model = [self.commendList objectAtIndex:indexPath.section];
        return cell;
    }else{
        CommentReplyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentReplyCell" forIndexPath:indexPath];
        cell.model = [self.replyList dictionaryWithIndex:indexPath.row-1];
        __weak typeof(self) weakSelf = self;
        cell.sendBlock= ^(NSDictionary *reply) {
            weakSelf.type = 2;
            weakSelf.replyData = [NSDictionary dictionaryWithDictionary:reply];
            [weakSelf.commentText becomeFirstResponder];
        };
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

#pragma mark - textView data source
- (void)keyboardChange:(NSNotification *)note {
    CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    self.bottomViewBottom.constant = endFrame.origin.y != screenH ? endFrame.size.height:0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{

    return YES;
}

- (IBAction)send:(id)sender
{
    if (self.commentText.text.length) {
        if (self.type==1) {
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            __weak typeof(self) wself = self;
            wself.bottomViewH.constant =50;
            [UIView animateWithDuration:0.25 animations:^{
                [wself.view layoutIfNeeded];
            }];
            
            SendReplayAPI * api = [SendReplayAPI new];
            api.type = [NSString stringWithFormat:@"%ld",(long)self.type];
            api.targetId =[self.data stringForKey:@"commentId"];
            api.commentId =[self.data stringForKey:@"commentId"];
            api.comment = self.commentText.text;
            
            [api startWithCompletionBlockWithSuccess:^(SendReplayAPI * request) {
                wself.commentText.text = nil;
                [wself.tableView.mj_header beginRefreshing];
            } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                
            }];
        }
        
        if (self.type==2) {
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            __weak typeof(self) wself = self;
            wself.bottomViewH.constant =50;
            [UIView animateWithDuration:0.25 animations:^{
                [wself.view layoutIfNeeded];
            }];
            
            SendReplayAPI * api = [SendReplayAPI new];
            api.type = [NSString stringWithFormat:@"%ld",(long)self.type];
            api.targetId =[self.replyData stringForKey:@"replyId"];
            api.toUserId = [self.replyData stringForKey:@"fromUserId"];
            api.commentId =[self.data stringForKey:@"commentId"];
            api.comment = self.commentText.text;
            
            [api startWithCompletionBlockWithSuccess:^(SendReplayAPI * request) {
                 wself.commentText.text = nil;
                 [wself.tableView.mj_header beginRefreshing];
            } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                
            }];
        }
    }
}
@end

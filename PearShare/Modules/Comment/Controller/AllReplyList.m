//
//  AllReplyList.m
//  PearShare
//
//  Created by Destiny on 2018/7/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AllReplyList.h"
#import "SingleCommentAPI.h"
#import "CommentAPI.h"
#import "CommentTopCell.h"
#import "CommentSonCell.h"
@interface AllReplyList ()<UITableViewDelegate,UITableViewDataSource>
{
    NSUInteger _page;
}
@property (nonatomic,strong)NSMutableArray* commendList;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textHeight;
@end
@implementation AllReplyList

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self.tableView.mj_header beginRefreshing];
    
}

#pragma mark_ 配置初始化页面
-(void)setUpView
{
    _page = 1;
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    [self.tableView registerNib:CommentTopCell.nib forCellReuseIdentifier:@"CommentTopCell"];
    [self.tableView registerNib:CommentSonCell.nib forCellReuseIdentifier:@"CommentSonCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    @weakify(self);
    self.tableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpAllCommenlist];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self moreData];
    }];
}


-(void)setUpAllCommenlist;
{
    CommentAPI *req = [CommentAPI new];
    req.shareId = self.shareId;
    req.type = @"0";
    _page = 1;
    req.pageNum  =_page;
    
    [req startWithCompletionBlockWithSuccess:^( CommentAPI*  request) {
        [self.tableView.mj_header endRefreshing];
        if ([request isSuccess]) {
            self.commendList = [[NSMutableArray arrayWithArray:[[request.result dictionaryForKey:@"data"]arrayForKey: @"commentDto"]] mutableCopy];
            self.title = [NSString stringWithFormat:@"%lu%@",(unsigned long)self.commendList.count,@"条评论"];
            [self.tableView reloadData];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header endRefreshing];
    }];
}

-(void)moreData
{
    CommentAPI *req = [CommentAPI new];
    req.shareId = self.shareId;
    req.type = @"0";
    _page = _page+1;
     req.pageNum  =_page;
    
    [req startWithCompletionBlockWithSuccess:^( CommentAPI*  request) {
        [self.tableView.mj_footer endRefreshing];
        if ([request isSuccess]) {
            NSMutableArray * moreList = [[NSMutableArray arrayWithArray:[[request.result dictionaryForKey:@"data"]arrayForKey: @"commentDto"]] mutableCopy];
            if (moreList.count>0) {
                [self.commendList addObjectsFromArray:moreList];
                [self.tableView reloadData];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_footer endRefreshing];
    }];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self.commendList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([[self.commendList dictionaryWithIndex:section] arrayForKey:@"commentReplys"].count) {
        return 2;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0) {
        CommentTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTopCell" forIndexPath:indexPath];
        cell.model = [self.commendList objectAtIndex:indexPath.section];
        return cell;
    }else{
        CommentSonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentSonCell" forIndexPath:indexPath];
        cell.model = [[[self.commendList dictionaryWithIndex:indexPath.section]arrayForKey:@"commentReplys"] dictionaryWithIndex:0];
        cell.replyCount = [NSString stringWithFormat:@"%ld",[[self.commendList dictionaryWithIndex:indexPath.section]arrayForKey:@"commentReplys"].count];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return UITableViewAutomaticDimension;
}
@end

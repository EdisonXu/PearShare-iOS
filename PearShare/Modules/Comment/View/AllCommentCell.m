//
//  AllCommentCell.m
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AllCommentCell.h"

@implementation AllCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setTotal:(NSInteger)total
{
    if (total>0) {
        self.lblTotal.text = [NSString stringWithFormat:@"%@%@%@",@"查看全部评论(", [NSString stringWithFormat:@"%ld",total],@")"];
    }else{
        self.lblTotal.text=   [NSString stringWithFormat:@"暂无评论"];
    }
}

@end

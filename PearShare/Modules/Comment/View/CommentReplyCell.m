//
//  CommentReplyCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CommentReplyCell.h"
#import "CommentLikeAPI.h"
#import "LoginVC.h"
@interface CommentReplyCell ()
@property (nonatomic,strong)NSDictionary* commentModel;
@property (nonatomic ,assign) BOOL  isLike;
@end
@implementation CommentReplyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnLike.imageColorOn = [UIColor colorWithHexString:@"FF4A4A"];
    _btnLike.imageColorOff = [UIColor colorWithHexString:@"CCCCCC"];
    _btnLike.circleColor = [UIColor colorWithHexString:@"FF4A4A"];
    _btnLike.lineColor =  [UIColor colorWithHexString:@"FF4A4A"];
}


-(void)setModel:(NSDictionary *)model
{
    self.commentModel = [NSDictionary dictionaryWithDictionary:model];
    if ([model stringForKey:@"isLike"].length) {
        self.isLike = YES;
    }else{
        self.isLike = NO;
    }
    
    if ([[model stringForKey:@"replyType"] isEqualToString:@"2"]) {
        self.lblName.text = [NSString stringWithFormat:@"%@%@%@%@",[model stringForKey:@"fromUserNickname"],@"回复",[model stringForKey:@"toUserNickname"],@":"];
    }else{
        self.lblName.text =[NSString stringWithFormat:@"%@%@", [model stringForKey:@"fromUserNickname"],@":"];
        if ([[model stringForKey:@"fromUserId"] isEqualToString:[model stringForKey:@"shareUserId"]]) {
            self.lblName.text = [NSString stringWithFormat:@"%@%@",[model stringForKey:@"fromUserNickname"],@"(博主):"];
        }
        
        if ([[model stringForKey:@"fromUserId"] isEqualToString:kDefaultUserId]) {
            self.lblName.text = [NSString stringWithFormat:@"%@%@",[model stringForKey:@"fromUserNickname"],@"(我):"];
        }
    }
    
    self.lblCommend.text = [model stringForKey:@"content"];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    self.lblLike.text = [model stringForKey:@"likeNum"];
    
    self.lblTime.text = [self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
    
    if ([model stringForKey:@"isLike"].length) {
        [self.btnLike select];
    }else{
        [self.btnLike deselect];
    }
}

- (IBAction)likeCommand:(GoodButton*)sender
{
    if (kDefaultLoginAlready) {
        CommentLikeAPI *req = [CommentLikeAPI new];
        req.action = [NSString stringWithFormat:@"%d",self.isLike];
        req.type =@"1";
        req.targetId =[self.commentModel stringForKey:@"replyId"];
        [req startWithCompletionBlockWithSuccess:^( CommentLikeAPI* _Nonnull request) {
            if ([request isSuccess]) {
                if (self.isLike) {
                    [self.btnLike deselect];
                    [self.lblLike decreaseNumber:1];
                } else {
                    [self.btnLike select];
                    [self.lblLike increaseNumber:1];
                }
                self.isLike = !self.isLike;
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
        
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
    }
    
}
- (IBAction)comendCommend:(id)sender
{
    if (self.sendBlock) {
        self.sendBlock(self.commentModel);
    }
}
@end

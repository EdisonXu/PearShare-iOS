//
//  CommentSonCell.h
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentSonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIButton *btnNumber;
@property (nonatomic,strong)NSDictionary* model;
@property (nonatomic,strong)NSString * replyCount;
@end

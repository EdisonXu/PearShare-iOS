//
//  AllCommentCell.h
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllCommentCell : UITableViewCell
@property (nonatomic,assign)NSInteger total;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@end

//
//  CommentSonCell.m
//  PearShare
//
//  Created by Destiny on 2018/6/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CommentSonCell.h"
#import "SingleComment.h"
@interface CommentSonCell ()
@property (nonatomic,strong)NSDictionary* commentModel;
@end
@implementation CommentSonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    self.commentModel = [NSDictionary dictionaryWithDictionary:model];
    NSString * name =[NSString stringWithFormat:@"%@%@", [model stringForKey:@"fromUserNickname"],@":  "];
    NSString *commend =[model stringForKey:@"content"];
    self.lblComment.text = [NSString stringWithFormat:@"%@%@",name,commend];
}

-(void)setReplyCount:(NSString *)replyCount
{
    [self.btnNumber setTitle:[NSString stringWithFormat:@"%@%@%@",@"共",replyCount,@"条回复 >"] forState:UIControlStateNormal];
}
- (IBAction)comment:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
    SingleComment *vc = [storyboard instantiateViewControllerWithIdentifier:@"SingleComment"];
    vc.commentId = [self.commentModel stringForKey:@"replyCommentId"];
    vc.type = 1;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}
@end

//
//  ReplyTopCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodButton.h"
#import "SPScrollNumLabel.h"
#import "BaseTableViewCell.h"
@interface ReplyTopCell : BaseTableViewCell
@property (nonatomic,strong)NSDictionary *model;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCommend;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet GoodButton *btnLike;
@property (weak, nonatomic) IBOutlet SPScrollNumLabel *lblLike;
@end

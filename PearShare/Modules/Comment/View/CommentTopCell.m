//
//  CommentTopCell.m
//  PearShare
//
//  Created by Destiny on 2018/6/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CommentTopCell.h"
#import "CommentLikeAPI.h"
#import "SingleComment.h"
#import "LoginVC.h"
@interface CommentTopCell ()
@property (nonatomic,strong)NSDictionary* commentModel;
@property (nonatomic ,assign) BOOL  isLike;
@end
@implementation CommentTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _btnLike.imageColorOn = [UIColor colorWithHexString:@"FF4A4A"];
    _btnLike.imageColorOff = [UIColor colorWithHexString:@"CCCCCC"];
    _btnLike.circleColor = [UIColor colorWithHexString:@"FF4A4A"];
    _btnLike.lineColor =  [UIColor colorWithHexString:@"FF4A4A"];
    
}

-(void)setModel:(NSDictionary *)model
{
    self.commentModel = [NSDictionary dictionaryWithDictionary:model];
    if ([model stringForKey:@"isLike"].length) {
        self.isLike = YES;
    }else{
        self.isLike = NO;
    }
    
    self.lblName.text =[NSString stringWithFormat:@"%@%@", [model stringForKey:@"fromUserNickname"],@":"];
    if ([[model stringForKey:@"fromUserId"] isEqualToString:[model stringForKey:@"shareUserId"]]) {
        self.lblName.text = [NSString stringWithFormat:@"%@%@",[model stringForKey:@"fromUserNickname"],@"(博主):"];
    }
    
    if ([[model stringForKey:@"fromUserId"] isEqualToString:kDefaultUserId]) {
        self.lblName.text = [NSString stringWithFormat:@"%@%@",[model stringForKey:@"fromUserNickname"],@"(我):"];
    }
    
    self.lblCommend.text = [model stringForKey:@"content"];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    self.lblLike.text = [model stringForKey:@"likeNum"];

    self.lblTime.text = [self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
    
    if ([model stringForKey:@"isLike"].length) {
        [self.btnLike select];
    }else{
        [self.btnLike deselect];
    }
}



- (IBAction)likeCommand:(GoodButton*)sender
{
    if (kDefaultLoginAlready) {
        CommentLikeAPI *req = [CommentLikeAPI new];
        req.action = [NSString stringWithFormat:@"%d",self.isLike];
        req.type =@"0";
        req.targetId =[self.commentModel stringForKey:@"commentId"];
        [req startWithCompletionBlockWithSuccess:^( CommentLikeAPI* _Nonnull request) {
            if ([request isSuccess]) {
                if (self.isLike) {
                    [self.btnLike deselect];
                    [self.lblLike decreaseNumber:1];
                } else {
                    [self.btnLike select];
                    [self.lblLike increaseNumber:1];
                }
                self.isLike = !self.isLike;
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
        
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
    }

}
- (IBAction)comment:(id)sender
{
    if (kDefaultLoginAlready) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        SingleComment *vc = [storyboard instantiateViewControllerWithIdentifier:@"SingleComment"];
        vc.commentId = [self.commentModel stringForKey:@"commentId"];
        vc.type = 1;
        [self.viewController.navigationController pushViewController:vc animated:YES];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
    }
}
@end

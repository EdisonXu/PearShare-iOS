//
//  SearchHotAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SearchHotAPI.h"

@implementation SearchHotAPI

-(NSString *)requestUrl{
    return  @"shares/findTopTag";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.hotType forKey:@"topType"];
    return param;
}
@end

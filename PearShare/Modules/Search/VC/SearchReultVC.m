//
//  SearchReultVC.m
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SearchReultVC.h"
#import "SearchResultAPI.h"
#import "WaterFlowLayout.h"
#import "HomeItem.h"
#import "EmptyCollectionCell.h"
#import "DetailMangager.h"
@interface SearchReultVC()
<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WaterFlowLayoutDelegate>
@property (strong, nonatomic)  UICollectionView *collectionView;
@property (nonatomic,strong)NSMutableArray * articleList;
@property (nonatomic,strong)NSMutableArray * modeList;
@end

@implementation SearchReultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =[UIColor whiteColor];
    [self setUpCollectionView];
    [self.collectionView.mj_header beginRefreshing];
    self.title = self.key;
}

-(void)setUpCollectionView
{
    //设置瀑布流布局
    WaterFlowLayout *layout = [WaterFlowLayout new];
    layout.columnCount =2;
    layout.sectionInset = UIEdgeInsetsZero;
    layout.rowMargin = 9;
    layout.columnMargin = 9;
    layout.sectionInset = UIEdgeInsetsMake(0, 9, 0, 9);
    layout.delegate = self;
    
    self.collectionView =[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];
     [self.collectionView registerNib:[UINib nibWithNibName:@"EmptyCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"EmptyCollectionCell"];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    @weakify(self);
    self.collectionView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self refreshData];
    }];
    
    self.collectionView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self moreData];
    }];
}


-(void)refreshData
{
    SearchResultAPI *req = [SearchResultAPI new];
    req.key = self.key;
    req.page = 1;

    [req startWithCompletionBlockWithSuccess:^(SearchResultAPI * request) {
        if ([request isSuccess]) {
            self.articleList = [[NSMutableArray alloc] initWithArray:[request.responseObject arrayForKey:@"data"]];
            [self setModelListCommand];
            [self.collectionView reloadData];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [request message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
         [self.collectionView.mj_header endRefreshing];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.collectionView.mj_header endRefreshing];
    }];
}


-(void)moreData
{
    SearchResultAPI *req = [SearchResultAPI new];
    req.key = self.key;
    [req addPage];
    
    [req startWithCompletionBlockWithSuccess:^(SearchResultAPI * request) {
        if ([request isSuccess]) {
            [self.collectionView.mj_footer endRefreshing];
            NSMutableArray * moreList =  [[NSMutableArray alloc] initWithArray:[request.responseObject arrayForKey:@"data"]];
            if (moreList.count>0) {
                [self.articleList addObjectsFromArray:moreList];
                [self setModelListCommand];
                [self.collectionView reloadData];
            }
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [request message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

-(void)setModelListCommand
{
    self.modeList =[NSMutableArray array];
    if (self.articleList.count>0) {
        for (int i = 0; i<self.articleList.count; i++) {
            HomeArticleModel* model =[[HomeArticleModel alloc] initWithDictionary:[self.articleList objectAtIndex:i] error:nil];
            [self.modeList addObject:model];
        }
    }
}



#pragma mark ————— layout 代理 —————
-(CGFloat)waterFlowLayout:(WaterFlowLayout *)WaterFlowLayout heightForWidth:(CGFloat)width andIndexPath:(NSIndexPath *)indexPath{
    if (self.modeList.count) {
        if (indexPath.row==1) {
            return 269;
        }else{
            return 322;
        }
    }else{
        return self.collectionView.frame.size.height;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.modeList.count) {
      return self.modeList.count;
    }else{
        return 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.modeList.count) {
        HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
        if (indexPath.row==1) {
            cell.coverHeight.constant = 174;
        }else{
            cell.coverHeight.constant = 228;
        }
        cell.model = [self.modeList objectAtIndex:indexPath.row];
        return cell;
    }else{
        EmptyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCollectionCell" forIndexPath:indexPath];
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeArticleModel * model = [[HomeArticleModel alloc] init];
    model = [self.modeList objectAtIndex:indexPath.row];
    DetailMangager* vc = [[DetailMangager alloc] init];
    if ([model.groupStatus boolValue] ) {
        vc.type = @"2";
    }else{
        vc.type = @"1";
    }
    vc.shareId = model.shareId;
    vc.goodsId = model.goodsId;
    [self.navigationController pushViewController:vc animated:YES];
}
@end

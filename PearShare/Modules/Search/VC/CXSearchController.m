//
//  CXsearchController.m
//  搜索页面的封装
//
//  Created by 蔡翔 on 16/7/28.
//  Copyright © 2016年 蔡翔. All rights reserved.
//

#import "CXSearchController.h"
#import "CXSearchSectionModel.h"
#import "CXSearchModel.h"
#import "CXSearchCollectionViewCell.h"
#import "SelectCollectionReusableView.h"
#import "SelectCollectionLayout.h"
#import "CXDBHandle.h"
#import "SearchHotAPI.h"
#import "SearchReultVC.h"
#import "BaseNavigation.h"
#import "SearchTextFile.h"

static NSString *const cxSearchCollectionViewCell = @"CXSearchCollectionViewCell";

static NSString *const headerViewIden = @"HeadViewIden";

@interface CXSearchController()<UICollectionViewDataSource,UICollectionViewDelegate,SelectCollectionCellDelegate,UICollectionReusableViewButtonDelegate,UITextFieldDelegate>

/**
 *  存储网络请求的热搜，与本地缓存的历史搜索model数组
 */
@property (nonatomic, strong) NSMutableArray *sectionArray;
/**
 *  存搜索的数组 字典
 */
@property (nonatomic,strong) NSMutableArray *searchArray;

@property (weak, nonatomic) IBOutlet UICollectionView *cxSearchCollectionView;

@property (strong, nonatomic) SearchTextFile *cxSearchTextField;


@end

@implementation CXSearchController

-(NSMutableArray *)sectionArray
{
    if (_sectionArray == nil) {
        _sectionArray = [NSMutableArray array];
    }
    return _sectionArray;
}

-(NSMutableArray *)searchArray
{
    if (_searchArray == nil) {
        _searchArray = [NSMutableArray array];
    }
    return _searchArray;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavBar];
    [self.cxSearchCollectionView setCollectionViewLayout:[[SelectCollectionLayout alloc] init] animated:YES];
    [self.cxSearchCollectionView registerClass:[SelectCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerViewIden];
    [self.cxSearchCollectionView registerNib:[UINib nibWithNibName:@"CXSearchCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cxSearchCollectionViewCell];
    
    [self prepareData];
    self.cxSearchTextField.textSearch.delegate =self;
}



- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.cxSearchTextField.size =CGSizeMake(SCREEN_WIDTH*0.8, 30);
}

-(SearchTextFile*)cxSearchTextField
{
    if (!_cxSearchTextField) {
        _cxSearchTextField=  [[[NSBundle mainBundle]loadNibNamed:@"SearchTextFile" owner:self options:nil]objectAtIndex:0];
        _cxSearchTextField.textSearch.delegate =self;
    }
    return _cxSearchTextField;
}


-(void)setUpNavBar
{
    self.navigationItem.titleView = nil;
    self.cxSearchTextField = [[SearchTextFile alloc]initWithFrame:CGRectZero];
    self.cxSearchTextField.intrinsicContentSize = CGSizeMake(SCREEN_WIDTH*0.8, 30);//iOS11 更新重要！！！
    self.navigationItem.titleView=self.cxSearchTextField;
}

- (void)prepareData
{
    SearchHotAPI *req = [SearchHotAPI new];
    req.hotType = @"0";
    [req startWithCompletionBlockWithSuccess:^(SearchHotAPI * request) {
        NSMutableArray* hot = [NSMutableArray arrayWithArray: [request.result arrayForKey:@"data"]];
        if (hot.count) {
            NSMutableArray * list = [NSMutableArray array];
            for (int i = 0; i<hot.count; i++) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:[[hot dictionaryWithIndex:i]stringForKey:@"name"] forKey:@"content_name"];
                [list addObject:dic];
            }
            NSMutableDictionary* testDict = [[NSMutableDictionary alloc] init];
            [testDict setObject:list forKey:@"section_content"];
            [testDict setObject:@"1" forKey:@"section_id"];
            [testDict setObject:@"热搜" forKey:@"section_title"];
            
            NSMutableArray *testArray = [@[] mutableCopy];
            [testArray addObject:testDict];
            
            /***  去数据查看 是否有数据*/
            NSDictionary *parmDict  = @{@"category":@"1"};
            NSDictionary *dbDictionary =  [CXDBHandle statusesWithParams:parmDict];
            
            if (dbDictionary.count) {
                [testArray addObject:dbDictionary];
                [self.searchArray addObjectsFromArray:dbDictionary[@"section_content"]];
            }
            
            for (NSDictionary *sectionDict in testArray) {
                CXSearchSectionModel *model = [[CXSearchSectionModel alloc]initWithDictionary:sectionDict];
                [self.sectionArray addObject:model];
            }
            [self.cxSearchCollectionView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.cxSearchTextField.textSearch becomeFirstResponder];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    CXSearchSectionModel *sectionModel =  self.sectionArray[section];
    return sectionModel.section_contentArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CXSearchCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cxSearchCollectionViewCell forIndexPath:indexPath];
    CXSearchSectionModel *sectionModel =  self.sectionArray[indexPath.section];
    CXSearchModel *contentModel = sectionModel.section_contentArray[indexPath.row];
    [cell.contentButton setTitle:contentModel.content_name forState:UIControlStateNormal];
    cell.selectDelegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.sectionArray.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if ([kind isEqualToString: UICollectionElementKindSectionHeader]){
        SelectCollectionReusableView* view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:headerViewIden forIndexPath:indexPath];
        view.delectDelegate = self;
        CXSearchSectionModel *sectionModel =  self.sectionArray[indexPath.section];
        [view setText:sectionModel.section_title];
        /***  此处完全 也可以自定义自己想要的模型对应放入*/
        if(indexPath.section == 0)
        {
            [view setImage:@"cxCool"];
            view.delectButton.hidden = YES;
        }else{
            [view setImage:@"cxSearch"];
            view.delectButton.hidden = NO;
        }
        reusableview = view;
    }
    return reusableview;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CXSearchSectionModel *sectionModel =  self.sectionArray[indexPath.section];
    if (sectionModel.section_contentArray.count > 0) {
        CXSearchModel *contentModel = sectionModel.section_contentArray[indexPath.row];
        return [CXSearchCollectionViewCell getSizeWithText:contentModel.content_name];
    }
    return CGSizeMake(80, 24);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - SelectCollectionCellDelegate
- (void)selectButttonClick:(CXSearchCollectionViewCell *)cell;
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    NSIndexPath* indexPath = [self.cxSearchCollectionView indexPathForCell:cell];
    CXSearchSectionModel *sectionModel =  self.sectionArray[indexPath.section];
    CXSearchModel *contentModel = sectionModel.section_contentArray[indexPath.row];
    NSLog(@"您选的内容是：%@",contentModel.content_name);
    SearchReultVC * vc = [[SearchReultVC alloc] init];
    vc.key = contentModel.content_name;
    [self.navigationController pushViewController:vc animated:YES];

}

#pragma mark - UICollectionReusableViewButtonDelegate
- (void)delectData:(SelectCollectionReusableView *)view;
{
    if (self.sectionArray.count > 1) {
        [self.sectionArray removeLastObject];
        [self.searchArray removeAllObjects];
        [self.cxSearchCollectionView reloadData];
        [CXDBHandle saveStatuses:@{} andParam:@{@"category":@"1"}];
    }
}
#pragma mark - scrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.cxSearchTextField.textSearch resignFirstResponder];
}
#pragma mark - textField
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]){
        return NO;
    }
    /***  每搜索一次   就会存放一次到历史记录，但不存重复的*/
    if ([self.searchArray containsObject:[NSDictionary dictionaryWithObject:textField.text forKey:@"content_name"]]) {
        return YES;
    }
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    SearchReultVC * vc = [[SearchReultVC alloc] init];
    vc.key = self.cxSearchTextField.textSearch.text;
    [self.navigationController pushViewController:vc animated:YES];
    [self reloadData:textField.text];
    return YES;
}
- (void)reloadData:(NSString *)textString
{
    [self.searchArray addObject:[NSDictionary dictionaryWithObject:textString forKey:@"content_name"]];
    
    NSDictionary *searchDict = @{@"section_id":@"2",@"section_title":@"历史记录",@"section_content":self.searchArray};
    
    /***由于数据量并不大 这样每次存入再删除没问题  存数据库*/
    NSDictionary *parmDict  = @{@"category":@"1"};
    [CXDBHandle saveStatuses:searchDict andParam:parmDict];
    
    CXSearchSectionModel *model = [[CXSearchSectionModel alloc]initWithDictionary:searchDict];
    if (self.sectionArray.count > 1) {
        [self.sectionArray removeLastObject];
    }
    [self.sectionArray addObject:model];
    [self.cxSearchCollectionView reloadData];
    self.cxSearchTextField.textSearch.text = @"";
}
@end

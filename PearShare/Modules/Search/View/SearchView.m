//
//  SearchView.m
//  PearShare
//
//  Created by Destiny on 2018/5/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SearchView.h"

#define loadXibWithName(name) [[NSBundle mainBundle] loadNibNamed:name owner:self options:nil][0]

@interface SearchView()
@end

@implementation SearchView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self = loadXibWithName(@"SearchView");
    self.width=SCREEN_WIDTH;
    self.height = 30;
    [self layoutIfNeeded];
    return self;
}




@end

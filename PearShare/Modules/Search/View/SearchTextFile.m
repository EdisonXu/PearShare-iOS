//
//  SearchTextFile.m
//  PearShare
//
//  Created by Edison on 2018/8/1.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SearchTextFile.h"
#define loadXibWithName(name) [[NSBundle mainBundle] loadNibNamed:name owner:self options:nil][0]
@implementation SearchTextFile

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    self = loadXibWithName(@"SearchTextFile");
    self.width=SCREEN_WIDTH;
    self.height = 30;
    [self layoutIfNeeded];
    return self;
}

@end

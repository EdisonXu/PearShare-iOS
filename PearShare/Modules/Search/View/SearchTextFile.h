//
//  SearchTextFile.h
//  PearShare
//
//  Created by Edison on 2018/8/1.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+TP.h"
@interface SearchTextFile : UIView
@property (weak, nonatomic) IBOutlet UITextField *textSearch;
@property(nonatomic, assign) CGSize intrinsicContentSize;
@end

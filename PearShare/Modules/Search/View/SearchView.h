//
//  SearchView.h
//  PearShare
//
//  Created by Destiny on 2018/5/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+TP.h"
@interface SearchView : UIView
@property(nonatomic, assign) CGSize intrinsicContentSize;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@end

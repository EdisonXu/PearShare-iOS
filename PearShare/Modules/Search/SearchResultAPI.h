//
//  SearchResultAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface SearchResultAPI : BaseRequestAPI
@property (nonatomic,strong)NSString *key;
@property (nonatomic,assign) NSInteger        page;
@property (nonatomic,assign) NSString*        pageSize;

-(void)addPage;
@end

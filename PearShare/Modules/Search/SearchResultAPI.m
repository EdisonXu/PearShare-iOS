//
//  SearchResultAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SearchResultAPI.h"

@implementation SearchResultAPI
-(NSString *)requestUrl{
    return  @"shares/findShareSolr";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.key forKey:@"queryString"];
    [param setString:self.pageSize forKey:@"pageSize"];
    [param setString:[NSString stringWithFormat:@"%ld",self.page] forKey:@"pageNum"];
    return param;
}

-(NSString*)pageSize
{
    if (!_pageSize) {
        _pageSize = [NSString stringWithFormat:@"20"];
    }
    return _pageSize;
}


-(NSInteger)page
{
    if (!_page) {
        _page =1;
    }
    return _page;
}

-(void)addPage
{
    self.page = self.page+1;
}
@end

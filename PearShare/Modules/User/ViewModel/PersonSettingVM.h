//
//  PersonSettingVM.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface PersonSettingVM : BaseViewModel
@property (nonatomic,strong)UIImage * imageData;
@property (nonatomic,strong)NSString * headerString;
-(RACSignal *)updateHeaderCommand;
-(RACSignal*)updateFileCommand;
@end

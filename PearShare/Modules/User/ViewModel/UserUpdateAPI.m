//
//  UserUpdateAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "UserUpdateAPI.h"

@implementation UserUpdateAPI
-(NSString *)requestUrl{
    return @"user/updateInfo";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPUT;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.avatar forKey:@"avatar"];
    [param setString:self.nickname forKey:@"nickname"];
    [param setString:self.sign forKey:@"sign"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}

@end

//
//  PersonSettingVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "PersonSettingVM.h"

@implementation PersonSettingVM
//更新头像
-(RACSignal *)updateHeaderCommand
{

    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking uploadWithImage:_imageData url:[baseRequestUrl stringByAppendingString:@"imgupload/imgUpload"] filename:@"header.png" name:@"upfile" mimeType:@"image/jpeg" parameters:nil progress:^(int64_t bytesWritten, int64_t totalBytesWritten) {
        } success:^(NSDictionary* response) {
            self.headerString = [NSString stringWithFormat:@"%@", [PinImageUrl stringByAppendingString:[response stringForKey:@"data"]]];
            [subscriber sendNext:response];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
           
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)updateFileCommand;
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self.param setString:kDefaultUserId forKey:@"userId"];
        [self.param setString:self.headerString forKey:@"avatar"];
        [HYBNetworking postWithUrl:[baseRequestUrl stringByAppendingString:@"user/modifyUser"] refreshCache:YES params:self.param success:^(id response) {
            [subscriber sendNext:response];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            
        }];
        return nil;
    }];
    return signal;

}
@end

//
//  LoginAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface LoginAPI : BaseRequestAPI
/**
 loginType：登陆类型（0-密码 1-验证码 2-微信 3-微博）
 */
@property (nonatomic,strong) NSString* loginType;
@property (nonatomic,strong) NSString* phone;//手机号
@property (nonatomic,strong) NSString* phoneCode;//验证码
@property (nonatomic,strong) NSString* openId;//openId
@property (nonatomic,strong) NSString* nickname;//昵称
@property (nonatomic,strong) NSString* avatar;//头像
@end

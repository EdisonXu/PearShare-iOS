//
//  LoginVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/25.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "LoginVM.h"
#import "LoginAPI.h"
#import "CodeAPI.h"
#import "BaseRequestAPI.h"
@implementation LoginVM

-(RACSignal*)loginCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        LoginAPI *req = [LoginAPI new];
        req.phone = self.phoneNumber;
        req.loginType =self.typeNumber;
        req.phoneCode =self.codeNumber;
        req.openId = self.openId;
        req.nickname = self.nickname;
        req.avatar = self.avatar;
        
        [req startWithCompletionBlockWithSuccess:^(LoginAPI * request) {
            [subscriber sendNext:request];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}

-(RACSignal*)registerThridLogin
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.navigationController.view animated:YES];
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
            if (error) {
                [hud hideAnimated:YES];
            } else {
                [hud hideAnimated:YES];
                UMSocialUserInfoResponse *resp = result;
                LoginAPI *req = [LoginAPI new];
                req.loginType =self.typeNumber;
                req.openId = resp.openid;
                req.nickname = resp.name;
                req.avatar = resp.iconurl;
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.navigationController.view animated:YES];
                [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                    [subscriber sendNext:req];
                    [subscriber sendCompleted];
                    [hud hideAnimated:YES];
                } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                    [hud hideAnimated:YES];
                }];
            }
        }];
        return nil;
    }];
    return signal;
}



-(RACSignal*)verifiationCodeCoomand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        CodeAPI *req = [CodeAPI new];
        req.phone = self.phoneNumber;
        req.type =[NSString stringWithFormat:@"1"];
        
        [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendNext:req];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}
@end

//
//  userInfoAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/8.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface userInfoAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* token;
@property (nonatomic,strong) NSString* userId;
@end

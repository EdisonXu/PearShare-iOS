//
//  ModifyPhoneAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface ModifyPhoneAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* phone;
@property (nonatomic,strong) NSString* phoneCode;
@end

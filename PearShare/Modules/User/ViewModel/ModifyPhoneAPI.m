//
//  ModifyPhoneAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ModifyPhoneAPI.h"

@implementation ModifyPhoneAPI
-(NSString *)requestUrl{
    return @"user/editPhone/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPUT;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.phone forKey:@"phone"];
    [param setString:self.phoneCode forKey:@"code"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

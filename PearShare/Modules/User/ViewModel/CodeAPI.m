//
//  CodeAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CodeAPI.h"

@implementation CodeAPI

-(NSString *)requestUrl{
    return [NSString stringWithFormat:@"%@%@%@%@",@"user/smsCode/",self.phone,@"/",self.type];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end

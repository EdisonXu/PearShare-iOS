//
//  LoginAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "LoginAPI.h"

@implementation LoginAPI

-(NSString *)requestUrl{
    return [@"user/login/" stringByAppendingString:self.loginType];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
     [param setString:self.phone forKey:@"phone"];
     [param setString:self.phoneCode forKey:@"code"];
     [param setString:self.openId forKey:@"openId"];
     [param setString:self.nickname forKey:@"nickname"];
     [param setString:self.avatar forKey:@"avatar"];
    return param;
}
@end

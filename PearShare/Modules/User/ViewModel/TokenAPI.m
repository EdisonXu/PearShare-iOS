//
//  TokenAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "TokenAPI.h"

@implementation TokenAPI
-(NSString *)requestUrl{
    return [NSString stringWithFormat:@"%@%@",@"user/saveDeviceToken/",[kDefaults stringForKey:@"pushToken"]];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}


- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

//
//  MyAttentionAPI.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MyAttentionAPI.h"

@implementation MyAttentionAPI

-(NSString *)requestUrl{
    return @"fans/getUserFans";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:@"20" forKey:@"pageSize"];
    [param setString:[NSString stringWithFormat:@"%ld",self.page] forKey:@"pageNum"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}


@end

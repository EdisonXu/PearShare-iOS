//
//  userCenterVM.m
//  PearShare
//
//  Created by Destiny on 2018/6/8.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "userCenterVM.h"
#import "userInfoAPI.h"
@implementation userCenterVM

-(RACSignal*)userInfoCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        userInfoAPI *req = [userInfoAPI new];
        req.userId = self.userId;

        [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendNext:req];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}
@end

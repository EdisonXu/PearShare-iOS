//
//  FeedAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface FeedAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* content;
@property (nonatomic,strong) NSString* phone;//手机号
@end

//
//  userCenterVM.h
//  PearShare
//
//  Created by Destiny on 2018/6/8.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface userCenterVM : BaseViewModel
@property (nonatomic,strong)NSString* userId;
-(RACSignal*)userInfoCommand;
@end

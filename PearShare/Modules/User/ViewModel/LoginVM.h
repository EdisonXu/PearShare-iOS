//
//  LoginVM.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/25.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface LoginVM : BaseViewModel
@property (nonatomic,strong)NSString * phoneNumber;
@property (nonatomic,strong)NSString * codeNumber;
@property (nonatomic,strong)NSString * typeNumber;
@property (nonatomic,assign) NSString* openId;//openId
@property (nonatomic,assign) NSString* nickname;//昵称
@property (nonatomic,assign) NSString* avatar;//头像

-(RACSignal*)loginCommand;

-(RACSignal*)registerThridLogin;

-(RACSignal*)verifiationCodeCoomand;

-(void)saveToken;

@end

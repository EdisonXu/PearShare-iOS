//
//  NickNameVM.h
//  QTFootBall_2.6.0
//
//  Created by Edison on 2017/6/22.
//  Copyright © 2017年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface NickNameVM : BaseViewModel
@property (nonatomic,strong)NSString * name;
@property (nonatomic,assign)NSInteger type;

-(RACSignal *)updateNameCommand;
@end

//
//  NickNameVM.m
//  QTFootBall_2.6.0
//
//  Created by Edison on 2017/6/22.
//  Copyright © 2017年 Edison. All rights reserved.
//

#import "NickNameVM.h"

@implementation NickNameVM
-(RACSignal *)updateNameCommand
{
    [self.param setObj:kDefaultUserId forKey:@"userId"];
    if (self.type==1) {
         [self.param setObj:_name forKey:@"nickname"];
    }else{
        [self.param setObj:_name forKey:@"signature"];
    }
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking postWithUrl:[baseRequestUrl stringByAppendingString:@"user/modifyUser"] refreshCache:YES params:self.param success:^(id response) {
                [subscriber sendNext:response];
                [subscriber sendCompleted];
        } fail:^(NSError *error) {
        }];
        return nil;
    }];
    return signal;
}
@end

//
//  UserUpdateAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface UserUpdateAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* avatar;
@property (nonatomic,strong) NSString* nickname;//手机号
@property (nonatomic,strong) NSString* sex;
@property (nonatomic,strong) NSString* sign;//手机号
@end

//
//  CodeAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface CodeAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* phone;
@property (nonatomic,strong) NSString* type;
@end

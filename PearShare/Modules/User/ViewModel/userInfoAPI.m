//
//  userInfoAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/8.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "userInfoAPI.h"

@implementation userInfoAPI

-(NSString *)requestUrl{
    return @"user/info/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.userId forKey:@"userId"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

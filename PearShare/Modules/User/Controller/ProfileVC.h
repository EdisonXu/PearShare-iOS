//
//  ProfileVC.h
//  PearShare
//
//  Created by Destiny on 2018/6/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewController.h"

@interface ProfileVC : BaseTableViewController
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblSign;
@property (weak, nonatomic) IBOutlet UIButton *btnMan;
@property (weak, nonatomic) IBOutlet UIButton *btnWomen;
@end

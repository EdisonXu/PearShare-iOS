//
//  BindingVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/5/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BindingVC.h"
#import "CountDown.h"
#import "MQValidator.h"
#import "LoginVM.h"
#import "LoginAPI.h"
#import "CodeAPI.h"
@interface BindingVC ()
{
    NSString * _phoneNumber;
    NSString * _codeNumber;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIButton *btnCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSure;
@property (strong, nonatomic) CountDown *countDownForBtn;
@property (nonatomic,strong)LoginVM * viewModel;
@end

@implementation BindingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel =[[LoginVM alloc]init];
    self.title = @"绑定手机号";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.countDownForBtn = [[CountDown alloc] init];
    [self.phoneText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnCode addTarget:self action:@selector(fetchCoder:) forControlEvents:UIControlEventTouchUpInside];
    [self setUpViewCommand];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)setUpViewCommand
{
    [[RACSignal combineLatest:@[self.phoneText.rac_textSignal,self.codeText.rac_textSignal]] subscribeNext:^(RACTuple* tuple) {
        if ([MQValidator isPhoneNumber:[tuple objectAtIndex:0]]) {
            _phoneNumber =[tuple objectAtIndex:0];
            self.btnCode.enabled = YES;
        }else{
            self.btnCode.enabled = NO;
        }
        
        if ([MQValidator isPhoneNumber:[tuple objectAtIndex:0]]&&[MQValidator isVerifyCode:[tuple objectAtIndex:1]]) {
            _codeNumber =[tuple objectAtIndex:1];
            self.btnSure.enabled = YES;
            self.btnSure.backgroundColor =   [UIColor colorWithHexString:@"FED500"];
        }else{
            self.btnSure.enabled = NO;
            self.btnSure.backgroundColor =   [UIColor colorWithHexString:@"DADADB"];
        }
    }];
}

-(void)fetchCoder:(UIButton *)sender{
    NSTimeInterval aMinutes = 60;
    [self startWithStartDate:[NSDate date] finishDate:[NSDate dateWithTimeIntervalSinceNow:aMinutes]];
}
//此方法用两个NSDate对象做参数进行倒计时
-(void)startWithStartDate:(NSDate *)strtDate finishDate:(NSDate *)finishDate{
    __weak __typeof(self) weakSelf= self;
    [_countDownForBtn countDownWithStratDate:strtDate finishDate:finishDate completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        NSInteger totoalSecond =day*24*60*60+hour*60*60 + minute*60+second;
        if (totoalSecond==0) {
            weakSelf.btnCode.enabled = YES;
            [weakSelf.btnCode setTitle:@"获取验证码" forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"FED500"] forState:UIControlStateNormal];
        }else{
            weakSelf.btnCode.enabled = NO;
            [weakSelf.btnCode setTitle:[NSString stringWithFormat:@"%lis后重发",totoalSecond] forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"dadadb"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark text代理

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneText) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }else{
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}


- (IBAction)bindingCommand:(id)sender
{
    self.viewModel.phoneNumber = _phoneNumber;
    self.viewModel.codeNumber = _codeNumber;
    self.viewModel.openId =self.openId;
    self.viewModel.nickname =self.nickname;
    self.viewModel.avatar =self.avatar;
    self.viewModel.typeNumber = _loginType;
    RACSignal * signal = self.viewModel.loginCommand;
    [signal subscribeNext:^(LoginAPI* api) {
        if ([api isSuccess]) {
            kUserDataSet([api.result dictionaryForKey:@"data"]);
            [kDefaults synchronize];
            NSLog(@"%@",kDefaultUserData);
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = NSLocalizedString(@"登录成功!", @"HUD message title");
            [hud hideAnimated:YES afterDelay:1.f];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                if ([self.loginType isEqualToString:@"1"]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName: Qnotifiction_MerchanLogin object:@"true"];
                }
            });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"%@",[api message]];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } error:^(NSError *error) {

    }];
}

- (IBAction)codeCommand:(id)sender
{
    self.viewModel.phoneNumber = _phoneNumber;
    RACSignal * signal = self.viewModel.verifiationCodeCoomand;
    [signal subscribeNext:^(CodeAPI* api) {
        if ([api isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"短信发送成功！"];
            [hud hideAnimated:YES afterDelay:2.f];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } error:^(NSError *error) {
        
    }];
}
@end

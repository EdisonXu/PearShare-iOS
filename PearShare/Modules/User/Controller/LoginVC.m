//
//  LoginVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/25.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "LoginVC.h"
#import "LoginVM.h"
#import "CountDown.h"
#import "MQValidator.h"
#import "BindingVC.h"
#import "LoginAPI.h"
#import "CodeAPI.h"
@interface LoginVC ()
{
    NSString * _phoneNumber;
    NSString * _codeNumber;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneTextFile;
@property (weak, nonatomic) IBOutlet UITextField *codeTextFile;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnCode;
@property (nonatomic,strong)LoginVM * viewModel;
@property (strong, nonatomic) CountDown *countDownForBtn;
@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel  = [[LoginVM alloc]init];
    self.viewModel.controller  = self;
    self.countDownForBtn = [[CountDown alloc] init];
    [self.phoneTextFile addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeTextFile addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnCode addTarget:self action:@selector(fetchCoder:) forControlEvents:UIControlEventTouchUpInside];
    [self setUpViewCommand];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)setUpViewCommand
{
    [[RACSignal combineLatest:@[self.phoneTextFile.rac_textSignal,self.codeTextFile.rac_textSignal]] subscribeNext:^(RACTuple* tuple) {
        if (((NSString*)[tuple objectAtIndex:0]).length==11) {
            _phoneNumber =[tuple objectAtIndex:0];
            self.btnCode.enabled = YES;
        }else{
            self.btnCode.enabled = NO;
        }
        
        if (((NSString*)[tuple objectAtIndex:0]).length==11&&[MQValidator isVerifyCode:[tuple objectAtIndex:1]]) {
            _codeNumber =[tuple objectAtIndex:1];
            self.btnLogin.enabled = YES;
            self.btnLogin.backgroundColor =   [UIColor colorWithHexString:@"FED500"];
        }else{
            self.btnLogin.enabled = NO;
            self.btnLogin.backgroundColor =   [UIColor colorWithHexString:@"DADADB"];
        }
    }];
}

-(void)fetchCoder:(UIButton *)sender{
    NSTimeInterval aMinutes = 60;
    [self startWithStartDate:[NSDate date] finishDate:[NSDate dateWithTimeIntervalSinceNow:aMinutes]];
}

//此方法用两个NSDate对象做参数进行倒计时
-(void)startWithStartDate:(NSDate *)strtDate finishDate:(NSDate *)finishDate{
    __weak __typeof(self) weakSelf= self;
    [_countDownForBtn countDownWithStratDate:strtDate finishDate:finishDate completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        NSInteger totoalSecond =day*24*60*60+hour*60*60 + minute*60+second;
        if (totoalSecond==0) {
            weakSelf.btnCode.enabled = YES;
            [weakSelf.btnCode setTitle:@"获取验证码" forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"FED500"] forState:UIControlStateNormal];
        }else{
            weakSelf.btnCode.enabled = NO;
            [weakSelf.btnCode setTitle:[NSString stringWithFormat:@"%lis后重发",totoalSecond] forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"dadadb"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark text代理

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneTextFile) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }else{
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}

- (IBAction)colseCommand:(id)sender {
    if ([self.form isEqualToString:@"UserCenter"]||[self.form isEqualToString:@"desire"]) {
        [((UITabBarController*)kRootViewController) setSelectedIndex:0];
    }
      [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginCommand:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    self.viewModel.phoneNumber = _phoneNumber;
    self.viewModel.codeNumber = _codeNumber;
    self.viewModel.typeNumber = @"1";
    RACSignal * signal = self.viewModel.loginCommand;
    [signal subscribeNext:^(LoginAPI* api) {
        [hud hideAnimated:YES];
        if ([api isSuccess]) {
            kUserDataSet([api.result dictionaryForKey:@"data"]);
            [kDefaults synchronize];
            NSLog(@"%@",kDefaultUserData);
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } error:^(NSError *error) {
        [hud hideAnimated:YES];
    }];
}

- (IBAction)codeCommand:(id)sender {
    self.viewModel.phoneNumber = _phoneNumber;
    RACSignal * signal = self.viewModel.verifiationCodeCoomand;
    [signal subscribeNext:^(CodeAPI* api) {
        if ([api isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"短信发送成功！"];
            [hud hideAnimated:YES afterDelay:2.f];
//            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
//            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//                [[NSNotificationCenter defaultCenter] postNotificationName: Qnotifiction_MerchanLogin object:@"true"];
//            });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } error:^(NSError *error) {
        
    }];
}
- (IBAction)merchantLogin:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    BindingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"BindingVC"];
    vc.loginType = [NSString stringWithFormat:@"1"];
    [self.navigationController pushViewController:vc animated:YES ];
}

- (IBAction)loginWexin:(id)sender
{
    self.viewModel.typeNumber = [NSString stringWithFormat:@"2"];
    RACSignal * signal = self.viewModel.registerThridLogin;
    [signal subscribeNext:^(LoginAPI*api) {
        if ([api isSuccess]) {
            kUserDataSet([api.result dictionaryForKey:@"data"]);
            [kDefaults synchronize];
            NSLog(@"%@",kDefaultUserData);
             [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            if ([api.code isEqualToString:@"1006"]) {
                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
                BindingVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"BindingVC"];
                vc.openId = api.openId;
                vc.avatar = api.avatar;
                vc.nickname = api.nickname;
                vc.loginType =@"2";
                [self.navigationController pushViewController:vc animated:YES ];
            }
        }
    } error:^(NSError *error) {
    
    }];
}

- (IBAction)loginWeibo:(id)sender
{
    
}
@end

//
//  ModifyPhoneVC.m
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ModifyPhoneVC.h"
#import "CountDown.h"
#import "MQValidator.h"
#import "ModifyPhoneAPI.h"
#import "CodeAPI.h"
@interface ModifyPhoneVC ()
{
    NSString * _phoneNumber;
    NSString * _codeNumber;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UIButton *btnCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSure;
@property (strong, nonatomic) CountDown *countDownForBtn;

@end

@implementation ModifyPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改绑定手机号";
//    NSLog(@"%@",kd)
    self.lblOldPhone.text =[NSString stringWithFormat:@"%@%@%@",@"当前手机号为",[kDefaultUserData stringForKey:@"phone"],@"，请输入新手机号"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.countDownForBtn = [[CountDown alloc] init];
    [self.phoneText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnCode addTarget:self action:@selector(fetchCoder:) forControlEvents:UIControlEventTouchUpInside];
    [self setUpViewCommand];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)setUpViewCommand
{
    [[RACSignal combineLatest:@[self.phoneText.rac_textSignal,self.codeText.rac_textSignal]] subscribeNext:^(RACTuple* tuple) {
        if ([MQValidator isPhoneNumber:[tuple objectAtIndex:0]]) {
            _phoneNumber =[tuple objectAtIndex:0];
            self.btnCode.enabled = YES;
        }else{
            self.btnCode.enabled = NO;
        }
        
        if ([MQValidator isPhoneNumber:[tuple objectAtIndex:0]]&&[MQValidator isVerifyCode:[tuple objectAtIndex:1]]) {
            _codeNumber =[tuple objectAtIndex:1];
            self.btnSure.enabled = YES;
            self.btnSure.backgroundColor =   [UIColor colorWithHexString:@"FED500"];
        }else{
            self.btnSure.enabled = NO;
            self.btnSure.backgroundColor =   [UIColor colorWithHexString:@"DADADB"];
        }
    }];
}

-(void)fetchCoder:(UIButton *)sender{
    NSTimeInterval aMinutes = 60;
    [self startWithStartDate:[NSDate date] finishDate:[NSDate dateWithTimeIntervalSinceNow:aMinutes]];
}
//此方法用两个NSDate对象做参数进行倒计时
-(void)startWithStartDate:(NSDate *)strtDate finishDate:(NSDate *)finishDate{
    __weak __typeof(self) weakSelf= self;
    [_countDownForBtn countDownWithStratDate:strtDate finishDate:finishDate completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        NSInteger totoalSecond =day*24*60*60+hour*60*60 + minute*60+second;
        if (totoalSecond==0) {
            weakSelf.btnCode.enabled = YES;
            [weakSelf.btnCode setTitle:@"获取验证码" forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"FED500"] forState:UIControlStateNormal];
        }else{
            weakSelf.btnCode.enabled = NO;
            [weakSelf.btnCode setTitle:[NSString stringWithFormat:@"%lis后重发",totoalSecond] forState:UIControlStateNormal];
            [weakSelf.btnCode setTitleColor:[UIColor colorWithHexString:@"dadadb"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark text代理

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneText) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }else{
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}


- (IBAction)bindingCommand:(id)sender
{
    ModifyPhoneAPI *req = [ModifyPhoneAPI new];
    req.phone = _phoneNumber;
    req.phoneCode =_codeNumber;

    [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([req isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = NSLocalizedString(@"修改成功!", @"HUD message title");
            [hud hideAnimated:YES afterDelay:2.f];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController popToRootViewControllerAnimated:NO];
            });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"%@",[req message]];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}

- (IBAction)codeCommand:(id)sender
{
    CodeAPI *req = [CodeAPI new];
    req.phone = _phoneNumber;
    req.type =[NSString stringWithFormat:@"3"];
    
    [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([req isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"短信发送成功！"];
            [hud hideAnimated:YES afterDelay:2.f];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [req message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}
@end

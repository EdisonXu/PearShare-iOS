//
//  MyAttentionList.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MyAttentionList.h"
#import "MyAttentionCell.h"
#import "MyAttentionAPI.h"
@interface MyAttentionList ()
@property (nonatomic,strong)NSMutableArray * attentionList;
@property (nonatomic,assign)NSInteger  page;
@end

@implementation MyAttentionList

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的关注";
    self.tableView.tableFooterView = [[UIView alloc] init];
    @weakify(self);
    self.tableView.mj_header =[MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpData];
    }];
    
    self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self moreData];
    }];
    [self.tableView registerNib:MyAttentionCell.nib forCellReuseIdentifier:@"MyAttentionCell"];
    [self.tableView.mj_header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.attentionList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        MyAttentionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyAttentionCell" forIndexPath:indexPath];
        cell.data = [self.attentionList dictionaryWithIndex:indexPath.row];
        return cell;
    }

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(void)setUpData
{
    MyAttentionAPI *api = [MyAttentionAPI new];
    self.page = 1;
    api.page = self.page;
    [api startWithCompletionBlockWithSuccess:^(MyAttentionAPI * request) {
        [self.tableView.mj_header  endRefreshing];
        if ([api isSuccess]) {
            self.attentionList = [[NSMutableArray arrayWithArray:[request.result arrayForKey:@"data"]] mutableCopy];
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header  endRefreshing];
    }];
}


-(void)moreData
{
    MyAttentionAPI *api = [MyAttentionAPI new];
    self.page = self.page+1;
    api.page = self.page;
    [api startWithCompletionBlockWithSuccess:^(MyAttentionAPI * request) {
        [self.tableView.mj_footer endRefreshing];
        if ([request isSuccess]) {
            NSMutableArray * moreList = [[NSMutableArray arrayWithArray:[request.result arrayForKey:@"data"]] mutableCopy];
            if (moreList.count>0) {
                [self.attentionList addObjectsFromArray:moreList];
                [self.tableView reloadData];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_footer endRefreshing];
    }];
}
@end

//
//  BindingVC.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/5/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BindingVC : UITableViewController
/**
 loginType：登陆类型（0-密码 1-验证码 2-微信 3-微博）
 */
@property (nonatomic,strong) NSString* loginType;
@property (nonatomic,strong) NSString* openId;//openId
@property (nonatomic,strong) NSString* nickname;//昵称
@property (nonatomic,strong) NSString* avatar;//头像
@end

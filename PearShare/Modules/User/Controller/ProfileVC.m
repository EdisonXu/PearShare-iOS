//
//  ProfileVC.m
//  PearShare
//
//  Created by Destiny on 2018/6/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ProfileVC.h"
#import "UIImage+Resize.h"
#import "UpdateSingalAPI.h"
#import "PPNetworkHelper.h"
#import "UserUpdateAPI.h"
#import "NickNameVC.h"
#import "HXPhotoPicker.h"
@interface ProfileVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,HXPhotoViewDelegate>
@property (nonatomic,strong)UserInfo *userModel;

/**  照片管理  */
@property (nonatomic, strong) HXPhotoManager *manager;
/**  照片视图  */
@property (nonatomic, strong) HXPhotoView *photoView;
@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"个人信息"];
     self.btnWomen.selected = YES;
    
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    self.userModel = [UserInfo yy_modelWithJSON:userInfo];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:self.userModel.avatar] placeholderImage:[UIImage imageNamed:@"logo"]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    self.userModel = [UserInfo yy_modelWithJSON:userInfo];
    self.lblName.text =self.userModel.nickname;
    self.lblSign.text =self.userModel.signature;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        [self headImageClick];
    }
    
    if (indexPath.row==1) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        NickNameVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NickNameVC"];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (indexPath.row==3) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        NickNameVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"NickNameVC"];
        vc.type = 2;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 点击头像
-(void) headImageClick{
    UIAlertController* actionSheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* weiboAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* vc = [UIImagePickerController new];
        vc.delegate = self;
        vc.allowsEditing = YES;
        [self presentViewController:vc animated:YES completion:nil];
    }];

    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [actionSheetController addAction:weiboAction];
    [actionSheetController addAction:cancelAction];
    
    [cancelAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [self presentViewController:actionSheetController animated:YES completion:nil];
}

#pragma mark - 相册代理
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *cutImage=[info[UIImagePickerControllerEditedImage] resizedImageToRefrenceWidth:750];
    NSMutableArray * list = [NSMutableArray arrayWithObject:cutImage];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/batchuploadFile"]parameters:nil name:@"files" images:list fileNames:nil imageScale:0.8 imageType:nil progress:^(NSProgress *progress) {
        NSLog(@"%@",progress);
    } success:^(NSDictionary* responseObject) {
         [hud hideAnimated:YES];
        NSLog(@"%@",responseObject);
        [self upLoadHeader:[[[responseObject dictionaryForKey:@"data"]arrayForKey:@"urls"]stringWithIndex:0]];
        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        
    }];
    
    
    [self.imageHeader setImage:cutImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)upLoadHeader:(NSString*)header
{
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObj:header forKey:@"avatar"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:[URL_main stringByAppendingString:@"/user/updateInfo"] parameters:nil error:nil];
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {

    }] resume];
    

}
- (IBAction)women:(id)sender
{
    self.btnWomen.selected = YES;
    self.btnMan.selected = NO;
}

- (IBAction)man:(id)sender
{
    self.btnWomen.selected = NO;
    self.btnMan.selected = YES;
}
@end

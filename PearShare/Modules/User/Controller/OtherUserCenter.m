//
//  OtherUserCenter.m
//  PearShare
//
//  Created by Destiny on 2018/6/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "OtherUserCenter.h"
#import "HomeItem.h"
#import "OtherUserInfoItem.h"
#import "UserDesireHeader.h"
#import "userCenterVM.h"
#import "userInfoAPI.h"
#import "GoodsDetail.h"
#import "ArticleDetail.h"
#import "DetailMangager.h"

#define Cell_Spacing 9
@interface OtherUserCenter ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate>
@property (nonatomic,strong)userCenterVM * viewModel;
@property (nonatomic,strong)UserInfo *userModel;
@end

@implementation OtherUserCenter

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.delegate = self;
    
    UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"OtherUserInfoItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"OtherUserInfoItem"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"UserDesireHeader" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UserDesireHeader"];
    
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    
    self.viewModel =[[userCenterVM alloc] init];
    [self setUpUserInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self navAlpha:0];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self navAlpha:1];
}


- (void)navAlpha:(double)alpha{
    UIView * barBackground = self.navigationController.navigationBar.subviews.firstObject;
    UIImage *img = [UIImage imageNamed:@""];
    
    [self.navigationController.navigationBar setBackgroundImage:img  forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    if (@available(iOS 11.0, *))
    {
        [barBackground.subviews setValue:@(alpha) forKeyPath:@"alpha"];
    } else {
        barBackground.alpha = alpha;
    }
}
-(void)setUpUserInfo
{
    self.viewModel.userId = self.userId;
    RACSignal * signal = self.viewModel.userInfoCommand;
    [signal subscribeNext:^(userInfoAPI* api) {
        if ([api isSuccess]) {
            [[UserManager sharedUserManager] loginSucess:[api.result dictionaryForKey:@"data"]];
            NSMutableDictionary *userInfo = [[NSMutableDictionary dictionaryWithDictionary:[api.result dictionaryForKey:@"data"]] mutableCopy];
            self.userModel = [UserInfo yy_modelWithJSON:userInfo];
            [self.collectionView reloadData];
        }
    } error:^(NSError *error) {
        
    }];
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else{
        return self.userModel.shares.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        OtherUserInfoItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OtherUserInfoItem" forIndexPath:indexPath];
        cell.userModel = self.userModel;
        return cell;
    }else{
        HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
        cell.model = [[HomeArticleModel alloc]initWithDictionary:[self.userModel.shares dictionaryWithIndex:indexPath.row] error:nil];
        return cell;
    }
    
}

#pragma mark <UICollectionViewDelegate>
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(SCREEN_WIDTH,270);
    }else{
        return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,322);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);//分别为上、左、下、右
}


//这个方法是返回 Header的大小 size
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return CGSizeMake(SCREEN_WIDTH, 30);
    }else{
        return CGSizeMake(0, 0);
    }
}

//这个也是最重要的方法 获取Header的 方法。
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UserDesireHeader*view = (UserDesireHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UserDesireHeader" forIndexPath:indexPath];
    view.lblTitle.text =[NSString stringWithFormat:@"%@%@%@",@"分享(", self.userModel.sharesLength,@")"];
    return view;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
            if ([[self.userModel.shares dictionaryWithIndex:indexPath.row] boolForKey:@"groupStatus"]) {
                DetailMangager* vc = [[DetailMangager alloc] init];
                vc.shareId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"shareId"];
                vc.goodsId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                vc.type = @"2";
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                DetailMangager* vc = [[DetailMangager alloc] init];
                vc.shareId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"shareId"];
                vc.goodsId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                vc.type = @"1";
                [self.navigationController pushViewController:vc animated:YES];
            }
    }
}

@end

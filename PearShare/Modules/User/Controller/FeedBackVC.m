//
//  FeedBackVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/5/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "FeedBackVC.h"
#import "MQValidator.h"
#import "FeedAPI.h"
#import "UITextView+ZWPlaceHolder.h"
@interface FeedBackVC ()
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextView *feedText;
@property (weak, nonatomic) IBOutlet UIButton *btnSure;

@end

@implementation FeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"意见与反馈"];
    
    
    self.feedText.placeholder = @"对我们的服务，选品，您还有什么建议吗？您还希望在梨分享上得到什么？请告诉我们，谢谢！";
    
    [[RACSignal combineLatest:@[self.phoneText.rac_textSignal,self.feedText.rac_textSignal]] subscribeNext:^(RACTuple* tuple) {
        if ([MQValidator isPhoneNumber:[tuple objectAtIndex:0]]&&((NSString*)[tuple objectAtIndex:1]).length>0) {
            self.btnSure.enabled = YES;
             self.btnSure.backgroundColor =   [UIColor colorWithHexString:@"FED500"];
        }else{
            self.btnSure.enabled = NO;
            self.btnSure.backgroundColor =  [UIColor colorWithHexString:@"DADADB"];
        }
    }];
}


#pragma mark text代理

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneText) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

- (IBAction)sureCommand:(id)sender
{
    FeedAPI *req = [FeedAPI new];
    req.phone = self.phoneText.text;
    req.content =self.feedText.text;
    
    [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([req isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = NSLocalizedString(@"提交成功!", @"HUD message title");
            [hud hideAnimated:YES afterDelay:2.f];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController popToRootViewControllerAnimated:NO];
            });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [NSString stringWithFormat:@"%@",[req message]];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];

}
@end

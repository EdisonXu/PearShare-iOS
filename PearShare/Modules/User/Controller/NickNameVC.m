//
//  NickNameVC.m
//  QTFootBall_2.6.0
//
//  Created by Edison on 2017/6/22.
//  Copyright © 2017年 Edison. All rights reserved.
//

#import "NickNameVC.h"
#import "NickNameVM.h"
@interface NickNameVC ()<UITextFieldDelegate>
{
    NickNameVM * _viewModel;
}
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UILabel *lblTip;
@property (nonatomic,strong)UserInfo *userModel;
@end

@implementation NickNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewModel = [[NickNameVM alloc]init];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    self.userModel = [UserInfo yy_modelWithJSON:userInfo];
    _textName.delegate = self;
    if (self.type==1) {
        self.title = @"昵称";
         _textName.placeholder =self.userModel.nickname;
    }else{
        self.title = @"签名";
        _textName.placeholder =self.userModel.signature;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveNameCommand:(id)sender {
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    if (self.type==1) {
        [param setObj:_textName.text forKey:@"nickname"];
    }else{
        [param setObj:_textName.text forKey:@"sign"];
    }

    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:[URL_main stringByAppendingString:@"/user/updateInfo"] parameters:nil error:nil];
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
        if ([[responseObject stringForKey:@"success"] isEqualToString:@"1"]) {
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
            if (self.type==1) {
                [userInfo setObj : _textName.text forKey:@"nickname"];
            }else{
                [userInfo setObj : _textName.text forKey:@"signature"];
            }
            kUserDataSet(userInfo);
            [kDefaults synchronize];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
    }] resume];

    
}

#pragma mark - textField代理
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    self.lblTip.text = @"2-12个字符，支持中英文，数字";
    [self.lblTip setTextColor:[UIColor colorWithHexString:@"858585"]];
    NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (text.length>12) {
        return NO;
    }
    return  YES;
    
}
@end

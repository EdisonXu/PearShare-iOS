//
//  UserManager.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/25.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "UserCenter.h"
#import "LoginVC.h"
#import "UserSetting.h"
#import "UserSetting.h"
#import "ProfileVC.h"
#import "HomeItem.h"
#import "UserInfoItem.h"
#import "EmptyPushCell.h"
#import "UserDesireHeader.h"
#import "userCenterVM.h"
#import "userInfoAPI.h"
#import "GoodsDetail.h"
#import "ArticleDetail.h"
#import "TokenAPI.h"
#import "MerchantApply.h"
#import "DetailMangager.h"

#define Cell_Spacing 9

@interface UserCenter ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)userCenterVM * viewModel;
@property (nonatomic,strong)UserInfo *userModel;
@property (nonatomic,strong)NSString * from;
@end

@implementation UserCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    [MobClick event:@"0002"];
    UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"UserInfoItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"UserInfoItem"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"EmptyPushCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"EmptyPushCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"UserDesireHeader" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UserDesireHeader"];
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [rightButton setImage:[UIImage imageNamed:@"user_setting"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(settingCommand)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem*rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
    

    self.viewModel = [[userCenterVM alloc]init];
    
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName: Qnotifiction_MerchanLogin object:nil] subscribeNext:^(NSNotification * x) {
        @strongify(self);
        self.from = [NSString stringWithFormat:@"%@",x.object];
    }];
    

}


- (void)navAlpha:(double)alpha{
    UIView * barBackground = self.navigationController.navigationBar.subviews.firstObject;
    UIImage *img = [UIImage imageNamed:@""];
    
    [self.navigationController.navigationBar setBackgroundImage:img  forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    if (@available(iOS 11.0, *))
    {
        [barBackground.subviews setValue:@(alpha) forKeyPath:@"alpha"];
    } else {
        barBackground.alpha = alpha;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self navAlpha:0];
    
    if (kDefaultLoginAlready) {
        [self setUpUserInfo];
    }else{
        [self loginCommand];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self navAlpha:1];
}

-(void)setUpUserInfo
{
    TokenAPI * api = [TokenAPI new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];

    self.viewModel.userId = kDefaultUserId;
    RACSignal * signal = self.viewModel.userInfoCommand;
    [signal subscribeNext:^(userInfoAPI* api) {
        if ([api isSuccess]) {
            self.collectionView.dataSource              = self;
            self.collectionView.delegate                   = self;
            kUserDataSet([api.result dictionaryForKey:@"data"]);
            NSLog(@"%@",kDefaultUserData);
            [kDefaults synchronize];
            NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
            self.userModel = [UserInfo yy_modelWithJSON:userInfo];
            [self.collectionView reloadData];
            if ([self.from isEqualToString:@"true"]) {
                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
                MerchantApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"MerchantApply"];
                [self.navigationController pushViewController:vc animated:YES];
                self.from = @"false";
            }
        }
    } error:^(NSError *error) {
        
    }];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else{
        if (self.userModel.shares.count) {
            return self.userModel.shares.count;
        }else{
            return 1;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        UserInfoItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UserInfoItem" forIndexPath:indexPath];
        cell.userModel = self.userModel;
        return cell;
    }else{
        if (self.userModel.shares.count) {
            HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
            cell.model = [[HomeArticleModel alloc]initWithDictionary:[self.userModel.shares dictionaryWithIndex:indexPath.row] error:nil];
            return cell;
        }else{
            EmptyPushCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyPushCell" forIndexPath:indexPath];
            return cell;
        }
    }

}

#pragma mark <UICollectionViewDelegate>
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(SCREEN_WIDTH,300);
    }else{
        if (self.userModel.shares.count) {
           return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,322);
        }else{
           return CGSizeMake(SCREEN_WIDTH,SCREEN_HEIGHT-260);
        }
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);//分别为上、左、下、右
}


//这个方法是返回 Header的大小 size
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return CGSizeMake(SCREEN_WIDTH, 30);
    }else{
        return CGSizeMake(0, 0);
    }
}

//这个也是最重要的方法 获取Header的 方法。
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UserDesireHeader*view = (UserDesireHeader *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"UserDesireHeader" forIndexPath:indexPath];
    view.lblTitle.text =[NSString stringWithFormat:@"%@%@%@",@"我的分享(", self.userModel.sharesLength,@")"];
    return view;
}

-(void)settingCommand
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    UserSetting *vc = [storyboard instantiateViewControllerWithIdentifier:@"UserSetting"];
    [self.navigationController pushViewController:vc animated:YES ];
}

-(void)loginCommand
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    vc.form = [NSString stringWithFormat:@"UserCenter"];
    UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
    navi.navigationBarHidden=YES;
    [self.navigationController presentViewController:navi animated:NO completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        if (self.userModel.shares.count) {
            if ([[self.userModel.shares dictionaryWithIndex:indexPath.row] boolForKey:@"groupStatus"]) {
                DetailMangager* vc = [[DetailMangager alloc] init];
                vc.shareId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"shareId"];
                vc.goodsId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                vc.type = @"2";
                [self.navigationController pushViewController:vc animated:YES];
            }else{
                DetailMangager* vc = [[DetailMangager alloc] init];
                vc.shareId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"shareId"];
                vc.goodsId = [[self.userModel.shares dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
                vc.type = @"1";
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }
}

@end

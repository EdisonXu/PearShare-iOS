//
//  UserSetting.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "UserSetting.h"
#import "UIImage+Resize.h"
#import "PersonSettingVM.h"
#import "NickNameVC.h"
#import "ModifyPhoneVC.h"
#import "FeedBackVC.h"
#import "MerchantApply.h"
#import "GoodsApply.h"
#import "PPNetworkHelper.h"

@interface UserSetting ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblNickName;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (nonatomic,strong)PersonSettingVM * viewModel;
@property (weak, nonatomic) IBOutlet UIButton *btnMan;
@property (weak, nonatomic) IBOutlet UIButton *btnWomen;
@property (weak, nonatomic) IBOutlet UILabel *lblSignature;
@property (nonatomic,strong)UserInfo *userModel;
@end

@implementation UserSetting

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"设置"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.viewModel = [[PersonSettingVM alloc] init];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    self.userModel = [UserInfo yy_modelWithJSON:userInfo];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        if (indexPath.row==1) {
            [self ModifyPhone];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                        sleep(1.);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
            });
        }
    }else{
        if (indexPath.row==1) {
            [self MerchatCommand];
        }
        if (indexPath.row==2) {
            [self feedBack];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section==0) {
        return 8;
    }else{
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1&&indexPath.row==1) {
        if ([self.userModel.type isEqualToString:@"2"]){
            return 0.1;
        }else{
            return 45;
        }
    }else{
        return 45;
    }
}

-(void)MerchatCommand
{
    if ([self.userModel.type isEqualToString:@"0"]){
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
        MerchantApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"MerchantApply"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
        GoodsApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoodsApply"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)ModifyPhone
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    ModifyPhoneVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ModifyPhoneVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)feedBack
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    FeedBackVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"FeedBackVC"];
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)loginOut:(id)sender {
    NSString * title =[NSString stringWithFormat:@"是否退出登陆"];
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * signAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
        kUserDataRemove;
    }];
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:signAction];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end

//
//  UserDesireHeader.h
//  PearShare
//
//  Created by Destiny on 2018/6/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserDesireHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

//
//  UserInfoItem.m
//  PearShare
//
//  Created by Destiny on 2018/6/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "UserInfoItem.h"
#import "ProfileVC.h"
#import "MySupplyVC.h"
#import "IntegralManager.h"
#import "MyAttentionList.h"
@interface UserInfoItem()

@property (nonatomic,strong)UserInfo *user;
@end
@implementation UserInfoItem

-(void)setUserModel:(UserInfo *)userModel
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    self.user = [UserInfo yy_modelWithJSON:userInfo];
    
    if (userModel.signature.length>0) {
        self.lblSign.text = userModel.signature;
    }else{
        self.lblSign.text = @"一句话介绍自己，这里可以有26个字";
    }
    self.lblName.text = userModel.nickname;
    self.lblFans.text = userModel.fansNum;
    self.lblFocus.text =userModel.focusNum;
    if ([userModel.type isEqualToString:@"0"]) {
        self.cardHeight.constant = 99;
    }else if ([userModel.type isEqualToString:@"1"]){
        self.cardHeight.constant = 132;
        self.lblMerchant.text = @"你已申请成为供应商，请耐心等待";
    }else{
        self.cardHeight.constant = 132;
        self.lblMerchant.text = @"去供应商品>>";
    }
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageNamed:@"logo"]];
}


- (IBAction)PersonSetting:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    ProfileVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    [self.viewController.navigationController pushViewController:vc animated:YES ];
}

- (IBAction)supplyListCommand:(id)sender
{
    if ([self.user.type isEqualToString:@"2"]) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
        MySupplyVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"MySupplyVC"];
        [self.viewController.navigationController pushViewController:vc animated:YES ];
    }
}
- (IBAction)jifen:(id)sender
{
    IntegralManager *vc =[[IntegralManager alloc] init];
    [self.viewController.navigationController pushViewController:vc animated:YES ];
}
- (IBAction)attentionList:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    MyAttentionList *vc = [storyboard instantiateViewControllerWithIdentifier:@"MyAttentionList"];
    [self.viewController.navigationController pushViewController:vc animated:YES ];
}
@end

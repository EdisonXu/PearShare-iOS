//
//  MyAttentionCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MyAttentionCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (nonatomic,strong)NSDictionary * data;
@property (weak, nonatomic) IBOutlet UIButton *btnAttention;
@end

//
//  OtherUserInfoItem.h
//  PearShare
//
//  Created by Destiny on 2018/6/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherUserInfoItem : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (nonatomic,strong)UserInfo *userModel;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblSig;
@property (weak, nonatomic) IBOutlet UILabel *lblFocus;
@property (weak, nonatomic) IBOutlet UILabel *lblFans;
@property (weak, nonatomic) IBOutlet UIButton *btnAttention;
@end

//
//  OtherUserInfoItem.m
//  PearShare
//
//  Created by Destiny on 2018/6/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "OtherUserInfoItem.h"
#import "FoucsAPI.h"
@interface OtherUserInfoItem()
{
    UserInfo*_itemInfo;
}
@property (nonatomic,assign)BOOL  isFollow;
@end
@implementation OtherUserInfoItem

-(void)setUserModel:(UserInfo *)userModel
{
    _itemInfo = [[UserInfo alloc]init];
    self.isFollow =  [userModel.isFollow boolValue];
    _itemInfo = userModel;
    self.lblName.text = userModel.nickname;
    self.lblFans.text = userModel.fansNum;
    self.lblFocus.text =userModel.focusNum;
    if (userModel.signature.length>0) {
        self.lblSig.text = userModel.signature;
    }else{
        self.lblSig.text = @"一句话介绍自己，这里可以有26个字";
    }

    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:[UIImage imageNamed:@"logo"]];
    
    if ([userModel.isFollow boolValue]) {
        [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"F2F2F2"]];
        [self.btnAttention setTitle:@"已关注" forState:UIControlStateNormal];
        self.btnAttention.selected = YES;
    }else{
        [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"FED500"]];
        [self.btnAttention setTitle:@"+ 关注" forState:UIControlStateNormal];
        self.btnAttention.selected = NO;
    }
}

- (IBAction)attentionCommand:(UIButton*)sender
{
    sender.enabled = NO;
    FoucsAPI *req = [FoucsAPI new];
    req.fansId = _itemInfo.userId;
    req.fansType = [NSString stringWithFormat:@"%d",self.isFollow];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    [req startWithCompletionBlockWithSuccess:^(FoucsAPI* request) {
        sender.enabled = YES;
        [hud hideAnimated:YES];
        if (self.isFollow) {
            [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"FED500"]];
            [self.btnAttention setTitle:@"+ 关注" forState:UIControlStateNormal];
            self.isFollow  =NO;
        }else{
            [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"F2F2F2"]];
            [self.btnAttention setTitle:@"已关注" forState:UIControlStateNormal];
            self.isFollow  =YES;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        sender.enabled = YES;
        [hud hideAnimated:YES];
    }];
}
@end

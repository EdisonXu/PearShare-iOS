//
//  UserInfoItem.h
//  PearShare
//
//  Created by Destiny on 2018/6/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserInfoItem : UICollectionViewCell
@property (nonatomic,strong)UserInfo *userModel;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblSign;
@property (weak, nonatomic) IBOutlet UILabel *lblFocus;
@property (weak, nonatomic) IBOutlet UILabel *lblFans;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cardHeight;
@property (weak, nonatomic) IBOutlet UILabel *lblMerchant;
@end

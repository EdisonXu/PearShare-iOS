//
//  MyAttentionCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MyAttentionCell.h"
#import "OtherUserCenter.h"
@interface MyAttentionCell()
@property (nonatomic,strong)NSString * userId;
@end
@implementation MyAttentionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(NSDictionary *)data
{
    
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[data stringForKey:@"avatar"]]];
    self.lblName.text = [data stringForKey:@"nickname"];
    self.lblCount.text = [NSString stringWithFormat:@"%@%@",[data stringForKey:@"fansNum"],@"个粉丝"];
    
    if ([data boolForKey:@"isTwoFans"]) {
        [self.btnAttention setTitle:@"互相关注" forState:UIControlStateNormal];
    }else{
        [self.btnAttention setTitle:@"关注" forState:UIControlStateNormal];
    }
    self.userId = [data stringForKey:@"userId"];
    
}
- (IBAction)userCommand:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
    OtherUserCenter *vc = [storyboard instantiateViewControllerWithIdentifier:@"OtherUserCenter"];
    vc.userId = self.userId;
    [self.viewController.navigationController pushViewController:vc animated:YES ];
    
}

@end

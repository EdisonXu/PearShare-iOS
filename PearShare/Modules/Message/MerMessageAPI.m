//
//  MerMessageAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MerMessageAPI.h"

@implementation MerMessageAPI
-(NSString *)requestUrl{
    return @"msg/list/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.pageNum forKey:@"pageNum"];
    [param setString:self.pageSize forKey:@"pageSize"];
    [param setString:self.type forKey:@"type"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    if (kDefaultLoginAlready) {
        [param setString:kDefaultUserId forKey:@"userId"];
        [param setString:kDefaultUserToken forKey:@"token"];
    }
    return param;
}
@end

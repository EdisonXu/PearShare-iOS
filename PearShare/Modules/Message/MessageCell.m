//
//  MessageCell.m
//  PearShare
//
//  Created by Destiny on 2018/5/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    self.lblMessage.text = [model stringForKey:@"comment"];
    self.lblTime.text =[self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
}
@end

//
//  MessCommendCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MessCommendCell : BaseTableViewCell
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imageFav;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCommend;
@property (weak, nonatomic) IBOutlet UILabel *lblFav;
@end

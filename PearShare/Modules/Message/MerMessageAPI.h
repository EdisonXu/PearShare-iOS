//
//  MerMessageAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface MerMessageAPI : BaseRequestAPI
@property (nonatomic,strong)NSString *pageNum;
@property (nonatomic,strong)NSString *pageSize;
@property (nonatomic,strong)NSString *type;
@end

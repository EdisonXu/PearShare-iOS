//
//  NewMessageAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "NewMessageAPI.h"

@implementation NewMessageAPI
-(NSString *)requestUrl{
    return @"msg/status/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    if (kDefaultLoginAlready) {
        [param setString:kDefaultUserId forKey:@"userId"];
        [param setString:kDefaultUserToken forKey:@"token"];
    }
    return param;
}

@end

//
//  MessageVM.m
//  PearShare
//
//  Created by Destiny on 2018/5/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageVM.h"
#import "MerMessageAPI.h"
#import "NewMessageAPI.h"
@implementation MessageVM
-(RACSignal *)messageCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        MerMessageAPI *req = [MerMessageAPI new];
        req.pageNum =[NSString stringWithFormat:@"%ld",(long)self.page];
        req.pageSize = self.pageSize;
        req.type = self.type;
        
        [req startWithCompletionBlockWithSuccess:^(MerMessageAPI * _Nonnull request) {
            [subscriber sendNext:[request.result arrayForKey:@"data"]];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal *)newMessage
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NewMessageAPI *req = [NewMessageAPI new];
        [req startWithCompletionBlockWithSuccess:^(NewMessageAPI * _Nonnull request) {
            [subscriber sendNext:[[request.result dictionaryForKey:@"data"]arrayForKey:@"status"]];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

//
//  MessageFavCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageFavCell.h"

@implementation MessageFavCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    [self.imageFav sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"shareCover"]]];
    
    self.lblName.text = [model stringForKey:@"fromUserNickname"];
    
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]] ];
    
    self.lblCount.text = [NSString stringWithFormat:@"等%@%@",[model stringForKey:@"collectionsNum"],@"个人想和你一起买"];
    
    
}



@end

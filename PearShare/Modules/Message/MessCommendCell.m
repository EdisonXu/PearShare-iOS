//
//  MessCommendCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessCommendCell.h"

@implementation MessCommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]]];
    [self.imageFav sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"shareCover"]]];
    self.lblName.text = [model stringForKey:@"fromUserNickname"];
    self.lblCommend.text = [model stringForKey:@"comment"];
    self.lblTime.text = [self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
    
    if ([[model stringForKey:@"msgType"] isEqualToString:@"3"]) {
        self.lblFav.text = @"赞了你的评论";
        self.lblCommend.text = [model stringForKey:@"comment"];
    }else{
        self.lblFav.text = @"";
        self.lblCommend.text = [NSString stringWithFormat:@"%@%@",@"回复您：",[model stringForKey:@"comment"]];
    }
    
}

@end

//
//  MessageOffcialCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MessageOffcialCell : BaseTableViewCell
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblConent;
@property (weak, nonatomic) IBOutlet UIImageView *imageMessage;
@end

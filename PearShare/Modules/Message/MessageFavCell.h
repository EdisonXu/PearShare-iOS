//
//  MessageFavCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface MessageFavCell : BaseTableViewCell
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imageFav;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@end

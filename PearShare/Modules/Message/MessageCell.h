//
//  MessageCell.h
//  PearShare
//
//  Created by Destiny on 2018/5/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface MessageCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (nonatomic,strong)NSDictionary * model;
@end

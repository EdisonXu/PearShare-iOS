//
//  MessageOffcialCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/16.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageOffcialCell.h"

@implementation MessageOffcialCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    [self.imageMessage sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"img"]]];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]]];
    self.lblTime.text = [self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
    self.lblConent.text = [model stringForKey:@"comment"];
    self.lblName.text = [model stringForKey:@"fromUserNickname"];
}

@end

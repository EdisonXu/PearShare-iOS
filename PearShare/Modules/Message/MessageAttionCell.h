//
//  MessageAttionCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"
@interface MessageAttionCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (nonatomic,strong)NSDictionary * model;
@property (weak, nonatomic) IBOutlet UIButton *btnAttention;
@end

//
//  MessageCenter.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageCenter.h"
#import "BaseRequestAPI.h"
#import "MessageVM.h"
#import "NewMessageAPI.h"
#import "MerMessageList.h"
@interface MessageCenter ()
{
    UserInfo* _info;
}
@property (weak, nonatomic) IBOutlet UIView *redWant;
@property (weak, nonatomic) IBOutlet UIView *redCommend;
@property (weak, nonatomic) IBOutlet UIView *redApp;
@property (weak, nonatomic) IBOutlet UIView *redMerant;
@end

@implementation MessageCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息";
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self setUpMessageCommand];
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:kDefaultUserData];
    _info = [UserInfo yy_modelWithJSON:userInfo];
}

-(void)setUpMessageCommand
{
    MessageVM * viewModel = [[MessageVM alloc] init];
    RACSignal * signal = viewModel.newMessage;
    [signal subscribeNext:^(NSArray* list) {
        [self setUpRedWith:list];
    } error:^(NSError *error) {
        
    }];
}

-(void)setUpRedWith:(NSArray*)list
{
    if (list.count) {
        if ([list boolWithIndex:0]) {
            self.redWant.hidden= NO;
        }else{
            self.redWant.hidden= YES;
        }
        
        if ([list boolWithIndex:1]) {
            self.redCommend.hidden= NO;
        }else{
            self.redCommend.hidden= YES;
        }
        
        if ([list boolWithIndex:2]) {
            self.redApp.hidden= NO;
        }else{
            self.redApp.hidden= YES;
        }
        
        if ([list boolWithIndex:3]) {
            self.redMerant.hidden= NO;
        }else{
            self.redMerant.hidden= YES;
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        MerMessageList *vc = [[MerMessageList alloc] init];
        vc.type = @"0";
        vc.titleName = @"想买和关注";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row==1){
        MerMessageList *vc = [[MerMessageList alloc] init];
        vc.type = @"1";
        vc.titleName = @"评论和赞";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row==2){
        MerMessageList *vc = [[MerMessageList alloc] init];
        vc.type = @"2";
        vc.titleName = @"通知消息";
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MerMessageList *vc = [[MerMessageList alloc] init];
        vc.type = @"3";
        vc.titleName = @"商户消息";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==3) {
        if (kDefaultLoginAlready&&[_info.type isEqualToString:@"2"]) {
            return 44;
        }else{
            return 0.01;
        }
    }else{
        return 44;
    }
}
@end

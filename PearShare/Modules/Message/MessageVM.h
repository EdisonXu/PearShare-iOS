//
//  MessageVM.h
//  PearShare
//
//  Created by Destiny on 2018/5/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface MessageVM : BaseViewModel
@property (nonatomic,strong)NSString * type;
-(RACSignal *)messageCommand;
-(RACSignal *)newMessage;
@end

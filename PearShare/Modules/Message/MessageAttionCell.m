//
//  MessageAttionCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MessageAttionCell.h"
#import "FoucsAPI.h"
@interface MessageAttionCell()
@property (nonatomic,strong)NSDictionary * itemModel;
@property (nonatomic,assign)BOOL  isFollow;
@end
@implementation MessageAttionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
     self.itemModel = [NSDictionary dictionaryWithDictionary:model];
     self.isFollow =  [[model stringForKey:@"isFocus"] boolValue];
    [self.imageHeader sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"fromUserAvatar"]]];
    self.lblName.text = [model stringForKey:@"fromUserNickname"];
    self.lblTime.text =[self getDateStringWithTimeStr:[model stringForKey:@"createTime"]];
    if ([model boolForKey:@"isFocus"]) {
        [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"F2F2F2"]];
        [self.btnAttention setTitle:@"已关注" forState:UIControlStateNormal];
    }else{
        [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"FED500"]];
        [self.btnAttention setTitle:@"+ 关注" forState:UIControlStateNormal];
    }
}

- (IBAction)attentionCommand:(UIButton*)sender
{
    sender.enabled = NO;
    FoucsAPI *req = [FoucsAPI new];
    req.fansId = [self.itemModel stringForKey:@"fromUserId"];
    req.fansType = [NSString stringWithFormat:@"%d",self.isFollow];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    [req startWithCompletionBlockWithSuccess:^(FoucsAPI* request) {
        sender.enabled = YES;
        [hud hideAnimated:YES];
        if (self.isFollow) {
            [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"FED500"]];
            [self.btnAttention setTitle:@"+ 关注" forState:UIControlStateNormal];
            self.isFollow  =NO;
        }else{
            [self.btnAttention setBackgroundColor:[UIColor colorWithHexString:@"F2F2F2"]];
            [self.btnAttention setTitle:@"已关注" forState:UIControlStateNormal];
            self.isFollow  =YES;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        sender.enabled = YES;
        [hud hideAnimated:YES];
    }];
    
}
@end

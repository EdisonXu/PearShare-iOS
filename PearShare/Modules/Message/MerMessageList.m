//
//  MerMessageList.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MerMessageList.h"
#import "MessageAttionCell.h"
#import "MessageCell.h"
#import "EmptyCell.h"
#import "MessageVM.h"
#import "MerMessageAPI.h"
#import "MessageFavCell.h"
#import "MessCommendCell.h"
#import "MessageOffcialCell.h"
@interface MerMessageList ()
@property (nonatomic,strong)MessageVM *viewModel;
@property (nonatomic,strong)NSMutableArray* messageList;
@end

@implementation MerMessageList

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}

-(void)setUpView
{
    self.title = [NSString stringWithFormat:@"%@",_titleName];
    self.viewModel = [[MessageVM alloc]init];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerNib:MessageCell.nib forCellReuseIdentifier:@"MessageCell"];
    [self.tableView registerNib:EmptyCell.nib forCellReuseIdentifier:@"EmptyCell"];
    [self.tableView registerNib:MessageAttionCell.nib forCellReuseIdentifier:@"MessageAttionCell"];
    [self.tableView registerNib:MessageFavCell.nib forCellReuseIdentifier:@"MessageFavCell"];
    [self.tableView registerNib:MessCommendCell.nib forCellReuseIdentifier:@"MessCommendCell"];
    [self.tableView registerNib:MessageOffcialCell.nib forCellReuseIdentifier:@"MessageOffcialCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self messageCommand];
    }];
    [self.tableView.mj_header beginRefreshing];
}

-(void)messageCommand
{
    self.viewModel.page = 1;
    self.viewModel.type = self.type;
    RACSignal * signal = self.viewModel.messageCommand;
    [signal subscribeNext:^(NSArray* list) {
        self.messageList = [[NSMutableArray arrayWithArray:list] mutableCopy];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.messageList.count>0) {
        return self.messageList.count;
    }else{
        return 1;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.messageList.count>0) {
        if ([self.type isEqualToString:@"0"]) {
            if ([[[self.messageList dictionaryWithIndex:indexPath.row] stringForKey:@"msgType"] isEqualToString:@"1"]) {
                MessageAttionCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MessageAttionCell" forIndexPath:indexPath];
                cell.model = [self.messageList dictionaryWithIndex:indexPath.row];
                return cell;
            }else{
                MessageFavCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MessageFavCell" forIndexPath:indexPath];
                cell.model = [self.messageList dictionaryWithIndex:indexPath.row];
                return cell;
            }
        }else if ([self.type isEqualToString:@"1"]){
            MessCommendCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MessCommendCell" forIndexPath:indexPath];
            cell.model = [self.messageList dictionaryWithIndex:indexPath.row];
            return cell;
        }else if ([self.type isEqualToString:@"2"]){
            MessageOffcialCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MessageOffcialCell" forIndexPath:indexPath];
            cell.model = [self.messageList dictionaryWithIndex:indexPath.row];
            return cell;
        }else{
            MessageCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MessageCell" forIndexPath:indexPath];
            cell.model = [self.messageList dictionaryWithIndex:indexPath.row];
            return cell;
        }
    }else{
        EmptyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.messageList.count>0) {
        if ([self.type isEqualToString:@"0"]) {
             if ([[[self.messageList dictionaryWithIndex:indexPath.row] stringForKey:@"msgType"] isEqualToString:@"1"]) {
                 return 70;
             }else{
                 return 91;
             }
        }else if ([self.type isEqualToString:@"1"]){
            return 91;
        }else if ([self.type isEqualToString:@"3"]){
            return 156;
        }else{
            return UITableViewAutomaticDimension;
        }
    }else{
        return self.tableView.frame.size.height;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
@end

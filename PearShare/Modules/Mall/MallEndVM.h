//
//  MallEndVM.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModel.h"
@interface MallEndVM : BaseViewModel
-(RACSignal*)mallEndCommand;
@end

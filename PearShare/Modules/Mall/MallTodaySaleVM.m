//
//  MallTodaySaleVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallTodaySaleVM.h"

@implementation MallTodaySaleVM
-(RACSignal*)advertisementCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking getWithUrl:[baseRequestUrl stringByAppendingString:@"advert/list"] refreshCache:YES params:nil success:^(NSDictionary* response) {
            [subscriber sendNext:[response arrayForKey:@"data"]];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)todayNewSaleCommand
{
//    [self.param setObj:@"20" forKey:@"pagesize"];
    [self.param setInteger:self.page forKey:@"pageno"];
    
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking getWithUrl:[baseRequestUrl stringByAppendingString:@"goods/getTodayNew"] refreshCache:YES params:self.param success:^(NSDictionary* response) {
            [subscriber sendNext:[response dictionaryForKey:@"data"]];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

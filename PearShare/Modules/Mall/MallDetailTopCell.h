//
//  MallDetailTopCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/23.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "UICustomLineLabel.h"
@interface MallDetailTopCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *coverImage;
@property (nonatomic,strong)NSDictionary*model;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNow;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeSale;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@end

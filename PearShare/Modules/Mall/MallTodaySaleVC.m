//
//  MallTodaySaleVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallTodaySaleVC.h"
#import "MallTodayAdCell.h"
#import "MallTodaySaleVM.h"
#import "GoodsModel.h"
#import "HotSaleCell.h"
#import "TitleCell.h"
#import "GoodsItem.h"
#import "MallDetailVC.h"
@interface MallTodaySaleVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)MallTodaySaleVM *viewModel;
@property (nonatomic,strong)NSMutableArray *adList;
@property (nonatomic,strong)NSMutableArray *recommandMList;
@property (nonatomic,strong)NSMutableArray *recommandNList;
@property (nonatomic,strong)NSMutableArray *recommandEList;
@property (nonatomic,strong)NSMutableArray *recommendList;

@end

#define Cell_Spacing 9

@implementation MallTodaySaleVC

static NSString * const adCell                            = @"MallTodayAdCell";
static NSString * const hotCell                           = @"HotSaleCell";
static NSString * const titleCell                          = @"TitleCell";
static NSString * const goodsCell                       = @"GoodsItem";

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        /** 创建布局参数 */
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
        [self.collectionView setCollectionViewLayout:flowLayout];
        /** 注册cell可重用ID */
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:adCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:hotCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:titleCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:goodsCell];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    self.viewModel = [[MallTodaySaleVM alloc]init];
    [self setUpAdvertisement];
//    [self setUpGoods];
    @weakify(self);
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpGoods];
    }];
    
//    self.collectionView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
//        @strongify(self);
//        [self loadMoreData];
//    }];
    
    [self.collectionView.mj_header beginRefreshing];
}


-(void)setUpAdvertisement
{
    @weakify(self);
    RACSignal * signal = self.viewModel.advertisementCommand;
    [signal subscribeNext:^(NSArray* list) {
        @strongify(self);
        self.adList = [[[NSMutableArray alloc] initWithArray:list] mutableCopy];
        for (int i = 0; i<list.count; i++) {
            if ([[[list dictionaryWithIndex:i] stringForKey:@"postion"]isEqualToString:@"2" ]) {
                NSString* adStr = [PinImageUrl stringByAppendingString:[[list dictionaryWithIndex:i] stringForKey:@"face"]];
                [self.adList addObject:adStr];
            }
        }
    } error:^(NSError *error) {
        
    }];
}

-(void)setUpGoods
{
    @weakify(self);
    self.viewModel.page = 1;
    RACSignal * signal = self.viewModel.todayNewSaleCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        @strongify(self);
        self.recommendList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listGoodsRecommend"]] mutableCopy];
        self.recommandEList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNowtwenty"]] mutableCopy];
        self.recommandNList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNowfourteen"]] mutableCopy];
        self.recommandMList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNowten"]] mutableCopy];
        [self setUpModelList:self.recommandEList];
        [self setUpModelList:self.recommandNList];
        [self setUpModelList:self.recommandMList];
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)setUpModelList:(NSMutableArray*)list
{
    NSMutableArray* newList = [[NSMutableArray alloc]init];
    newList = [list mutableCopy];
    [list removeAllObjects];
    
    for (int i =0; i<newList.count; i++) {
        GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[newList dictionaryWithIndex:i] error:nil];
        [list addObject:model];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 8;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return self.recommendList.count;
    }else if (section==2){
        return 1;
    }else if (section==3){
        return self.recommandMList.count;
    }else if (section==4){
        return 1;
    }else if (section==5){
        return self.recommandNList.count;
    }else if (section==6){
        return 1;
    }else{
        return self.recommandEList.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        MallTodayAdCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:adCell forIndexPath:indexPath];
        cell.adList = self.adList;
        return cell;
    }else if (indexPath.section==1){
        HotSaleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:hotCell forIndexPath:indexPath];
        cell.data = [self.recommendList dictionaryWithIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==2){
        TitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"上午 10:00";
        return cell;
    }else if (indexPath.section==3){
        GoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:goodsCell forIndexPath:indexPath];
        cell.goodsModel = [self.recommandMList objectAtIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==4){
        TitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"下午 2:00";
        return cell;
    }else if (indexPath.section==5){
        GoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:goodsCell forIndexPath:indexPath];
        cell.goodsModel = [self.recommandNList objectAtIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==6){
        TitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"晚上 8:00";
        return cell;
    }else{
        GoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:goodsCell forIndexPath:indexPath];
        cell.goodsModel = [self.recommandEList objectAtIndex:indexPath.row];
        return cell;
    }
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(SCREEN_WIDTH,160);
    }else if (indexPath.section==1){
         return CGSizeMake(SCREEN_WIDTH,110);
    }else if (indexPath.section==2){
        return CGSizeMake(SCREEN_WIDTH,30);
    }else if (indexPath.section==3){
         return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,321);
    }else if (indexPath.section==4){
        return CGSizeMake(SCREEN_WIDTH,30);
    }else if (indexPath.section==5){
         return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,321);
    }else if (indexPath.section==6){
         return CGSizeMake(SCREEN_WIDTH,30);
    }else{
       return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,321);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==3||section==5||section==7) {
        return Cell_Spacing;
    }else{
        return 0;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==3||section==5||section==7) {
        return Cell_Spacing;
    }else{
        return 0;
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section==3||section==5||section==7) {
        return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);
    }else{
        return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = [[self.recommendList dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==3){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandMList objectAtIndex:indexPath.row]).goodsId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==5){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandNList objectAtIndex:indexPath.row]).goodsId;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==7){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandEList objectAtIndex:indexPath.row]).goodsId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end

//
//  AnnounceGoodsCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/20.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AnnounceGoodsCell.h"

@implementation AnnounceGoodsCell
-(void)setGoodsModel:(GoodsModel *)goodsModel
{
    self.lblTitle.text = goodsModel.title;
    [self.imageFace sd_setImageWithURL:[NSURL URLWithString: [[PinImageUrl stringByAppendingString:goodsModel.face] stringByAppendingString:@"/480"]]];
    self.lblPriceOld.text = [NSString stringWithFormat:@"%@%@",@"¥ ", goodsModel.originalPrice];
    self.lblPriceNow.text =[NSString stringWithFormat:@"%@%@",@"¥", goodsModel.currentPrice];
    self.lblPriceOld.lineType = LineTypeMiddle;
}
@end


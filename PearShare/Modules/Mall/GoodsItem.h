//
//  GoodsItem.h
//  PomeloChoose
//
//  Created by Destiny on 2018/1/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsModel.h"
#import "UICustomLineLabel.h"
@interface GoodsItem : BaseCollectionCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNew;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;

@property (nonatomic, nonatomic) GoodsModel *goodsModel;
@end

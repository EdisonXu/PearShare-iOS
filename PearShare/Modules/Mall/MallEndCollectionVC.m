//
//  MallEndCollectionVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallEndCollectionVC.h"
#import "GoodsItem.h"
#import "MallEndVM.h"
#import "GoodsModel.h"
#import "MallDetailVC.h"

#define Cell_Spacing 9
@interface MallEndCollectionVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)MallEndVM *viewModel;
@property (nonatomic,strong)NSMutableArray *goodsList;
@property (nonatomic,strong)NSMutableArray *modelList;
@end

@implementation MallEndCollectionVC
static NSString * const cellreuseIdentifier         = @"GoodsItem";

#pragma mark-lifecyle

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        /** 创建布局参数 */
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
        [self.collectionView setCollectionViewLayout:flowLayout];
        /** 注册cell可重用ID */
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cellreuseIdentifier];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    self.viewModel = [[MallEndVM alloc]init];
    self.modelList = [[NSMutableArray alloc]init];
    
    @weakify(self);
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
    @strongify(self);
        [self setUpEndData];
    }];
    
    self.collectionView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadMoreData];
    }];
    
    [self.collectionView.mj_header beginRefreshing];
}

-(void)setUpEndData
{
    self.viewModel.page = 1;
    RACSignal * signal = self.viewModel.mallEndCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        NSArray* goodsList = [data arrayForKey:@"data"];
        [self setUpModelList:goodsList];
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)loadMoreData
{
    [self.viewModel addPage];
    RACSignal * signal = self.viewModel.mallEndCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        NSArray* goodsList = [data arrayForKey:@"data"];
        if (goodsList.count>0) {
            for (int i =0; i<goodsList.count; i++) {
                GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[goodsList dictionaryWithIndex:i] error:nil];
                [self.modelList addObject:model];
            }
        }
        [self.collectionView reloadData];
        [self.collectionView.mj_footer endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

-(void)setUpModelList:(NSArray*)list
{
    if (list.count>0) {
        self.modelList = [NSMutableArray array];
        for (int i =0; i<list.count; i++) {
            GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[list dictionaryWithIndex:i] error:nil];
            [self.modelList addObject:model];
        }
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.modelList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellreuseIdentifier forIndexPath:indexPath];
    cell.goodsModel = [self.modelList objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,321);
}

 - (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);//分别为上、左、下、右
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MallDetailVC *vc = [MallDetailVC new];
    vc.goodsId = ((GoodsModel*)[self.modelList objectAtIndex:indexPath.row]).goodsId;
    [self.navigationController pushViewController:vc animated:YES];
}

@end

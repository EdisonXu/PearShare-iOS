//
//  HotTopAdCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface HotTopAdCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *adView;
@property (nonatomic,strong)NSMutableArray * adList;
@end

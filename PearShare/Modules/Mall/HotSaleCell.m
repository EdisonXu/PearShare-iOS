//
//  HotSaleCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HotSaleCell.h"
//#import <AlibcTradeBiz/AlibcTradeBiz.h>
//#import <AlibcTradeSDK/AlibcTradeSDK.h>
//#import <AlibabaAuthSDK/albbsdk.h>
@interface HotSaleCell()
@property (nonatomic,strong)NSMutableDictionary * model;
@end
@implementation HotSaleCell

-(void)setData:(NSDictionary *)data
{
    self.model =[NSMutableDictionary dictionaryWithDictionary:data];
    self.lblTitle.text = [data stringForKey:@"title"];
    self.lblPriceOld.text = [NSString stringWithFormat:@"%@%@",@"¥ ", [data stringForKey:@"originalPrice"]];
    self.lblPreiceNew.text =[NSString stringWithFormat:@"%@%@",@"¥", [data stringForKey:@"currentPrice"]];
    self.lblPriceOld.lineType = LineTypeMiddle;
    
    if ([data stringForKey:@"face"].length>0) {
        [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[[PinImageUrl stringByAppendingString:[data stringForKey:@"face"]] stringByAppendingString:@"/480"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    }else{
        [self.imageCover setImage:[UIImage imageNamed:@"mall_empty"]];
    }
}

- (IBAction)buyCommand:(id)sender
{
  
//    id<AlibcTradePage> page = [AlibcTradePageFactory page:[self.model stringForKey:@"url"]];
//    //淘客信息
//    AlibcTradeTaokeParams *taoKeParams=[[AlibcTradeTaokeParams alloc] init];
//    taoKeParams.pid= @"mm_44889980_43066955_279234503";
//    //打开方式
//    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
//    showParam.openType = AlibcOpenTypeNative;
//    [[AlibcTradeSDK sharedInstance] .tradeService show:[self getCurrentVC].navigationController page:page showParams:showParam taoKeParams:taoKeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
}

@end


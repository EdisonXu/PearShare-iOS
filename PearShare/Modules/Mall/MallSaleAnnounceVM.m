//
//  MallSaleAnnounceVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/20.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallSaleAnnounceVM.h"

@implementation MallSaleAnnounceVM
-(RACSignal*)saleAnnounceCommand
{
    [self.param setObj:@"20" forKey:@"pagesize"];
    [self.param setObj:@"1" forKey:@"pageno"];
    
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking getWithUrl:[baseRequestUrl stringByAppendingString:@"goods/getNewForecast"] refreshCache:YES params:self.param success:^(NSDictionary* response) {
            [subscriber sendNext:[response dictionaryForKey:@"data"]];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

//
//  HotSaleCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomLineLabel.h"
@interface HotSaleCell : BaseCollectionCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPreiceNew;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@property (nonatomic,strong)NSDictionary * data;
@end

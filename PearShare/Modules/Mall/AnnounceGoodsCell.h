//
//  AnnounceGoodsCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/20.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsModel.h"
#import "UICustomLineLabel.h"
@interface AnnounceGoodsCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *imageFace;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNow;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@property (nonatomic,strong)GoodsModel* goodsModel;

@end

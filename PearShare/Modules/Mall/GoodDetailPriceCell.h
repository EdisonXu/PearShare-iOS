//
//  GoodDetailPriceCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/24.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomLineLabel.h"
#import "SDCycleScrollView.h"
@interface GoodDetailPriceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *coverImage;
@property (nonatomic,strong)NSDictionary*model;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNow;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeSale;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@property (weak, nonatomic) IBOutlet UIView *countView;
@end

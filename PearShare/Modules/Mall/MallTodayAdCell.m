//
//  MallTodayAdCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallTodayAdCell.h"

@implementation MallTodayAdCell
-(void)setAdList:(NSMutableArray *)adList
{
    self.adView.imageURLStringsGroup = adList;
}
@end

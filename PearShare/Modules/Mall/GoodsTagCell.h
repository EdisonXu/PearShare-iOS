//
//  GoodsTagCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/24.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol SelectCollectionCellDelegate <NSObject>
//- (void)selectButttonClick:(CXSearchCollectionViewCell *)cell;
//@end

@interface GoodsTagCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *contentButton;

//@property (weak, nonatomic) id<SelectCollectionCellDelegate> selectDelegate;
+ (CGSize) getSizeWithText:(NSString*)text;
@end

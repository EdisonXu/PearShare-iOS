//
//  MallSaleAnnounceVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/20.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallSaleAnnounceVC.h"
#import "MallSaleAnnounceVM.h"
#import "AnnounceTitleCell.h"
#import "AnnounceGoodsCell.h"
#import "AnnounceRemindCell.h"
#import "GoodsModel.h"
#import "MallDetailVC.h"
#define Cell_Spacing 9
@interface MallSaleAnnounceVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)MallSaleAnnounceVM* viewModel;

@property (nonatomic,strong)NSMutableArray *recommandMList;
@property (nonatomic,strong)NSMutableArray *recommandNList;
@property (nonatomic,strong)NSMutableArray *recommandEList;
@property (nonatomic,strong)NSMutableArray *recommendList;

@end

@implementation MallSaleAnnounceVC

static NSString * const titleCell                           = @"AnnounceTitleCell";
static NSString * const goodsCell                        = @"AnnounceGoodsCell";
static NSString * const remindCell                       = @"AnnounceRemindCell";

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        /** 创建布局参数 */
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
        [self.collectionView setCollectionViewLayout:flowLayout];
        
        /** 注册cell可重用ID */
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:titleCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:goodsCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:remindCell];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    self.viewModel = [[MallSaleAnnounceVM alloc]init];
    @weakify(self);
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpGoods];
    }];
    [self.collectionView.mj_header beginRefreshing];
}


-(void)setUpGoods
{
    @weakify(self);
    RACSignal * signal = self.viewModel.saleAnnounceCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        @strongify(self);
    self.recommendList    = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listGoodsRecommend"]] mutableCopy];
    self.recommandMList  = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNewten"]] mutableCopy];
    self.recommandNList  = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNewfourteen"]] mutableCopy];
    self.recommandEList  = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listNewtwenty"]] mutableCopy];
    [self setUpModelList:self.recommendList];
    [self setUpModelList:self.recommandMList];
    [self setUpModelList:self.recommandNList];
    [self setUpModelList:self.recommandEList];
    [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)setUpModelList:(NSMutableArray*)list
{
    NSMutableArray* newList = [[NSMutableArray alloc]init];
    newList = [list mutableCopy];
    [list removeAllObjects];
    
    NSMutableArray * cache = [kDefaults objectForKey:@"remindList"];
    
    for (int i =0; i<newList.count; i++) {
        GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[newList dictionaryWithIndex:i] error:nil];
        for (int k =0; k<cache.count; k++) {
            if ([[cache stringWithIndex:k] isEqualToString:model.goodsId]) {
                model.isRemind = YES;
            }else{
                model.isRemind = NO;
            }
            
        }
        [list addObject:model];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 8;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return self.recommendList.count;
    }else if (section==2){
        return 1;
    }else if (section==3){
        return self.recommandMList.count;
    }else if (section==4){
        return 1;
    }else if (section==5){
        return self.recommandNList.count;
    }else if (section==6){
        return 1;
    }else{
        return self.recommandEList.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        AnnounceTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"精品抢先看";
        return cell;
    }else if (indexPath.section==1){
        AnnounceGoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:goodsCell forIndexPath:indexPath];
        cell.goodsModel = [self.recommendList objectAtIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==2){
        AnnounceTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"上午 10 : 00";
        return cell;
    }else if (indexPath.section==3){
        AnnounceRemindCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:remindCell forIndexPath:indexPath];
        GoodsModel * model =[self.recommandMList objectAtIndex:indexPath.row];

        [cell cellSelectedWithBlock:^(BOOL select) {
            model.isRemind = select;
            if (select) {
                cell.btnRemind.selected =YES;
            }else{
                cell.btnRemind.selected =NO;
            }
        }];
        [cell reloadDataWithModel:model];
        return cell;
    }else if (indexPath.section==4){
        AnnounceTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"下午 2 : 00";
        return cell;
    }else if (indexPath.section==5){
        AnnounceRemindCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:remindCell forIndexPath:indexPath];
        GoodsModel * model =[self.recommandNList objectAtIndex:indexPath.row];
        
        [cell cellSelectedWithBlock:^(BOOL select) {
            model.isRemind = select;
            if (select) {
                cell.btnRemind.selected =YES;
            }else{
                cell.btnRemind.selected =NO;
            }
        }];
        [cell reloadDataWithModel:model];
        return cell;
    }else if (indexPath.section==6){
        AnnounceTitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:titleCell forIndexPath:indexPath];
        cell.lblTitle.text = @"晚上 8 : 00";
        return cell;
    }else{
        AnnounceRemindCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:remindCell forIndexPath:indexPath];
        GoodsModel * model =[self.recommandEList objectAtIndex:indexPath.row];
        
        [cell cellSelectedWithBlock:^(BOOL select) {
            model.isRemind = select;
            if (select) {
                cell.btnRemind.selected =YES;
            }else{
                cell.btnRemind.selected =NO;
            }
        }];
        [cell reloadDataWithModel:model];
        return cell;
    }
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(SCREEN_WIDTH,30);
    }else if (indexPath.section==1){
        return CGSizeMake(100,100);
    }else if (indexPath.section==2){
        return CGSizeMake(SCREEN_WIDTH,30);
    }else if (indexPath.section==3){
        return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,320);
    }else if (indexPath.section==4){
         return CGSizeMake(SCREEN_WIDTH,30);
    }else if (indexPath.section==5){
          return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,320);
    }else if (indexPath.section==6){
         return CGSizeMake(SCREEN_WIDTH,30);
    }else{
         return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,320);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==0) {
        return 0;
    }else if (section==1){
        return Cell_Spacing;
    }else if (section==2){
         return 0;
    }else if (section==3){
        return Cell_Spacing;
    }else if (section==4){
        return 0;
    }else if (section==5){
        return Cell_Spacing;
    }else if (section==6){
        return 0;
    }else{
        return Cell_Spacing;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==0) {
        return 0;
    }else if (section==1){
        return Cell_Spacing;
    }else if (section==2){
        return 0;
    }else if (section==3){
        return Cell_Spacing;
    }else if (section==4){
        return 0;
    }else if (section==5){
        return Cell_Spacing;
    }else if (section==6){
        return 0;
    }else{
        return Cell_Spacing;
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section==0) {
          return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }else if (section==1){
        return UIEdgeInsetsMake(Cell_Spacing*2, Cell_Spacing, Cell_Spacing*2,Cell_Spacing*2);
    }else if (section==2){
           return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }else if (section==3){
        return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);
    }else if (section==4){
        return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }else if (section==5){
       return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);
    }else if (section==6){
         return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }else{
        return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = [[self.recommendList dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
        vc.detailType = @"announce";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==3){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandMList objectAtIndex:indexPath.row]).goodsId;
        vc.detailType = @"announce";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==5){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandNList objectAtIndex:indexPath.row]).goodsId;
        vc.detailType = @"announce";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==7){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.recommandEList objectAtIndex:indexPath.row]).goodsId;
        vc.detailType = @"announce";
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end

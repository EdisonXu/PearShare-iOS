//
//  MallDetailInfoCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/24.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MallDetailInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *infoImage;
@property (nonatomic,strong)NSString * infoStr;
@end

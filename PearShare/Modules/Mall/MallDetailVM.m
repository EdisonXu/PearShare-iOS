//
//  MallDetailVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/23.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallDetailVM.h"

@implementation MallDetailVM

-(RACSignal*)mallDetailCommand
{
    [self.param setObj:@"1" forKey:@"userId"];
    [self.param setObj:self.goodsId forKey:@"goodsId"];
    
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking getWithUrl:[baseRequestUrl stringByAppendingString:@"goods/getGoodsById"] refreshCache:YES params:self.param success:^(NSDictionary* response) {
            [subscriber sendNext:[response dictionaryForKey:@"data"]];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

//
//  GoodDetailPriceCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/24.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodDetailPriceCell.h"

@implementation GoodDetailPriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     [self.countView bringSubviewToFront: self.contentView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    self.coverImage.showPageControl = NO;
    self.coverImage.placeholderImage = [UIImage imageNamed:@"mall_empty"];
    self.coverImage.backgroundColor = [UIColor whiteColor];
    if (model.count) {
        NSMutableArray *list = [[[model stringForKey:@"imageStr"] componentsSeparatedByString:@","]mutableCopy ];
        NSMutableArray *adList = [[NSMutableArray alloc] init];
        
        for (int i = 0; i<list.count; i++) {
            NSString* adStr = [PinImageUrl stringByAppendingString:[list stringWithIndex:i]];
            [adList addObject:adStr];
        }
        self.coverImage.imageURLStringsGroup = adList;
        
        self.lblTitle.text = [model stringForKey:@"title"];
        self.lblPriceNow.text = [NSString stringWithFormat:@"%@%@",@"¥ ",[model stringForKey:@"currentPrice"]];
        self.lblPriceOld.text =[NSString stringWithFormat:@"%@%@",@"¥ ",[model stringForKey:@"originalPrice"]];
        self.lblPriceOld.lineType = LineTypeMiddle;
    }
}
- (IBAction)kefuCommand:(id)sender {
    
//    HChatClient *client = [HChatClient sharedClient];
//    if (client.isLoggedInBefore != YES) {
//        HError *error = [client loginWithUsername:@"admin" password:@"123456asd"];
//        if (!error) { //登录成功
//            HDMessageViewController *chatVC = [[HDMessageViewController alloc] initWithConversationChatter:@"kefuchannelimid_481315"];
//            [[self getCurrentVC].navigationController pushViewController:chatVC animated:YES];
//        } else { //登录失败
//            return;
//        }
//    }
}


#pragma mark  获取当前控制器
- (UIViewController *)getCurrentVC{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    return result;
}
@end

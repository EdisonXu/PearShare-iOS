//
//  AnnounceRemindCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/21.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsModel.h"
#import "UICustomLineLabel.h"
@interface AnnounceRemindCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceNew;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@property (weak, nonatomic) IBOutlet UIButton *btnRemind;

@property (nonatomic, nonatomic) GoodsModel *goodsModel;

typedef void(^CellSelectedBlock)(BOOL select);
- (void)cellSelectedWithBlock:(CellSelectedBlock)block;

-(void)reloadDataWithModel:(GoodsModel*)model;
@end

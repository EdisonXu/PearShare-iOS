//
//  AnnounceRemindCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/21.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AnnounceRemindCell.h"
//#import "EventCalendar.h"
@interface AnnounceRemindCell()
{
    CellSelectedBlock cellSelectedBlock;
}
@property (nonatomic,strong)GoodsModel* model;
@end

@implementation AnnounceRemindCell




- (void)cellSelectedWithBlock:(CellSelectedBlock)block {
    cellSelectedBlock = block;
}

-(void)reloadDataWithModel:(GoodsModel *)model
{
    self.model = [[GoodsModel alloc] init];
    self.model = model;
    self.lblTitle.text = model.name;
    self.lblPriceOld.text =  [NSString stringWithFormat:@"%@%@",@"¥ ", model.originalPrice];
    self.lblPriceNew.text = [NSString stringWithFormat:@"%@%@",@"¥", model.currentPrice];
    
    self.lblPriceOld.lineType = LineTypeMiddle;
    [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[PinImageUrl stringByAppendingString:model.face]]];
    
    if (model.isRemind) {
        self.btnRemind.selected=YES;
    }else{
        self.btnRemind.selected=NO;
    }
}


- (IBAction)remindCommand:(id)sender {
    if (![kDefaults objectForKey:@"remindList"]) {
        NSMutableArray * list = [NSMutableArray array];
        [kDefaults setObject:list forKey:@"remindList"];
    }
    
    NSMutableArray * list = [[ kDefaults objectForKey:@"remindList"] mutableCopy];
    self.btnRemind.selected = !self.btnRemind.selected;
    
    if (self.btnRemind.selected) {
        [list addString:self.model.goodsId];
//        EventCalendar *calendar = [EventCalendar sharedEventCalendar];
//        [calendar createEventCalendarTitle:@"梨分享上新提醒" location:self.goodsModel.name startDate:[NSDate dateWithTimeInterval:100 sinceDate:[NSDate date]] endDate:[NSDate dateWithTimeInterval:7200 sinceDate:[NSDate date]] allDay:NO alarmArray:@[@"-3000"]];
    }else{
        [list removeObject:self.model.goodsId];
    }
    [kDefaults setObject:list forKey:@"remindList"];
    [kDefaults synchronize];
    
    if (cellSelectedBlock) {
        cellSelectedBlock(self.btnRemind.selected);
    }
}
@end

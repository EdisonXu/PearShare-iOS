//
//  TitleCell.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

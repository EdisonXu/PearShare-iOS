//
//  MallEndVM.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallEndVM.h"

@implementation MallEndVM

-(RACSignal*)mallEndCommand
{
    [self.param setObj:@"20" forKey:@"pagesize"];
    [self.param setInteger:self.page forKey:@"pageno"];
    
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [HYBNetworking getWithUrl:[baseRequestUrl stringByAppendingString:@"goods/getComingEnd"] refreshCache:YES params:self.param success:^(NSDictionary* response) {
            [subscriber sendNext:response];
            [subscriber sendCompleted];
        } fail:^(NSError *error) {
            [subscriber sendError:error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}
@end

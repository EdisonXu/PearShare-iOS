//
//  MallDetailTopCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/23.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallDetailTopCell.h"

@implementation MallDetailTopCell

-(void)setModel:(NSDictionary *)model
{
    NSMutableArray *list = [[[model stringForKey:@"imageStr"] componentsSeparatedByString:@","]mutableCopy ];
    NSMutableArray *adList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<list.count; i++) {
        NSString* adStr = [PinImageUrl stringByAppendingString:[list stringWithIndex:i]];
        [adList addObject:adStr];
    }
    self.coverImage.imageURLStringsGroup = adList;
    self.coverImage.showPageControl = NO;
    self.coverImage.placeholderImage = [UIImage imageNamed:@"mall_empty"];
    self.coverImage.backgroundColor = [UIColor whiteColor];
    
    self.lblTitle.text = [model stringForKey:@"title"];
    self.lblPriceNow.text = [NSString stringWithFormat:@"%@%@",@"¥ ",[model stringForKey:@"currentPrice"]];
    self.lblPriceOld.text =[NSString stringWithFormat:@"%@%@",@"¥ ",[model stringForKey:@"originalPrice"]];
    self.lblPriceOld.lineType = LineTypeMiddle;
}

@end

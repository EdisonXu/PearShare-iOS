//
//  MallDetailInfoCell.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/24.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallDetailInfoCell.h"

@implementation MallDetailInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setInfoStr:(NSString *)infoStr
{
    if (infoStr.length) {        
        [self.infoImage sd_setImageWithURL:[NSURL URLWithString:[[PinImageUrl stringByAppendingString:infoStr] stringByAppendingString:@"/480"]] placeholderImage:[UIImage imageNamed:@"mall_empty"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        [self.infoImage sd_setImageWithURL:[NSURL URLWithString:[[PinImageUrl stringByAppendingString:infoStr] stringByAppendingString:@"/480"]] placeholderImage:[UIImage imageNamed:@"mall_empty"] ];
        
        
        
        NSLog(@"%@",[[PinImageUrl stringByAppendingString:infoStr] stringByAppendingString:@"/480"]);
    }
}
@end

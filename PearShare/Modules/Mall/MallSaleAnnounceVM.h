//
//  MallSaleAnnounceVM.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/20.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface MallSaleAnnounceVM : BaseViewModel
-(RACSignal*)saleAnnounceCommand;
@end

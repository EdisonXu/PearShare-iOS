//
//  GoodsItem.m
//  PomeloChoose
//
//  Created by Destiny on 2018/1/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsItem.h"
//#import <AlibcTradeBiz/AlibcTradeBiz.h>
//#import <AlibcTradeSDK/AlibcTradeSDK.h>
//#import <AlibabaAuthSDK/albbsdk.h>
@interface GoodsItem()
@property (nonatomic,strong)GoodsModel* model;
@end
@implementation GoodsItem

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self layoutIfNeeded];
}

-(void)setGoodsModel:(GoodsModel *)goodsModel
{
    self.model = [[GoodsModel alloc]init];
    self.model  = goodsModel;
    self.lblTitle.text = goodsModel.name;
    self.lblPriceOld.text = [NSString stringWithFormat:@"%@%@",@"¥ ", goodsModel.originalPrice];
    self.lblPriceNew.text =[NSString stringWithFormat:@"%@%@",@"¥", goodsModel.currentPrice];
    self.lblPriceOld.lineType = LineTypeMiddle;
    if (goodsModel.face.length>0) {
        [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[[PinImageUrl stringByAppendingString:goodsModel.face] stringByAppendingString:@"/480"]]];
    }else{
        [self.imageCover setImage:[UIImage imageNamed:@"mall_empty"]];
    }
}

- (IBAction)buyCommand:(id)sender
{
//    id<AlibcTradePage> page = [AlibcTradePageFactory page:self.model.url];
//    //淘客信息
//    AlibcTradeTaokeParams *taoKeParams=[[AlibcTradeTaokeParams alloc] init];
//    taoKeParams.pid= @"mm_44889980_43066955_279234503";
//    //打开方式
//    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
//    showParam.openType = AlibcOpenTypeNative;
//    [[AlibcTradeSDK sharedInstance] .tradeService show:[self getCurrentVC].navigationController page:page showParams:showParam taoKeParams:taoKeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
}
@end

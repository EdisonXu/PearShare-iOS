//
//  MallDetailVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/23.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallDetailVC.h"
#import "GoodDetailPriceCell.h"
#import "MallDetailTagCell.h"
#import "MallDetailInfoCell.h"
#import "MallDetailVM.h"
#import "GoodsModel.h"
//#import "WRNavigationBar.h"

@interface MallDetailVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UICollectionView*collectionView;
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong)UIView*toolView;
@property (nonatomic,strong)MallDetailVM*viewModel;
@property (nonatomic,strong)NSDictionary*goodModel;
@property (nonatomic,strong)NSArray*goodsTagList;
@property (nonatomic,strong)NSArray*infoList;
@property (nonatomic,strong)UIButton *btnBuy;
@end

@implementation MallDetailVC

static NSString * const priceCell                        = @"GoodDetailPriceCell";
static NSString * const tagCell                           = @"MallDetailTagCell";
static NSString * const infoCell                          = @"MallDetailInfoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.toolView];

    self.viewModel = [[MallDetailVM alloc]init];
    [self setUpDetail];
    
//    [self wr_setNavBarBarTintColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0]];
//    // 设置初始导航栏透明度
//    [self wr_setNavBarBackgroundAlpha:0];
//    // 设置导航栏按钮和标题颜色
//    [self wr_setNavBarTintColor:[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0]];
    // 设置初始导航栏透明度];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mall_share"] style:UIBarButtonItemStyleDone target:self action:nil];
    
    
    if ([self.detailType isEqualToString:@"announce"]) {
        self.btnBuy = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        [self.btnBuy setTitle:@"设置提醒" forState:UIControlStateNormal];
        [self.btnBuy setBackgroundColor:[UIColor clearColor]];
//        [self.btnBuy addTarget:self action:@selector(buyCommand) forControlEvents:UIControlEventTouchUpInside];
        [self.toolView addSubview:self.btnBuy];
    }else{
        self.btnBuy = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        [self.btnBuy setTitle:@"立即购买" forState:UIControlStateNormal];
        [self.btnBuy setBackgroundColor:[UIColor clearColor]];
        [self.btnBuy addTarget:self action:@selector(buyCommand) forControlEvents:UIControlEventTouchUpInside];
        [self.toolView addSubview:self.btnBuy];
    }
}

//-(void)buyCommand
//{
//    id<AlibcTradePage> page = [AlibcTradePageFactory page:[self.goodModel stringForKey:@"url"]];
//    //淘客信息
//    AlibcTradeTaokeParams *taoKeParams=[[AlibcTradeTaokeParams alloc] init];
//    taoKeParams.pid= @"mm_44889980_43066955_279234503";
//    //打开方式
//    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
//    showParam.openType = AlibcOpenTypeNative;
//    [[AlibcTradeSDK sharedInstance] .tradeService show:self.navigationController page:page showParams:showParam taoKeParams:taoKeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
//}

-(void)setUpDetail
{
    self.viewModel.goodsId = self.goodsId;
    RACSignal * signal = self.viewModel.mallDetailCommand;
    @weakify(self);
    [signal subscribeNext:^(NSDictionary* data) {
        @strongify(self);
        self.goodModel = [[[NSMutableDictionary alloc] initWithDictionary:data] mutableCopy];
        self.goodsTagList = [NSArray arrayWithArray:[data arrayForKey:@"goodsTags"]];
        self.infoList = [[data stringForKey:@"goodsInfo"] componentsSeparatedByString:@","];
        [self.tableView reloadData];
    } error:^(NSError *error) {
        
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0||section==1) {
        return 1;
    }else{
        return self.infoList.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        GoodDetailPriceCell *cell =[tableView dequeueReusableCellWithIdentifier:priceCell forIndexPath:indexPath];
        cell.model = self.goodModel;
        return cell;
    }else if (indexPath.section==1){
        MallDetailTagCell *cell =[tableView dequeueReusableCellWithIdentifier:tagCell forIndexPath:indexPath];
        return cell;
    }else{
        MallDetailInfoCell *cell =[tableView dequeueReusableCellWithIdentifier:infoCell forIndexPath:indexPath];
        [cell.infoImage sd_setImageWithURL:[NSURL URLWithString:[[PinImageUrl stringByAppendingString:[self.infoList stringWithIndex:indexPath.row]] stringByAppendingString:@"/480"]] placeholderImage:[UIImage imageNamed:@"mall_empty"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self viewIfLoaded];
        }];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 500;
    }else if (indexPath.section==1){
        return 100;
    }else{
        return UITableViewAutomaticDimension;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

#pragma mark - getter / setter
- (UITableView *)tableView
{
    if (_tableView == nil) {

        
        
        _tableView.backgroundColor = [UIColor whiteColor];
//        _tableView.contentInset = UIEdgeInsetsMake(-[self navBarBottom], 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        /** 注册cell可重用ID */
      [_tableView registerNib:GoodDetailPriceCell.nib forCellReuseIdentifier:priceCell];
      [_tableView registerNib:MallDetailTagCell.nib forCellReuseIdentifier:tagCell];
      [_tableView registerNib:MallDetailInfoCell.nib forCellReuseIdentifier:infoCell];
    }
    return _tableView;
}

-(UIView*)toolView
{
    if (_toolView ==nil) {
        CGRect frame = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        _toolView= [[UIView alloc] initWithFrame:frame];
        _toolView.backgroundColor = [UIColor colorWithHexString:@"FF3636"];
    }
    return _toolView;
}
@end

//
//  MallDetailVC.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/23.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewController.h"

@interface MallDetailVC : BaseViewController
@property (nonatomic,strong)NSString * goodsId;
@property (nonatomic,strong)NSString * detailType;
@end

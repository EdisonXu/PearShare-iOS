//
//  MallHotSaleCollectionVC.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallHotSaleCollectionVC.h"
#import "HotSaleVM.h"
#import "SDCycleScrollView.h"
#import "HotTopAdCell.h"
#import "HotSaleCell.h"
#import "TitleCell.h"
#import "GoodsItem.h"
#import "MallDetailVC.h"
#import "WeiBoMineController.h"

#define Cell_Spacing 9
@interface MallHotSaleCollectionVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *adView;
@property (nonatomic,strong)HotSaleVM *viewModel;
@property (nonatomic,strong)NSMutableArray *goodsList;
@property (nonatomic,strong)NSMutableArray *recommendList;
@property (nonatomic,strong)NSMutableArray *adList;



@end

@implementation MallHotSaleCollectionVC

static NSString * const cellreuseIdentifier         = @"HotSaleCell";
static NSString * const cellTitle                        = @"TitleCell";
static NSString * const goodsCell                     = @"GoodsItem";
static NSString * const adCell                          = @"HotTopAdCell";

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        /** 创建布局参数 */
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
        [self.collectionView setCollectionViewLayout:flowLayout];
        
        /** 注册cell可重用ID */
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cellreuseIdentifier];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cellTitle];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:goodsCell];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:adCell];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    self.viewModel = [[HotSaleVM alloc]init];
    [self setUpAdvertisement];
    
    @weakify(self);
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self setUpGoods];
    }];
    
    self.collectionView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadMoreData];
    }];
    
    [self.collectionView.mj_header beginRefreshing];
    [self setUpGoods];
}

-(void)setUpAdvertisement
{
    @weakify(self);
    RACSignal * signal = self.viewModel.advertisementCommand;
    [signal subscribeNext:^(NSArray* list) {
        @strongify(self);
        self.adList = [[[NSMutableArray alloc] initWithArray:list] mutableCopy];
        for (int i = 0; i<list.count; i++) {
            if ([[[list dictionaryWithIndex:i] stringForKey:@"postion"]isEqualToString:@"1" ]) {
                NSString* adStr = [PinImageUrl stringByAppendingString:[[list dictionaryWithIndex:i] stringForKey:@"face"]];
                [self.adList addObject:adStr];
            }
        }
    } error:^(NSError *error) {
        
    }];
}

-(void)setUpGoods
{
    @weakify(self);
    self.viewModel.page = 1;
    RACSignal * signal = self.viewModel.hotSaleGoodsCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        @strongify(self);
        self.recommendList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listGoodsRecommend"]] mutableCopy];
        self.goodsList = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listGoods"]] mutableCopy];
        [self setUpModelList:self.goodsList];
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)loadMoreData
{
    @weakify(self);
    [self.viewModel addPage];
    RACSignal * signal = self.viewModel.hotSaleGoodsCommand;
    [signal subscribeNext:^(NSDictionary* data) {
        @strongify(self);
        NSMutableArray * list = [[[NSMutableArray alloc]initWithArray:  [data arrayForKey:@"listGoods"]] mutableCopy];
        if (list.count>0) {
            for (int i =0; i<list.count; i++) {
                GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[list dictionaryWithIndex:i] error:nil];
                [self.goodsList addObject:model];
            }
        }
        [self.collectionView reloadData];
        [self.collectionView.mj_footer endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

-(void)setUpModelList:(NSArray*)list
{
    self.goodsList = [[NSMutableArray alloc] init];
    for (int i =0; i<list.count; i++) {
        GoodsModel * model = [[GoodsModel alloc]initWithDictionary:[list dictionaryWithIndex:i] error:nil];
        [self.goodsList addObject:model];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return self.recommendList.count;
    }else if (section==2){
        return 1;
    }else{
        return self.goodsList.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        HotTopAdCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:adCell forIndexPath:indexPath];
        cell.adList = self.adList;
        return cell;
    }else if (indexPath.section==1){
        HotSaleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellreuseIdentifier forIndexPath:indexPath];
        cell.data = [self.recommendList dictionaryWithIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==2){
        TitleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellTitle forIndexPath:indexPath];
        return cell;
    }else{
        GoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:goodsCell forIndexPath:indexPath];
        cell.goodsModel = [self.goodsList objectAtIndex:indexPath.row];
        return cell;
    }
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return CGSizeMake(SCREEN_WIDTH,160);
    }else if (indexPath.section==1){
        return CGSizeMake(SCREEN_WIDTH,110);
    }else if (indexPath.section==2){
        return  CGSizeMake(SCREEN_WIDTH,30);
    }else{
         return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,321);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==3) {
          return Cell_Spacing;
    }else{
        return 0;
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    if (section==3) {
        return Cell_Spacing;
    }else{
        return 0;
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section==3) {
         return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);
    }else{
         return UIEdgeInsetsMake(0, 0, 0,0);//分别为上、左、下、右
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = [[self.recommendList dictionaryWithIndex:indexPath.row] stringForKey:@"goodsId"];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section==3){
        MallDetailVC *vc = [MallDetailVC new];
        vc.goodsId = ((GoodsModel*)[self.goodsList objectAtIndex:indexPath.row]).goodsId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
@end

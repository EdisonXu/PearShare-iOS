//
//  MallManager.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/3/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MallManager.h"
#import "MallEndCollectionVC.h"
#import "MallHotSaleCollectionVC.h"
#import "MallTodaySaleVC.h"
#import "MallSaleAnnounceVC.h"
@interface MallManager ()
@property (nonatomic,strong)NSArray* controllersNames;
@end

@implementation MallManager

#pragma mark 懒加载

- (NSArray *)controllersNames {
    if (_controllersNames == nil) {
        _controllersNames = @[@"即将结束",
                                                @"限时热卖",
                                                @"今日上新",
                                                @"新品预告"];
    }
    return _controllersNames;
}

-(instancetype)init
{
    if (self = [super init]) {
        [self setUpPageView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"今日优选"];
    self.menuView.backgroundColor = [UIColor colorWithHexString:@"333333"];
//    [WRNavigationBar wr_setDefaultNavBarShadowImageHidden:YES];
//    [self wr_setNavBarBackgroundAlpha:0];
}

-(void)setUpPageView
{
    self.menuViewStyle = WMMenuViewStyleFlood;
    self.showOnNavigationBar = NO;
    self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
    self.titleColorNormal = [UIColor whiteColor];
    self.progressColor = [UIColor colorWithHexString:@"fed000"];
    self.titleSizeNormal = 17;
    self.titleSizeSelected = 17;
    self.menuItemWidth = self.view.frame.size.width/4;
}

#pragma mark-dataSouce
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.controllersNames.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.controllersNames stringWithIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index==0) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        MallEndCollectionVC *vc = [sb instantiateViewControllerWithIdentifier:@"MallEndCollectionVC"];
        return vc;
    }else if (index==1){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        MallHotSaleCollectionVC *vc = [sb instantiateViewControllerWithIdentifier:@"MallHotSaleCollectionVC"];
         return vc;
    }else if (index==2){
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        MallTodaySaleVC *vc = [sb instantiateViewControllerWithIdentifier:@"MallTodaySaleVC"];
        return vc;
    }else{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        MallSaleAnnounceVC *vc = [sb instantiateViewControllerWithIdentifier:@"MallSaleAnnounceVC"];
        return vc;
    }
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width ;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : CGRectGetMaxY(self.navigationController.navigationBar.frame);
    return CGRectMake(leftMargin, originY, self.view.frame.size.width - 2*leftMargin, 57);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}
@end

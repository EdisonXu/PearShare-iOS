//
//  MallTodaySaleVM.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface MallTodaySaleVM : BaseViewModel
-(RACSignal*)advertisementCommand;
-(RACSignal*)todayNewSaleCommand;
@end

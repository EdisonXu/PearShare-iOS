//
//  AddAddress.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "AddAddress.h"
#import "PPNetworkHelper.h"
@interface AddAddress ()

@end

@implementation AddAddress

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"编辑地址";

    self.tableView.tableFooterView = [[UIView alloc]init];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==4) {
        NSMutableDictionary*pram = [[NSMutableDictionary alloc] init];
        [pram setString:self.textName.text forKey:@"userName"];
        [pram setString:self.textPhone.text forKey:@"userTel"];
        [pram setString:self.textArea.text forKey:@"region"];
        [pram setString:self.textDetail.text forKey:@"address"];
        [pram setString:@"1" forKey:@"isDefault"];

        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:pram options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[URL_main stringByAppendingString:@"/Integra/addressHandleType?addressType=0"] parameters:nil error:nil];
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
        [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
            if ([[responseObject stringForKey:@"success"] boolValue]) {
                
                
                  self.callBackBlock(self.textArea.text); 
                [self.navigationController popViewControllerAnimated:YES];
            }
        }] resume];
    }
}



@end

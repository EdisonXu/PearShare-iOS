//
//  IntegralOrder.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntegralOrder.h"
#import "OrderCell.h"
#import "AddressCell.h"
#import "AddAddress.h"
#import "PPNetworkHelper.h"
@interface IntegralOrder ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblNedds;
@property (weak, nonatomic) IBOutlet UILabel *lblREst;
@property (nonatomic,strong)NSString * adress;
@property (nonatomic,strong)NSMutableDictionary* dic;

@end

@implementation IntegralOrder

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"确认订单";
    self.adress = @"请选择地址";
    self.tableView.delegate = self;
    self.tableView.dataSource =self;
    [self.tableView registerNib:OrderCell.nib forCellReuseIdentifier:@"OrderCell"];
    [self.tableView registerNib:AddressCell.nib forCellReuseIdentifier:@"AddressCell"];
    
    self.tableView.tableFooterView = [[UIView alloc]init];
    
     NSInteger my = [kDefaultUserPoint integerValue];
    NSInteger nedds =  [self.model integerForKey:@"integralGoodsAmt"];
    self.lblNedds.text = [NSString stringWithFormat:@"%@积分",[self.model stringForKey:@"integralGoodsAmt"]];
    self.lblREst.text = [NSString stringWithFormat:@"剩余( %ld 积分)",my-nedds];
    self.dic = [NSMutableDictionary dictionary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        AddressCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell" forIndexPath:indexPath];
        cell.lblAddress.text = self.adress;
        return cell;
    }else{
        OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
        [cell.imageCover sd_setImageWithURL:[NSURL URLWithString:[self.model stringForKey:@"integralGoodsCover"]]];
        cell.lblTitle.text = [self.model stringForKey:@"integralGoodsTitle"];
        cell.lblPrice.text =[NSString stringWithFormat:@"¥ %@",[self formatFloat: [self.model stringForKey:@"integralGoodsActivityAmt"]]];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0) {
        return 44;
    }else{
        return 200;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 8;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0&&[self.adress isEqualToString:@"请选择地址"]) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        AddAddress *vc = [storyboard instantiateViewControllerWithIdentifier:@"AddAddress"];
        vc.callBackBlock = ^(NSString *text){   // 1
            self.adress = text;
            [self.tableView reloadData];
            [self getAdrress];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

-(void)getAdrress
{
    [PPNetworkHelper setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [PPNetworkHelper setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [PPNetworkHelper POST:[URL_main stringByAppendingString:@"/Integra/findUserAddressList"] parameters:nil success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
            [self.dic setString:[[[responseObject arrayForKey:@"data"]dictionaryWithIndex:0]stringForKey:@"addressId"] forKey:@"addressId"];
            [self.dic setString:@"1" forKey:@"integralGoodsNum"];
            [self.dic setString:[self.model stringForKey:@"integralGoodsId"] forKey:@"integralGoodsId"];
        }
    } failure:^(NSError *error) {
        
    }];
}


- (NSString *)formatFloat:(NSString*)price
{
    float f = [price floatValue];
    f = f/100;
    
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}

- (IBAction)duihuan:(id)sender
{
    [PPNetworkHelper setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [PPNetworkHelper setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [PPNetworkHelper POST:[URL_main stringByAppendingString:@"/Integra/userExchangeGoods"] parameters:self.dic success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"兑换成功！";
            [hud hideAnimated:YES afterDelay:1.f];
            
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        }
    } failure:^(NSError *error) {
        
    }];
}
@end

//
//  IntergralCheckCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntergralCheckCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (nonatomic,strong)NSMutableArray * checkList;
@property (weak, nonatomic) IBOutlet UIImageView *imageMon;
@property (weak, nonatomic) IBOutlet UIImageView *imageTue;
@property (weak, nonatomic) IBOutlet UIImageView *imageWed;
@property (weak, nonatomic) IBOutlet UIImageView *imageThu;
@property (weak, nonatomic) IBOutlet UIImageView *imageFri;
@property (weak, nonatomic) IBOutlet UIImageView *imageSat;
@property (weak, nonatomic) IBOutlet UIImageView *imageSun;
@end

//
//  CheckStatusAPI.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CheckStatusAPI.h"

@implementation CheckStatusAPI

-(NSString *)requestUrl{
    return @"Integra/findUserReport";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

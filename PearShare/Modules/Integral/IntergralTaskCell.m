//
//  IntergralTaskCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntergralTaskCell.h"

@implementation IntergralTaskCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(NSDictionary *)model
{
    self.lblTitle.text =[model stringForKey:@"taskTitle"];
    self.lbllittl.text =[model stringForKey:@"taskContent"];
    self.lblIntergral.text = [NSString stringWithFormat:@"%@%@",@"+",[model stringForKey:@"taskIntegral"]];
    self.lblDone.text = [NSString stringWithFormat:@"%@%@%@",[model stringForKey:@"integralTaskNum"],@"/",[model stringForKey:@"taskNum"]];
    
    if ([[model stringForKey:@"integralTaskNum"] isEqualToString:[model stringForKey:@"taskNum"]]) {
        [self.rightView setBackgroundColor:[UIColor colorWithHexString:@"7E7E7E"]];
    }else{
        [self.rightView setBackgroundColor:[UIColor colorWithHexString:@"FED500"]];
    }
}
@end

//
//  IntergralCheckCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntergralCheckCell.h"

@implementation IntergralCheckCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCheckList:(NSMutableArray *)checkList
{
    self.lblCount.text =[NSString stringWithFormat:@"%ld", checkList.count];
    
    [checkList enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期一"]) {
            [self.imageMon setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期二"]) {
            [self.imageTue setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期三"]) {
            [self.imageWed setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期四"]) {
            [self.imageThu setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期五"]) {
            [self.imageFri setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期六"]) {
            [self.imageSat setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
        if ([[obj stringForKey:@"weekReport"] isEqualToString:@"星期天"]) {
            [self.imageSun setImage:[UIImage imageNamed:@"schedule_seclect"]];
        }
            
    }];
}


@end

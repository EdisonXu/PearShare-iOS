//
//  IntegralManager.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntegralManager.h"
#import "INtegralHeader.h"
#import "IntegralMall.h"
#import "IntergralCheckList.h"
#import "IntergralTask.h"

static CGFloat const kWMMenuViewHeight = 44.0;
@interface IntegralManager () <UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSArray *musicCategories;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, strong) UIView *redView;
@end

@implementation IntegralManager

- (NSArray *)musicCategories {
    if (!_musicCategories) {
        _musicCategories = @[@"积分商城", @"我的签到", @"每日任务"];
    }
    return _musicCategories;
}

- (instancetype)init {
    if (self = [super init]) {
        self.viewTop = kNavigationBarHeight + kWMHeaderViewHeight;
        self.menuViewStyle = WMMenuViewStyleLine;
         self.menuItemWidth = [UIScreen mainScreen].bounds.size.width / self.musicCategories.count;
        self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
        self.titleColorNormal = [UIColor colorWithHexString:@"828282"];
        self.progressColor = [UIColor colorWithHexString:@"fed000"];
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"积分兑换";
    
    INtegralHeader * header = [[NSBundle mainBundle] loadNibNamed: @"INtegralHeader" owner:self options:nil].firstObject;
    header.backgroundColor = [UIColor whiteColor];
    self.redView = header;
    [self.view addSubview:self.redView];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGRect tempFrame = CGRectMake(0, kNavigationBarHeight, [UIScreen mainScreen].bounds.size.width, kWMHeaderViewHeight);
    self.redView.frame = tempFrame;
}


#pragma mark - Datasource & Delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.musicCategories.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index==0) {
        IntegralMall *vc = [[IntegralMall alloc] init];
        return vc;
    }else if (index==1){
        IntergralCheckList *vc = [[IntergralCheckList alloc] init];
        return vc;
    }else{
        IntergralTask *vc = [[IntergralTask alloc] init];
        return vc;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, _viewTop, self.view.frame.size.width, kWMMenuViewHeight);

}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = _viewTop + kWMMenuViewHeight;
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}
@end

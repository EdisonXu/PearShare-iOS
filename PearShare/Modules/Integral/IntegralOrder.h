//
//  IntegralOrder.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntegralOrder : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSDictionary* model;
@end

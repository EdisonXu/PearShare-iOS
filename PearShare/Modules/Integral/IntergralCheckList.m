//
//  IntergralCheckList.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntergralCheckList.h"
#import "IntergralCheckCell.h"
#import "CheckStatusAPI.h"
#import "CheckAPI.h"
@interface IntergralCheckList ()
@property (nonatomic,strong)NSMutableArray* checkList;
@end
@implementation IntergralCheckList

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:IntergralCheckCell.nib forCellReuseIdentifier:@"IntergralCheckCell"];
    
    [self checkCommand];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IntergralCheckCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IntergralCheckCell" forIndexPath:indexPath];
    cell.checkList  = self.checkList;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 280;
}


-(void)setUpCheck
{
    CheckStatusAPI * api = [CheckStatusAPI new];
    [api startWithCompletionBlockWithSuccess:^(CheckStatusAPI* _Nonnull request) {
        if ([api isSuccess]) {
            self.checkList =[[NSMutableArray arrayWithArray:[request.result arrayForKey:@"data" ]] mutableCopy];
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)checkCommand
{
    CheckAPI * api = [CheckAPI new];
    [api startWithCompletionBlockWithSuccess:^(CheckAPI* _Nonnull request) {
        if ([api isSuccess]) {
            self.checkList =[[NSMutableArray arrayWithArray:[request.result arrayForKey:@"data" ]] mutableCopy];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"签到成功"];
            [hud hideAnimated:YES afterDelay:1.f];
            [self.tableView reloadData];
        }
        [self setUpCheck];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self setUpCheck];
    }];
}


@end

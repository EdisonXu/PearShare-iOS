//
//  TaskAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "TaskAPI.h"

@implementation TaskAPI


-(NSString *)requestUrl{
    return @"Integra/findIntegralTask/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

//
//  INtegralHeader.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "INtegralHeader.h"
#import "LEEAlert.h"
@implementation INtegralHeader

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib
{
    [super awakeFromNib];
    self.lblPoint.text = kDefaultUserPoint;
}

- (IBAction)show:(id)sender {
    [LEEAlert actionsheet].config
    .LeeTitle(@"活动说明")
    .LeeContent(@"注：此活动与设备制造商APPLE®️无关。")
    .LeeAction(@"好的", ^{
        
        // 点击事件Block
    })
    .LeeOpenAnimationConfig(^(void (^animatingBlock)(void), void (^animatedBlock)(void)) {
        
        [UIView animateWithDuration:1.5f delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:1 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            
            animatingBlock(); //调用动画中Block
            
        } completion:^(BOOL finished) {
            
            animatedBlock(); //调用动画结束Block
        }];
        
    })
    .LeeCloseAnimationConfig(^(void (^animatingBlock)(void), void (^animatedBlock)(void)) {
        
        [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            
            animatingBlock();
            
        } completion:^(BOOL finished) {
            
            animatedBlock();
        }];
        
    })
    .LeeShow();
}

@end

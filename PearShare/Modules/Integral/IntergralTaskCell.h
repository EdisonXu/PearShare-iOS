//
//  IntergralTaskCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntergralTaskCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbllittl;
@property (weak, nonatomic) IBOutlet UILabel *lblIntergral;
@property (nonatomic,strong)NSDictionary* model;
@property (weak, nonatomic) IBOutlet UILabel *lblDone;
@property (weak, nonatomic) IBOutlet UIView *rightView;
@end

//
//  IntegralManager.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "WMPageController.h"
static CGFloat const kWMHeaderViewHeight = 155;
static CGFloat const kNavigationBarHeight = 64;
@interface IntegralManager : WMPageController
@property (nonatomic, assign) CGFloat viewTop;
@end

//
//  IntergralTask.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntergralTask.h"
#import "IntergralTaskCell.h"
#import "TaskAPI.h"
@interface IntergralTask ()
{
    NSArray * _taskList;
}
@end

@implementation IntergralTask

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:IntergralTaskCell.nib forCellReuseIdentifier:@"IntergralTaskCell"];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setUpdata];
}

- (void)setUpdata
{
    TaskAPI * api = [TaskAPI new];
    [api startWithCompletionBlockWithSuccess:^(TaskAPI *  request) {
        if ([request isSuccess]) {
            _taskList = [NSArray arrayWithArray:[request.result arrayForKey:@"data"]];
            [self.tableView reloadData];
        }else{
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
       return _taskList.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IntergralTaskCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IntergralTaskCell" forIndexPath:indexPath];
    cell.model = [_taskList dictionaryWithIndex:indexPath.section];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
@end

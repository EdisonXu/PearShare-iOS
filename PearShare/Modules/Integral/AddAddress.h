//
//  AddAddress.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAddress : UITableViewController
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textPhone;
@property (weak, nonatomic) IBOutlet UITextField *textDetail;
@property (weak, nonatomic) IBOutlet UITextField *textArea;

typedef void(^CallBackBlcok) (NSString *text);
@property (nonatomic,copy)CallBackBlcok callBackBlock;

@end

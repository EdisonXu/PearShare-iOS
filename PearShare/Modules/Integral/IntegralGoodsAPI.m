//
//  IntegralGoodsAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntegralGoodsAPI.h"

@implementation IntegralGoodsAPI
-(NSString *)requestUrl{
    return @"Integra/findIntegralGoods/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString: [NSString stringWithFormat:@"%ld",(long)self.pageNum] forKey:@"pageNum"];
    [param setString:@"20" forKey:@"pageSize"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

//
//  IntegralGoodsCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntegralGoodsCell.h"
#import "IntegralOrder.h"

@interface IntegralGoodsCell()
{
    NSDictionary * _itemModel;
}
@end
@implementation IntegralGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    _itemModel = [[NSDictionary alloc] initWithDictionary:model];
    self.lblTitle.text = [model stringForKey:@"integralGoodsTitle"];
    [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"integralGoodsCover"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    self.lblPrice.text = [model stringForKey:@"integralGoodsAmt"];
}

- (IBAction)change:(id)sender
{
    NSInteger  need = [[_itemModel stringForKey:@"integralGoodsAmt"] integerValue];
    NSInteger my = [kDefaultUserPoint integerValue];
    
    if (my>=need) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        IntegralOrder *vc = [storyboard instantiateViewControllerWithIdentifier:@"IntegralOrder"];
        vc.model = _itemModel;
        [self.viewController.navigationController pushViewController:vc animated:YES ];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"继续赚取积分吧";
        [hud hideAnimated:YES afterDelay:1.f];
    }
    
}

@end

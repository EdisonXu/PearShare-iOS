//
//  IntegralMall.m
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "IntegralMall.h"
#import "IntegralGoodsCell.h"
#import "IntegralGoodsAPI.h"
@interface IntegralMall ()
{
    NSArray * _goodsList;
}
@end

@implementation IntegralMall

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:IntegralGoodsCell.nib forCellReuseIdentifier:@"IntegralGoodsCell"];
    [self setUpdata];
}

- (void)setUpdata
{
    IntegralGoodsAPI * api = [IntegralGoodsAPI new];
    api.pageNum = 1;
    [api startWithCompletionBlockWithSuccess:^(IntegralGoodsAPI *  request) {
        if ([request isSuccess]) {
            _goodsList = [NSArray arrayWithArray:[request.result arrayForKey:@"data"]];
            [self.tableView reloadData];
        }else{
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _goodsList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IntegralGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IntegralGoodsCell" forIndexPath:indexPath];
    cell.model = [_goodsList dictionaryWithIndex:indexPath.row];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 114;
}

@end

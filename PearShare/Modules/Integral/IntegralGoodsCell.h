//
//  IntegralGoodsCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/5.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntegralGoodsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (nonatomic,strong)NSDictionary * model;
@end

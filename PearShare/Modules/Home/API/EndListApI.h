//
//  EndListApI.h
//  PearShare
//
//  Created by Edison on 2018/7/31.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface EndListApI : BaseRequestAPI
@property (nonatomic,assign)NSInteger pageNum;
@property (nonatomic,strong)NSString*pageSize;
@end

//
//  FavoriteAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface FavoriteAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * collectionType;
@property (nonatomic,strong)NSString * articleIdList;
@end

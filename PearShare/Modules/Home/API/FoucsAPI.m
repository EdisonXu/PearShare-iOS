//
//  FoucsAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "FoucsAPI.h"

@implementation FoucsAPI
-(NSString *)requestUrl{
    return @"fans/typeFans";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.fansId forKey:@"fansId"];
    [param setString:self.fansType forKey:@"fansType"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

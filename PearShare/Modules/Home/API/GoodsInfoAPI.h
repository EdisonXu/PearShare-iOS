//
//  GoodsInfoAPI.h
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface GoodsInfoAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * goodsId;
@end

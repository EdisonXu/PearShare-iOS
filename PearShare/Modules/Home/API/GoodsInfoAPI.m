//
//  GoodsInfoAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsInfoAPI.h"

@implementation GoodsInfoAPI


-(NSString *)requestUrl{
    return[NSString stringWithFormat:@"%@%@",@"shares/goodsDetail/",self.goodsId];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end

//
//  SaleListAPI.m
//  PearShare
//
//  Created by Edison on 2018/7/31.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SaleListAPI.h"

@implementation SaleListAPI
-(NSString *)requestUrl{
    return  @"shares/groupList";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:self.type forKey:@"type"];
    return param;
}
@end

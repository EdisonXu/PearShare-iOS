//
//  HomeAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface HomeAPI : BaseRequestAPI
@property (nonatomic,strong)NSString*type;
@property (nonatomic,assign)NSInteger pageNum;
@property (nonatomic,strong)NSString*pageSize;
@end

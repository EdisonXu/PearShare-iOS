//
//  SystemDicsAPI.m
//  PearShare
//
//  Created by Destiny on 2018/7/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SystemDicsAPI.h"

@implementation SystemDicsAPI
-(NSString *)requestUrl{
    return [NSString stringWithFormat:@"%@%@", @"system/dics/",self.pid];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end

//
//  PushAPI.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface PushAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * title;
@property (nonatomic,strong)NSString * content;
@property (nonatomic,strong)NSString * imgs;
@property (nonatomic,strong)NSString * classify;
@property (nonatomic,strong)NSString * tags;
@end

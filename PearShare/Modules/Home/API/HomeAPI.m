//
//  HomeAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeAPI.h"

@implementation HomeAPI
-(NSString *)requestUrl{
   return  @"shares/list/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.type forKey:@"type"];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:[NSString stringWithFormat:@"%ld",self.pageNum] forKey:@"pageNum"];
    [param setString:self.pageSize forKey:@"pageSize"];
    return param;
}

@end

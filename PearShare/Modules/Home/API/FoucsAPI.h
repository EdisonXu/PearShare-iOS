//
//  FoucsAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface FoucsAPI : BaseRequestAPI
@property (nonatomic,strong)NSString * fansId;
@property (nonatomic,strong)NSString * fansType;
@end

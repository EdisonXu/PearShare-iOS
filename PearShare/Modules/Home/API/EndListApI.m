//
//  EndListApI.m
//  PearShare
//
//  Created by Edison on 2018/7/31.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "EndListApI.h"

@implementation EndListApI
-(NSString *)requestUrl{
    return  @"shares/endGroupList";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:[NSString stringWithFormat:@"%ld",self.pageNum] forKey:@"pageNum"];
    [param setString:self.pageSize forKey:@"pageSize"];
    return param;
}
@end

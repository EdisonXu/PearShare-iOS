//
//  PushAPI.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/7/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "PushAPI.h"

@implementation PushAPI
-(NSString *)requestUrl{
    return @"shares/publishShare";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.title forKey:@"title"];
    [param setString:self.content forKey:@"content"];
    [param setString:self.imgs forKey:@"imgs"];
    [param setString:self.classify forKey:@"classify"];
    [param setString:self.tags forKey:@"tags"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

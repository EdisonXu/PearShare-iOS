
//
//  FavoriteAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "FavoriteAPI.h"

@implementation FavoriteAPI
-(NSString *)requestUrl{
    return @"article/typeCollection/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.articleIdList forKey:@"articleIdList"];
    [param setString:self.collectionType forKey:@"collectionType"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

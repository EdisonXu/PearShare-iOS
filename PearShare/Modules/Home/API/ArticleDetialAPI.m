//
//  ArticleDetialAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ArticleDetialAPI.h"

@implementation ArticleDetialAPI
-(NSString *)requestUrl{
    return  @"shares/detail/";
    
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.shareId forKey:@"shareId"];
    
    if (kDefaultLoginAlready) {
        [param setString:kDefaultUserId forKey:@"userId"];
    }
    NSLog(@"%lu",(unsigned long)kDefaultUserId);
    return param;
}


@end

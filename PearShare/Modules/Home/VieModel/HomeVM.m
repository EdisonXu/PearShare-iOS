
//  HomeVM.m
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeVM.h"
#import "HomeAPI.h"
#import "EndListApI.h"
@implementation HomeVM
-(RACSignal*)homeDataCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        HomeAPI *req = [HomeAPI new];
        req.type = self.type;
        req.pageSize =self.pageSize;
        req.pageNum =self.page;
        
        [req startWithCompletionBlockWithSuccess:^(HomeAPI * request) {
            [subscriber sendNext:request];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}

-(RACSignal*)endDataCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        EndListApI *req = [EndListApI new];
        req.pageSize =self.pageSize;
        req.pageNum =self.page;
        
        [req startWithCompletionBlockWithSuccess:^(HomeAPI * request) {
            [subscriber sendNext:request];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}
@end

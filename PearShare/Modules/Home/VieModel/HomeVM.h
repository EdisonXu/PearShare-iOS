//
//  HomeVM.h
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface HomeVM : BaseViewModel
@property (nonatomic,strong)NSString*type;

-(RACSignal*)homeDataCommand;

-(RACSignal*)endDataCommand;
@end

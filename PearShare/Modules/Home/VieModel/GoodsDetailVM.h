//
//  GoodsDetailVM.h
//  PearShare
//
//  Created by Edison on 2018/8/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface GoodsDetailVM : BaseViewModel
@property (nonatomic,strong)NSString * goodsId;

-(RACSignal*)goodsInfo;
@end

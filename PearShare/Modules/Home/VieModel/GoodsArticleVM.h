//
//  GoodsArticleVM.h
//  PearShare
//
//  Created by Destiny on 2018/6/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface GoodsArticleVM : BaseViewModel
@property (nonatomic,strong)NSString * shareId;
@property (nonatomic,strong)NSString * comment;
@property (nonatomic,strong)NSString * commentType;
-(RACSignal*)detailCommand;

-(RACSignal*)hotComment;

-(RACSignal*)hotShare;

-(RACSignal*)sendComment;
@end

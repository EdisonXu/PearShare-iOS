//
//  PublishVM.h
//  PearShare
//
//  Created by Destiny on 2018/7/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface PublishVM : BaseViewModel
@property (nonatomic,strong)NSString *pid;
@property (nonatomic,strong)NSString * title;
@property (nonatomic,strong)NSString * content;
@property (nonatomic,strong)NSString * imgs;
@property (nonatomic,strong)NSString * classify;
@property (nonatomic,strong)NSString * tags;
-(RACSignal*)systemDicsCommand;
-(RACSignal*)pushCommand;
@end

//
//  GoodsArticleVM.m
//  PearShare
//
//  Created by Destiny on 2018/6/27.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsArticleVM.h"
#import "ArticleDetialAPI.h"
#import "CommentAPI.h"
#import "HotShareAPI.h"
#import "SendCommentAPI.h"
#import "FavoriteAPI.h"
#import "HomeArticleModel.h"
@interface GoodsArticleVM()
@property (nonatomic,strong)NSMutableArray * articleList;
@property (nonatomic,strong)NSMutableArray * modeList;
@end
@implementation GoodsArticleVM

-(RACSignal*)detailCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
        ArticleDetialAPI *req = [ArticleDetialAPI new];
        req.shareId = self.shareId;
        [req startWithCompletionBlockWithSuccess:^(ArticleDetialAPI * request) {
            [hud hideAnimated:YES];
            if ([request isSuccess]) {
                [subscriber sendNext:request.result];
                [subscriber sendCompleted];
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text = [request message];
                [hud hideAnimated:YES afterDelay:1.f];
            }
        } failure:^(ArticleDetialAPI * request) {
            [hud hideAnimated:YES];
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)hotComment
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        CommentAPI *req = [CommentAPI new];
        req.shareId = self.shareId;
        req.type = self.commentType;
        
        [req startWithCompletionBlockWithSuccess:^(CommentAPI *  request) {
            if ([request isSuccess]) {
                [subscriber sendNext:[request.result dictionaryForKey:@"data"]];
                [subscriber sendCompleted];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

        }];
        return nil;
    }];
    return signal;
}


-(RACSignal*)hotShare
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        HotShareAPI *req = [HotShareAPI new];
        
        [req startWithCompletionBlockWithSuccess:^(HotShareAPI * request) {
            if ([request isSuccess]) {
                self.articleList = [[NSMutableArray alloc] initWithArray:[request.responseObject arrayForKey:@"data"]];
                [self setModelListCommand];
                [subscriber sendNext:self.modeList];
                [subscriber sendCompleted];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)sendComment
{
        RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            SendCommentAPI *req = [SendCommentAPI new];
            req.shareId = self.shareId;
            req.comment = self.comment;
            [req startWithCompletionBlockWithSuccess:^(SendCommentAPI * request) {
                if ([request isSuccess]) {
                    [subscriber sendNext:@"yes"];
                    [subscriber sendCompleted];
                }else{
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
                    hud.mode = MBProgressHUDModeText;
                    hud.label.text = [request message];
                    [hud hideAnimated:YES afterDelay:1.f];
                }
            } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                
            }];
        return nil;
        }];
    return signal;
}

-(RACSignal*)wantCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        FavoriteAPI *req = [FavoriteAPI new];
//        req.collectionType = sender.selected?@"0":@"1";
//        req.articleIdList =[list componentsJoinedByString:@","];
        
        [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            if ([req isSuccess]) {
                
            }else{
                
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
        return nil;
    }];
    return signal;
}

-(NSArray*)setModelListCommand
{
    self.modeList =[NSMutableArray array];
    if (self.articleList.count>0) {
        for (int i = 0; i<self.articleList.count; i++) {
            HomeArticleModel* model =[[HomeArticleModel alloc] initWithDictionary:[self.articleList objectAtIndex:i] error:nil];
            [self.modeList addObject:model];
        }
    }
    return self.modeList;
}

@end

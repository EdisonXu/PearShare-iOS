//
//  GoodsDetailVM.m
//  PearShare
//
//  Created by Edison on 2018/8/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsDetailVM.h"
#import "GoodsInfoAPI.h"
@implementation GoodsDetailVM
-(RACSignal*)goodsInfo
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
        GoodsInfoAPI *req = [GoodsInfoAPI new];
        req.goodsId = self.goodsId;
        [req startWithCompletionBlockWithSuccess:^( GoodsInfoAPI *  request) {
            [hud hideAnimated:YES];
            if ([request isSuccess]) {
                [subscriber sendNext:[request.result dictionaryForKey:@"data"]];
                [subscriber sendCompleted];
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text = [request message];
                [hud hideAnimated:YES afterDelay:1.f];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [hud hideAnimated:YES];
        }];
        return nil;
    }];
    return signal;
}
@end

//
//  GoodsInfoVM.m
//  PearShare
//
//  Created by Destiny on 2018/7/10.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsInfoVM.h"
#import "GoodsInfoAPI.h"

@implementation GoodsInfoVM
-(RACSignal*)goodsInfo
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
        GoodsInfoAPI *req = [GoodsInfoAPI new];
        req.goodsId = self.goodsId;
        [req startWithCompletionBlockWithSuccess:^( GoodsInfoAPI *  request) {
            [hud hideAnimated:YES];
            if ([request isSuccess]) {
                [subscriber sendNext:[request.result dictionaryForKey:@"data"]];
                [subscriber sendCompleted];
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.controller.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text = [request message];
                [hud hideAnimated:YES afterDelay:1.f];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [hud hideAnimated:YES];
        }];
        return nil;
    }];
    return signal;
}
@end

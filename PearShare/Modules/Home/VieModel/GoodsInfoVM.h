//
//  GoodsInfoVM.h
//  PearShare
//
//  Created by Destiny on 2018/7/10.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface GoodsInfoVM : BaseViewModel
@property (nonatomic,strong)NSString * goodsId;

-(RACSignal*)goodsInfo;
@end

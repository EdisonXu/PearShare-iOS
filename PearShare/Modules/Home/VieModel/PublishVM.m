//
//  PublishVM.m
//  PearShare
//
//  Created by Destiny on 2018/7/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "PublishVM.h"
#import "SystemDicsAPI.h"
#import "PushAPI.h"
#import "SelectedListModel.h"
@implementation PublishVM
-(RACSignal*)systemDicsCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        SystemDicsAPI *req = [SystemDicsAPI new];
        req.pid = self.pid;
        
        [req startWithCompletionBlockWithSuccess:^(SystemDicsAPI * request) {
            if ([self.pid isEqualToString:@"22"]) {
                NSMutableArray* arry = [NSMutableArray array];
                [[request.result arrayForKey:@"data"] enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [arry addObject:[[SelectedListModel alloc] initWithSid:[obj integerForKey:@"id"] Title:[obj stringForKey:@"name"]]];
                }];
                [subscriber sendNext:arry];
                [subscriber sendCompleted];
            }else{
                [subscriber sendNext:[request.result arrayForKey:@"data"]];
                [subscriber sendCompleted];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        return nil;
    }];
    return signal;
}

-(RACSignal*)pushCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.param options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[URL_main stringByAppendingString:@"/shares/publishShare"] parameters:nil error:nil];
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
        [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
                [subscriber sendNext:[responseObject stringForKey:@"success"]];
                [subscriber sendCompleted];
        }] resume];
        return nil;
    }];
    return signal;
}



@end

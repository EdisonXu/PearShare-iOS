//
//  DetailMangager.m
//  PearShare
//
//  Created by Edison on 2018/8/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DetailMangager.h"
#import "ShareDetail.h"
#import "GoodsDetail.h"
@interface DetailMangager ()
@property (nonatomic,strong)NSArray * itemList;
@end

@implementation DetailMangager

-(instancetype)init
{
    if (self = [super init]) {
        self.menuViewStyle = WMMenuViewStyleLine;
        self.showOnNavigationBar = YES;
        self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
        self.titleColorNormal = [UIColor colorWithHexString:@"828282"];
        self.progressColor = [UIColor colorWithHexString:@"fed000"];
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 16;
        self.progressHeight = 2;
        self.progressWidth = 40;
        self.typeCocoler = [UIColor clearColor];
        self.preloadPolicy = 1;
        self.titleFontName = @"Courier-Bold";

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self.type isEqualToString:@"1"]) {
        _itemList = [[NSArray alloc] initWithObjects:@"分享", nil];
    }else{
        _itemList = [[NSArray alloc] initWithObjects:@"分享",@"好货", nil];
    }
    [self reloadData];
}


#pragma mark-dataSouce
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.itemList.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.itemList stringWithIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index==0) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        ShareDetail *vc = [storyboard instantiateViewControllerWithIdentifier:@"ShareDetail"];
         if ([self.type isEqualToString:@"1"]) {
             vc.commentType = @"1";
         }else{
             vc.commentType = @"2";
         }
        vc.shareId =self.shareId;
        vc.tapBlcok = ^(BOOL select) {
            self.selectIndex = 1;
        };
        return vc;
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        GoodsDetail *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoodsDetail"];
        vc.goodsId =self.goodsId;
        return vc;
    }
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width ;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : CGRectGetMaxY(self.navigationController.navigationBar.frame);
    return CGRectMake(leftMargin, originY, self.view.frame.size.width - 2*leftMargin, 30);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}




@end

//
//  GoodsArticle.m
//  PearShare
//
//  Created by Destiny on 2018/7/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsDetail.h"
#import "GoodsDetailVM.h"
#import "PictureHeader.h"
#import "CommentHeader.h"
#import "GoodsTopCell.h"
#import "GoodsDiscountCell.h"
#import "GoodsLiziCell.h"
#import "CollocationCell.h"
#import "CountDown.h"
#import <AlibcTradeBiz/AlibcTradeBiz.h>
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <AlibabaAuthSDK/albbsdk.h>
#import "DetailMangager.h"

@interface GoodsDetail ()<UITableViewDelegate,UITableViewDataSource>
{
    GoodsDetailVM * _viewModel;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIView *ToolView;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic)  CountDown *countDown;

@property (nonatomic,strong)NSDictionary* data;
@property(nonatomic,strong)NSArray *imageUrlList;
@property(nonatomic,strong)NSMutableArray *imageSizeLsit;
@property(nonatomic,strong)NSMutableArray *imageModelArr;

@property (nonatomic,strong)PictureHeader* header;
@end

@implementation GoodsDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    [MobClick event:@"0010"];
    _viewModel = [[GoodsDetailVM alloc]init];
    _viewModel.controller = self;
    
    self.data = [NSDictionary dictionary];
    [self setUpTable];
    [self requestData];
}

-(void)updateTimeInVisibleCells{
    NSArray  *cells = self.tableView.visibleCells; //取出屏幕可见ceLl
    for (GoodsTopCell *cell in cells) {
        if ([cell isKindOfClass:[GoodsTopCell class]]) {
            NSArray* list = [NSArray arrayWithArray:[self getNowTimeWithString:[self getDateStringWithTimeStr:[self.data stringForKey:@"endTime"]]]];
            cell.lblDay.text = [list stringWithIndex:0];
            cell.lblHour.text = [list stringWithIndex:1];
            cell.lblMin.text = [list stringWithIndex:2];
            cell.lblsecond.text = [list stringWithIndex:3];
        }
    }
}

-(void)setUpTable
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:GoodsTopCell.nib forCellReuseIdentifier:@"GoodsTopCell"];
    [self.tableView registerNib:GoodsDiscountCell.nib forCellReuseIdentifier:@"GoodsDiscountCell"];
    [self.tableView registerNib:GoodsLiziCell.nib forCellReuseIdentifier:@"GoodsLiziCell"];
    [self.tableView registerNib:CollocationCell.nib forCellReuseIdentifier:@"CollocationCell"];
    
}

#pragma mark_详情请求
-(void)requestData
{
    _viewModel.goodsId=self.goodsId;
    RACSignal * signal = _viewModel.goodsInfo;
    __weak typeof(self) weakSelf = self;
    [signal subscribeNext:^(NSDictionary* detail) {
        weakSelf.tableView.delegate = self;
        weakSelf.tableView.dataSource = self;
        weakSelf.data  = detail;
        [self.tableView reloadData];
        [weakSelf setUpPictureheader];
        self.countDown = [[CountDown alloc] init];
        ///每秒回调一次
        [weakSelf.countDown countDownWithPER_SECBlock:^{
            [weakSelf updateTimeInVisibleCells];
        }];
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return [self.data arrayForKey:@"goodsOffers"].count;
    }else if (section==2){
        return 1;
    }else{
        return [self.data arrayForKey:@"goodsRelateds"].count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        GoodsTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsTopCell" forIndexPath:indexPath];
        cell.data = self.data;
        return cell;
    }else if (indexPath.section==1){
        GoodsDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsDiscountCell" forIndexPath:indexPath];
        cell.model = [[self.data arrayForKey:@"goodsOffers"] dictionaryWithIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==2){
        GoodsLiziCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsLiziCell" forIndexPath:indexPath];
        cell.lblConent.text = [self.data stringForKey:@"pearSay"];
        return cell;
    }else{
        CollocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CollocationCell" forIndexPath:indexPath];
        cell.model = [[self.data arrayForKey:@"goodsRelateds"] dictionaryWithIndex:indexPath.row];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==3) {
        return 131;
    }else{
      return UITableViewAutomaticDimension;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"优惠和售后";
        return header;
    }else if (section==2){
       CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"梨子有话说";
        return header;;
    }else if (section==3){
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"相关搭配";
        return header;;
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return 30;
    }else if (section==2||section==3){
        return 30;
    }else{
        return 0.01;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 8;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==3) {
        DetailMangager* vc = [[DetailMangager alloc] init];
        vc.shareId = [[[self.data arrayForKey:@"goodsRelateds"] dictionaryWithIndex:indexPath.row] stringForKey:@"shareIdRelated"];
        vc.goodsId = [[[self.data arrayForKey:@"goodsRelateds"] dictionaryWithIndex:indexPath.row] stringForKey:@"relatedGoodsId"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)setUpPictureheader
{
    self.header = [[PictureHeader alloc]initWithmodel:[self setUpHeader:self.data]];
    self.tableView.tableHeaderView = self.header;
    __weak typeof(self) weakSelf = self;
    self.header.changeHeightBlock = ^(CGSize passedValue){
        weakSelf.header.frame = CGRectMake(0, 0, SCREEN_WIDTH, passedValue.height);
        weakSelf.tableView.tableHeaderView = weakSelf.header;
        [weakSelf viewIfLoaded];
    };
}



- (NSString *)getDateStringWithTimeStr:(NSString *)str{
    NSTimeInterval time=[str doubleValue];//传入的时间戳str如果是精确到毫秒的记得要/1000
    NSDate *detailDate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; //实例化一个NSDateFormatter对象
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentDateStr = [dateFormatter stringFromDate: detailDate];
    return currentDateStr;
}

-(NSArray *)getNowTimeWithString:(NSString *)aTimeString{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 截止时间date格式
    NSDate  *expireDate = [formater dateFromString:aTimeString];
    NSDate  *nowDate = [NSDate date];
    // 当前时间字符串格式
    NSString *nowDateStr = [formater stringFromDate:nowDate];
    // 当前时间date格式
    nowDate = [formater dateFromString:nowDateStr];
    
    NSTimeInterval timeInterval =[expireDate timeIntervalSinceDate:nowDate];
    
    int days = (int)(timeInterval/(3600*24));
    int hours = (int)((timeInterval-days*24*3600)/3600);
    int minutes = (int)(timeInterval-days*24*3600-hours*3600)/60;
    int seconds = timeInterval-days*24*3600-hours*3600-minutes*60;
    
    NSString *dayStr;NSString *hoursStr;NSString *minutesStr;NSString *secondsStr;
    //天
    dayStr = [NSString stringWithFormat:@"%d",days];
    //小时
    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<10)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    //秒
    if(seconds < 10)
        secondsStr = [NSString stringWithFormat:@"0%d", seconds];
    else
        secondsStr = [NSString stringWithFormat:@"%d",seconds];
    if (hours<=0&&minutes<=0&&seconds<=0) {
        return @[@"0",@"0",@"0",@"0"];
    }
    if (days) {
        return @[dayStr,hoursStr,minutesStr,secondsStr];
        
    }
    return@[@"0",hoursStr,minutesStr,secondsStr];
}

-(NSInteger)getDayNumberWithYear:(NSInteger )y month:(NSInteger )m{
    int days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    if (2 == m && 0 == (y % 4) && (0 != (y % 100) || 0 == (y % 400))) {
        days[1] = 29;
    }
    return (days[m - 1]);
}
- (IBAction)taobaoCommand:(id)sender
{
    
    [MobClick event:@"0004"];
        id<AlibcTradePage> page = [AlibcTradePageFactory page:[self.data stringForKey:@"url"]];
        //淘客信息
        AlibcTradeTaokeParams *taoKeParams=[[AlibcTradeTaokeParams alloc] init];
        taoKeParams.pid= @"mm_44889980_43066955_279234503";
        //打开方式
        AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
        showParam.openType = AlibcOpenTypeNative;
        [[AlibcTradeSDK sharedInstance] .tradeService show:self.navigationController page:page showParams:showParam taoKeParams:taoKeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
}
@end

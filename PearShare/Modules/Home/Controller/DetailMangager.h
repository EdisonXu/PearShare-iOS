//
//  DetailMangager.h
//  PearShare
//
//  Created by Edison on 2018/8/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "WMPageController.h"

@interface DetailMangager : WMPageController
@property (nonatomic,strong)NSString * shareId;
@property (nonatomic,strong)NSString * goodsId;
@property (nonatomic,strong)NSString * type;
@end

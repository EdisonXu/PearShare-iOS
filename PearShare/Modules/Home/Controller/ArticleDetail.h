//
//  ArticleDetail.h
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MountingViewController.h"
#import "WSTextView.h"
@interface ArticleDetail : MountingViewController
@property (nonatomic,strong)NSString * shareId;
@property (weak, nonatomic) IBOutlet WSTextView *commentText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewH;
@property (weak, nonatomic) IBOutlet UIButton *btnWant;
@end

//
//  HomeCollectionVC.h
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewController.h"

@interface HomeCollectionVC : BaseViewController
@property (nonatomic,strong)NSString *type;
@property (nonatomic,strong)NSString *position;

typedef void(^CallBackBlcok) (NSString *text);//1


@property (nonatomic,copy)CallBackBlcok callBackBlock;//2

@end

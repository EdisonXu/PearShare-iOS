//
//  ClassifyManager.m
//  PearShare
//
//  Created by Edison on 2018/7/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ClassifyManager.h"
#import "HomeCollectionVC.h"
#import "SystemDicsAPI.h"
#import "PPNetworkHelper.h"
@interface ClassifyManager ()
@property (nonatomic,strong)NSMutableArray*itemList;
@property (nonatomic,assign)CGFloat meuHeight;
@end

@implementation ClassifyManager

-(instancetype)init
{
    if (self = [super init]) {
        self.meuHeight = 30;
        self.showOnNavigationBar = NO;
        self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
        self.titleColorNormal = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
        self.titleSizeNormal = 14;
        self.titleSizeSelected = 14;
        self.itemMargin = 45;
        self.automaticallyCalculatesItemWidths = YES;
        self.typeCocoler =[UIColor colorWithHexString:@"EEEEEE"];
        self.titleFontName = @"Courier-Bold";
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.menuView.backgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
}

-(void)setUpItemList
{
    NSDictionary*param = [NSDictionary dictionaryWithObject:@"1" forKey:@"type"];
    [PPNetworkHelper GET:[[URL_main stringByAppendingString:@"/system/dics/"]stringByAppendingString:@"4"] parameters:param success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
            self.itemList = [NSMutableArray arrayWithArray:[responseObject arrayForKey:@"data"]];
            NSMutableDictionary* all = [NSMutableDictionary dictionaryWithObject:@"全部" forKey:@"name"];
            [self.itemList insertObject:all atIndex:0];
            [self reloadData];
        }
    } failure:^(NSError *error) {
        
    }];
}


-(NSMutableArray*)itemList
{
    if (!_itemList) {
        if ([kDefaults objectForKey:@"homeClass"]) {
            _itemList = [NSMutableArray arrayWithArray:[kDefaults arrayForKey:@"homeClass"]];
        }else{
            [self setUpItemList];
        }
    }
    return _itemList;
}

#pragma mark-dataSouce
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.itemList.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [[self.itemList dictionaryWithIndex:index] stringForKey:@"name"];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
    HomeCollectionVC *vc = [sb instantiateViewControllerWithIdentifier:@"HomeCollectionVC"];
    vc.position = @"left";
    vc.type =  [[self.itemList dictionaryWithIndex:index] stringForKey:@"id"];
    return vc;
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width ;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : 0;
    return CGRectMake(leftMargin, originY, self.view.frame.size.width - 2*leftMargin, self.meuHeight);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

@end

//
//  PublishVC.m
//  PearShare
//
//  Created by Destiny on 2018/7/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "PublishVC.h"
#import "AddPictureCell.h"
#import "PublishVM.h"
#import "SelectedListView.h"
#import "LEEAlert.h"
#import "SKTagView.h"
#import "TagViewCell.h"
#import "PPNetworkHelper.h"
#import "HXPhotoPicker.h"
@interface PublishVC ()<HXPhotoViewDelegate>
{
    Demo9Model * _model;
    SelectedListView *_selectView;
}
@property (weak, nonatomic) IBOutlet UILabel *lblClassify;
@property (weak, nonatomic) IBOutlet UILabel *lblTagCount;
@property (weak, nonatomic) IBOutlet UITextField *textTitle;
@property (weak, nonatomic) IBOutlet UITextView *textConenct;
@property (nonatomic, strong) NSMutableArray *tagList;
@property (nonatomic, strong) NSMutableArray *photoList;
@property (nonatomic,strong)  PublishVM * viewModel;
@end

@implementation PublishVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self setUpTag];
}

-(void)setUpView
{
    self.title = @"发布内容";
    self.tableView.tableFooterView = [[UIView alloc] init];
    _model = [[Demo9Model alloc] init];
    _viewModel = [[PublishVM alloc]init];
    UIBarButtonItem *myButton = [[UIBarButtonItem alloc] initWithTitle:@"发布" style:UIBarButtonItemStylePlain target:self action:@selector(pushEvent)];
    self.navigationItem.rightBarButtonItem = myButton;
    self.photoList =[NSMutableArray array];
    self.tagList = [NSMutableArray array];
    [self.textTitle addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)setUpTag
{
    _viewModel.pid = @"22";
    RACSignal * signal = [_viewModel systemDicsCommand];
    [signal subscribeNext:^(NSArray* list) {
    __weak __typeof(self)weakSelf = self;
    [list enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [weakSelf.tagList addObject:[obj stringForKey:@"name"]];
    }];
        [self.tableView reloadData];
    }];
}

-(void)setUpClassify
{
    _viewModel.pid = @"4";
    RACSignal * signal = [_viewModel systemDicsCommand];
    [signal subscribeNext:^(NSArray* list) {
       _selectView = [[SelectedListView alloc] initWithFrame:CGRectMake(0, 0, 280, 0) style:UITableViewStylePlain];
        _selectView.isSingle = YES;
        _selectView.array =  list;
        __weak __typeof(self)weakSelf = self;
        _selectView.selectedBlock = ^(NSArray<SelectedListModel *> *array) {
            [LEEAlert closeWithCompletionBlock:^{
                 weakSelf.lblClassify.text = [NSString stringWithFormat:@"%@",[[array dictionaryWithIndex:0] stringForKey:@"name"]];
                [weakSelf.viewModel.param setString:[NSString stringWithFormat:@"%ld",[[array dictionaryWithIndex:0] stringForKey:@"pid"]]  forKey:@"classify" ];
            }];
        };
        [LEEAlert alert].config
        .LeeTitle(@"分类")
        .LeeItemInsets(UIEdgeInsetsMake(20, 0, 20, 0))
        .LeeCustomView(_selectView)
        .LeeItemInsets(UIEdgeInsetsMake(0, 0, 0, 0))
        .LeeHeaderInsets(UIEdgeInsetsMake(10, 0, 0, 0))
        .LeeClickBackgroundClose(YES)
        .LeeShow();
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1){//爱好 （动态cell）
        return 1;
    }
    
    if (section==3) {
        return 1;
    }
    
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        AddPictureCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddPictureCell"];
        if(!cell){
            cell = [[AddPictureCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddPictureCell"];
        }
        
        cell.model = _model;
        __weak typeof(self) weakSelf = self;
        
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
           [weakSelf.tableView reloadRowsAtIndexPaths:@[[weakSelf.tableView indexPathForCell:mycell]] withRowAnimation:0];
            [weakSelf setUpPhotoList];
        }];
        
        cell.addToCartsBlock = ^(AddPictureCell *cell) {
            [weakSelf setUpPhotoList];
        };
        return cell;
    }
    if(indexPath.section==3){
        TagViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TagViewCell"];
        if(!cell){
            cell = [[TagViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TagViewCell"];
        }
        __weak typeof(self) weakSelf = self;
        
        [cell.tagView selectAction:^(SQButtonTagView * _Nonnull tagView, NSArray * _Nonnull selectArray) {
            _lblTagCount.text = [NSString stringWithFormat:@"%lu%@",(unsigned long)selectArray.count,@"/5"];
            [weakSelf setUpSelectTag:selectArray];
        }];
        [cell setTextArray:_tagList row:indexPath.section];
        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==2&&indexPath.row==0) {
            [self setUpClassify];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1) {
        Demo9Model *model = _model;
        return model.cellHeight+16;
    }else if (indexPath.section==3){
       return [TagViewCell cellHeightTextArray:_tagList Row:indexPath.section];
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

//cell的缩进级别，动态静态cell必须重写，否则会造成崩溃
- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        return [super tableView:tableView indentationLevelForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:1]];
    }
    return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

-(void)pushEvent
{
    if (self.textTitle.text.length&&self.textConenct.text.length&&self.photoList.count&&[self.viewModel.param stringForKey:@"classify"]&&[self.viewModel.param stringForKey:@"tags"]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/batchuploadFile"]parameters:nil name:@"files" images:self.photoList fileNames:nil imageScale:0.8 imageType:nil progress:^(NSProgress *progress) {
            NSLog(@"%@",progress);
        } success:^(NSDictionary* responseObject) {
            NSLog(@"%@",responseObject);
            [hud hideAnimated:YES];
            [_viewModel.param setString:[[[responseObject dictionaryForKey:@"data"] arrayForKey:@"urls"] componentsJoinedByString:@","] forKey:@"imgs" ];
            [_viewModel.param setString:self.textTitle.text  forKey:@"title" ];
            [_viewModel.param setString:self.textConenct.text  forKey:@"content" ];
            [self push];
        } failure:^(NSError *error) {
            [hud hideAnimated:YES];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"发布失败，请稍后重试"];
            [hud hideAnimated:YES afterDelay:1.f];
        }];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"请完善发布信息"];
        [hud hideAnimated:YES afterDelay:1.f];
    }
}

-(void)push
{
    RACSignal *signal = [_viewModel pushCommand];
    [signal subscribeNext:^(NSString* data) {
        if([data isEqualToString:@"1"]){
            [self.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName: Qnotifiction_PushSucess object:nil];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"发布失败，请稍后重试"];
            [hud hideAnimated:YES afterDelay:1.f];
        }
    }];
}

-(void)setUpPhotoList
{
    if (_model.endSelectedList.count) {
        [self.photoList removeAllObjects];
        NSMutableArray * list =[NSMutableArray array];
        __weak typeof(self) weakSelf = self;
        [_model.endSelectedList enumerateObjectsUsingBlock:^(HXPhotoModel* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [list addObject:obj.asset];
        }];
        
        [list enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [HXPhotoTools getHighQualityFormatPhoto:obj size:CGSizeMake(750, 750) succeed:^(UIImage *image) {
                [weakSelf.photoList addObject:image];
            } failed:^{
                
            }];
        }];
    }else{
        [self.photoList removeAllObjects];
    }
}

-(void)setUpSelectTag:(NSArray*)lis
{
    NSMutableArray * tags =[NSMutableArray array];
    [lis enumerateObjectsUsingBlock:^(NSString* obj1, NSUInteger idx1, BOOL * _Nonnull stop) {
        NSInteger tag = [obj1 integerValue];
        [tags addObject:[_tagList objectAtIndex:tag]];
    }];
    [self.viewModel.param setString:[tags componentsJoinedByString:@","]  forKey:@"tags" ];
}

#pragma mark text代理

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.textTitle) {
        if (textField.text.length > 30) {
            textField.text = [textField.text substringToIndex:30];
        }
    }
}

@end

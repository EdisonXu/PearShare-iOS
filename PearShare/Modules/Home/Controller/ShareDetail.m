//
//  ShareDetail.m
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ShareDetail.h"
#import "ArticleTopCell.h"
#import "CommentTopCell.h"
#import "CommentSonCell.h"
#import "AllCommentCell.h"
#import "HotShareCell.h"
#import "PictureHeader.h"
#import "CommentHeader.h"
#import "GoodsArticleVM.h"
#import "AllReplyList.h"
#import "FavoriteAPI.h"
#import "UIButton+SLYExtension.h"
#import "LoginVC.h"

@interface ShareDetail ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>
@property (nonatomic,strong) GoodsArticleVM*    viewModel;
@property (nonatomic,strong)PictureHeader* header;
@property (nonatomic,assign)BOOL  isCollect;
@property (nonatomic,assign)NSInteger totalRow;
@property (nonatomic,strong) NSDictionary* detailDic;
@property (nonatomic,strong)NSArray* commentList;
@property (nonatomic,strong)NSMutableArray* hotList;
@property (nonatomic,strong)NSArray * modeList;
@property (nonatomic,assign)NSInteger  likeCount;
@property(nonatomic,strong)NSArray *imageUrlList;
@property(nonatomic,strong)NSMutableArray *imageSizeLsit;
@property(nonatomic,strong)NSMutableArray *imageModelArr;
@end

@implementation ShareDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[GoodsArticleVM alloc]initWith:self];
    self.viewModel.controller =self;
    [self setUpView];
    [self requestDataCommand];
    if ([self.commentType isEqualToString:@"1"]) {
        [self requestHotCommend];
    }
    
    [MobClick event:@"0009"];
    
}

#pragma mark_ 配置初始化页面
-(void)setUpView
{
    //配置table
    self.tableView.tableFooterView = [UIView new];

    [self.tableView registerNib:ArticleTopCell.nib forCellReuseIdentifier:@"ArticleTopCell"];
    [self.tableView registerNib:CommentTopCell.nib forCellReuseIdentifier:@"CommentTopCell"];
    [self.tableView registerNib:CommentSonCell.nib forCellReuseIdentifier:@"CommentSonCell"];
    [self.tableView registerNib:AllCommentCell.nib forCellReuseIdentifier:@"AllCommentCell"];
    [self.tableView registerNib:HotShareCell.nib forCellReuseIdentifier:@"HotShareCell"];
    
    if (![self.commentType isEqualToString:@"1"]) {
        @weakify(self);
        self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
            @strongify(self);
            [self.tableView.mj_footer endRefreshing];
            self.tapBlcok(YES);
        }];
        
        [((MJRefreshBackStateFooter*)self.tableView.mj_footer) setTitle:@" " forState:MJRefreshStateIdle];
        [((MJRefreshBackStateFooter*)self.tableView.mj_footer)  setTitle:@" " forState:MJRefreshStatePulling];
        [((MJRefreshBackStateFooter*)self.tableView.mj_footer)  setTitle:@" " forState:MJRefreshStateRefreshing];
    }else{
//        @weakify(self);
//        self.tableView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
//            @strongify(self);
//            [self moreHotCommend];
//        }];
    }
    
    self.commentText.returnKeyType = UIReturnKeySend;//变为搜索按钮
    self.commentText.delegate = self;//设置代理
    
    //评论键盘
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = NO;
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    //设置最大行数
    self.commentText.maxNumberOfLines = 3;
    //设置提示内容
    self.commentText.placeHolder = @"说点什么吧～";
    self.commentText.placeHolderColor = [UIColor lightGrayColor];
    //高度改变的回调
    __weak typeof(self) wself = self;
    self.commentText.ws_textHeightChangeHandle = ^(NSString *text, CGFloat height){
        //20是textView上下边距的和
        wself.bottomViewH.constant = height + 16;
        [UIView animateWithDuration:0.25 animations:^{
            [wself.view layoutIfNeeded];
        }];
    };
}

#pragma mark_详情请求
-(void)requestDataCommand
{
    self.viewModel.shareId=self.shareId;
    RACSignal * signal = self.viewModel.detailCommand;
    [signal subscribeNext:^(NSDictionary* detail) {
        self.detailDic = [NSDictionary dictionaryWithDictionary:[detail dictionaryForKey:@"data"]];
        self.commentList = [NSArray arrayWithArray:[ [self.detailDic dictionaryForKey:@"comments"] arrayForKey:@"commentDto"]];
        self.totalRow = [[self.detailDic dictionaryForKey:@"comments"] integerForKey:@"totalRow"];
        self.isCollect =  [[self.detailDic stringForKey:@"isCollections"] boolValue];
        self.likeCount =  [[self.detailDic stringForKey:@"collectionsNum"] integerValue];
        [self setViewData];
        [self setUpPictureheader];
        self.tableView.delegate = self;
        self.tableView.dataSource =self;
        [self.tableView reloadData];
    }];
}

#pragma mark_ 热门
-(void)requestHotCommend
{
    RACSignal * signal = self.viewModel.hotShare;
    [signal subscribeNext:^(NSArray* hot) {
        self.hotList = [[NSMutableArray arrayWithArray:hot] mutableCopy];
        [self.tableView reloadData];
    }];
}

-(void)moreHotCommend
{
    [self.viewModel addPage];
    RACSignal * signal = self.viewModel.hotShare;
    [signal subscribeNext:^(NSArray* hot) {
        [self.tableView.mj_footer endRefreshing];
        NSArray *more = [NSArray arrayWithArray:hot];
        if (hot.count>0) {
            [self.hotList addObjectsFromArray:more];
        }
        [self.tableView reloadData];
    }];
}

-(void)setUpPictureheader
{
    self.header = [[PictureHeader alloc]initWithmodel:[self setUpHeader:self.detailDic]];
    self.tableView.tableHeaderView = self.header;
    __weak typeof(self) weakSelf = self;
    self.header.changeHeightBlock = ^(CGSize passedValue){
        weakSelf.header.frame = CGRectMake(0, 0, SCREEN_WIDTH, passedValue.height);
        weakSelf.tableView.tableHeaderView = weakSelf.header;
        [weakSelf viewIfLoaded];
    };
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.commentType isEqualToString:@"1"]?self.commentList.count+3:self.commentList.count+2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0||section==self.commentList.count+1||section==self.commentList.count+2) {
        return 1;
    }else{
        if ([[self.commentList dictionaryWithIndex:section-1]arrayForKey:@"commentReplys"].count) {
            return 2;
        }else{
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        ArticleTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleTopCell" forIndexPath:indexPath];
        cell.model = self.detailDic;
        cell.lblLike.text = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%ld",self.likeCount]];
        [cell.btnWantCount setTitle:[NSString stringWithFormat:@"  %ld人想买",(long)self.likeCount] forState:UIControlStateNormal];
        if (self.isCollect) {
            [cell.btnlike select];
            [cell.lblLike setTextColor:[UIColor colorWithHexString:@"FF4A4A"]];
        }else{
            [cell.btnlike deselect];
            [cell.lblLike setTextColor:[UIColor colorWithHexString:@"666666"]];
        }
        return cell;
    }else if (indexPath.section==self.commentList.count+1){
        AllCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AllCommentCell" forIndexPath:indexPath];
        cell.total = self.totalRow;
        return cell;
    }else if (indexPath.section==self.commentList.count+2){
        HotShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotShareCell" forIndexPath:indexPath];
        cell.modelList = self.hotList;
        return cell;
    }else{
        if (indexPath.row==0) {
            CommentTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTopCell" forIndexPath:indexPath];
            cell.model = [self.commentList objectAtIndex:indexPath.section-1];
            return cell;
        }else{
            CommentSonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentSonCell" forIndexPath:indexPath];
            cell.model = [[[self.commentList dictionaryWithIndex:indexPath.section-1]arrayForKey:@"commentReplys"] dictionaryWithIndex:0];
            cell.replyCount = [[self.commentList dictionaryWithIndex:indexPath.section-1] stringForKey:@"replyNum"];
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==self.commentList.count+1&&self.totalRow>0){
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        AllReplyList *vc = [storyboard instantiateViewControllerWithIdentifier:@"AllReplyList"];
        vc.shareId = [self.detailDic stringForKey:@"shareId"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==self.commentList.count+1){
        return  [self.commentType isEqualToString:@"1"]?40:90;
    }else{
        return UITableViewAutomaticDimension;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"精彩评论";
        return header;
    }else if (section==self.commentList.count+2){
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"热门分享";
        return header;
    }
    else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1||section==self.commentList.count+2) {
        return 30;
    }else{
        return 0;
    }
}

#pragma mark - textView data source
- (void)keyboardChange:(NSNotification *)note {
    CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    self.bottomViewBottom.constant = endFrame.origin.y != screenH ? endFrame.size.height:0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){
        __weak typeof(self) wself = self;
        wself.bottomViewH.constant =50;
        [UIView animateWithDuration:0.25 animations:^{
            [wself.view layoutIfNeeded];
        }];
        
        self.viewModel.comment = self.commentText.text;
        RACSignal * signal = self.viewModel.sendComment;
        
        [signal subscribeNext:^(NSString* send) {
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            textView.text = nil;
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"评论发布成功";
            [hud hideAnimated:YES afterDelay:1.f];
            self.totalRow = self.totalRow+1;
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:self.commentList.count+1];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        return NO;
    }
    return YES;
}

- (IBAction)wantCommand:(UIButton*)sender
{
    if (kDefaultLoginAlready) {
        NSMutableArray * list = [NSMutableArray arrayWithObject:self.shareId];
        FavoriteAPI *req = [FavoriteAPI new];
        req.collectionType =   [NSString stringWithFormat:@"%d",self.isCollect];
        req.articleIdList =[list componentsJoinedByString:@","];
        [req startWithCompletionBlockWithSuccess:^(FavoriteAPI* request) {
            if ([request isSuccess]) {
                if (self.isCollect) {
                    self.btnWant.topImage =[UIImage imageNamed:@"want_normal"];
                    self.likeCount = self.likeCount-1;
                } else {
                    self.btnWant.topImage =[UIImage imageNamed:@"want_select"];
                    self.likeCount = self.likeCount+1;
                }
                self.isCollect = !self.isCollect;
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.navigationController presentViewController:navi animated:NO completion:nil];
    }
}

#pragma mark_填充页面数据
-(void)setViewData
{
    if (self.isCollect) {
        self.btnWant.topImage =[UIImage imageNamed:@"want_select"];
        self.btnWant.selected =YES;
    }else{
        self.btnWant.topImage =[UIImage imageNamed:@"want_normal"];
        self.btnWant.selected =NO;
    }
}


@end

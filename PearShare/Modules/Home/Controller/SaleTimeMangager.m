//
//  SaleTimeMangager.m
//  PearShare
//
//  Created by Edison on 2018/7/30.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SaleTimeMangager.h"
#import "HomeCollectionVC.h"
#import "SaleCollectionVC.h"
@interface SaleTimeMangager ()
@property (nonatomic,strong)NSArray*itemList;
@end

@implementation SaleTimeMangager

-(instancetype)init
{
    if (self = [super init]) {
        self.showOnNavigationBar = NO;
        self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
        self.titleColorNormal = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
        self.titleSizeNormal = 14;
        self.titleSizeSelected = 14;
        self.itemMargin = 45;
        self.automaticallyCalculatesItemWidths = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.menuView.backgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
}

-(NSArray*)itemList
{
    if (!_itemList) {
        _itemList = [[NSArray alloc] initWithObjects:@"明日预告",@"今日上新",@"即将结束", nil];
    }
    return _itemList;
}

#pragma mark-dataSouce
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.itemList.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.itemList stringWithIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index==2) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        HomeCollectionVC *vc = [sb instantiateViewControllerWithIdentifier:@"HomeCollectionVC"];
        vc.position = @"right";
        return vc;
    }else{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        SaleCollectionVC *vc = [sb instantiateViewControllerWithIdentifier:@"SaleCollectionVC"];
        vc.type = [NSString stringWithFormat:@"%ld",(long)index];
        
        [MobClick event:@"0007"];
        [MobClick event:@"0006"];
        return vc;
    }
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width ;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : 0;
    return CGRectMake(leftMargin, originY, self.view.frame.size.width - 2*leftMargin, 30);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}


@end

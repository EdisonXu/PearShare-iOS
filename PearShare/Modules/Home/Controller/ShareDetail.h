//
//  ShareDetail.h
//  PearShare
//
//  Created by Edison on 2018/8/3.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewController.h"
#import "WSTextView.h"
#import "MountingViewController.h"
@interface ShareDetail : MountingViewController
@property (nonatomic,strong)NSString * shareId;
@property (nonatomic,strong)NSString * commentType;
@property (weak, nonatomic) IBOutlet WSTextView *commentText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewH;
@property (weak, nonatomic) IBOutlet UIButton *btnWant;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

typedef void(^TapBlcok) (BOOL select);
@property (nonatomic,copy)TapBlcok tapBlcok;
@end

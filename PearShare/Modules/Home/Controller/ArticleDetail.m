//
//  ArticleDetail.m
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ArticleDetail.h"
#import "ArticleTopCell.h"
#import "CommentTopCell.h"
#import "CommentSonCell.h"
#import "AllCommentCell.h"
#import "HotShareCell.h"
#import "CommentHeader.h"
#import "GoodsArticleVM.h"
#import "HotShareAPI.h"
#import "AllCommentCell.h"
#import "AllReplyList.h"
#import "SendCommentAPI.h"
#import "UIButton+SLYExtension.h"
#import "FavoriteAPI.h"
#import "LoginVC.h"
#import "PictureHeader.h"
#import "ImageModel.h"


@interface ArticleDetail ()<UITableViewDataSource,UITableViewDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableTop;
@property (nonatomic,assign)NSInteger totalRow;
@property (nonatomic,strong) NSDictionary* detailDic;
@property (nonatomic,strong)NSArray* commentList;
@property (nonatomic,strong)NSArray * modeList;
@property (nonatomic,assign)BOOL  isCollect;
@property (nonatomic,assign)NSInteger  likeCount;
@property (nonatomic,strong) GoodsArticleVM*    viewModel;
@property (nonatomic,strong)PictureHeader* header;
@property(nonatomic,strong)NSArray *imageUrlList;
@property(nonatomic,strong)NSMutableArray *imageSizeLsit;
@property(nonatomic,strong)NSMutableArray *imageModelArr;
@end

@implementation ArticleDetail

#pragma Mark_lifeCylice
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"想买";
    self.viewModel = [[GoodsArticleVM alloc]initWith:self];
    self.viewModel.controller =self;
    [self setUpView];
    [self requestDataCommand];
    [self requestCommend];
    [self requestHotShare];

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //TODO: 页面appear 禁用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //TODO: 页面Disappear 启用
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
   
}

#pragma mark_ 配置初始化页面
-(void)setUpView
{
    //配置table
    self.tableView.tableFooterView = [UIView new];
    //    self.tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    [self.tableView registerNib:ArticleTopCell.nib forCellReuseIdentifier:@"ArticleTopCell"];
    [self.tableView registerNib:CommentTopCell.nib forCellReuseIdentifier:@"CommentTopCell"];
    [self.tableView registerNib:CommentSonCell.nib forCellReuseIdentifier:@"CommentSonCell"];
    [self.tableView registerNib:AllCommentCell.nib forCellReuseIdentifier:@"AllCommentCell"];
    [self.tableView registerNib:HotShareCell.nib forCellReuseIdentifier:@"HotShareCell"];
    
    self.commentText.returnKeyType = UIReturnKeySend;//变为搜索按钮
    self.commentText.delegate = self;//设置代理
    
    //评论键盘
    [IQKeyboardManager sharedManager].shouldToolbarUsesTextFieldTintColor = NO;
    //监听键盘
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    //设置最大行数
    self.commentText.maxNumberOfLines = 4;
    //设置提示内容
    self.commentText.placeHolder = @"说点什么吧～";
    self.commentText.placeHolderColor = [UIColor lightGrayColor];
    //高度改变的回调
    __weak typeof(self) wself = self;
    self.commentText.ws_textHeightChangeHandle = ^(NSString *text, CGFloat height){
        //20是textView上下边距的和
        wself.bottomViewH.constant = height + 16;
        [UIView animateWithDuration:0.25 animations:^{
            [wself.view layoutIfNeeded];
        }];
    };
}

#pragma mark_详情请求
-(void)requestDataCommand
{
    self.viewModel.shareId=self.shareId;
    RACSignal * signal = self.viewModel.detailCommand;
    [signal subscribeNext:^(NSDictionary* detail) {
        self.detailDic = [NSDictionary dictionaryWithDictionary:[detail dictionaryForKey:@"data"]];
        self.isCollect =  [[self.detailDic stringForKey:@"isCollections"] boolValue];
        self.likeCount =  [[self.detailDic stringForKey:@"collectionsNum"] integerValue];
        [self setViewData];
        [self setUpPictureheader];
        self.tableView.delegate = self;
        self.tableView.dataSource =self;
        [self.tableView reloadData];
    }];
}

#pragma mark_评论请求
-(void)requestCommend
{
    self.viewModel.shareId=self.shareId;
    self.viewModel.commentType = @"1";
    RACSignal * signal = self.viewModel.hotComment;
    [signal subscribeNext:^(NSDictionary* comment) {
        self.commentList = [NSArray arrayWithArray:[comment arrayForKey:@"commentDto"]];
        self.totalRow = [comment integerForKey:@"totalRow"];
        [self.tableView reloadData];
    }];
}

#pragma mark_热门请求
-(void)requestHotShare
{
    RACSignal * signal = self.viewModel.hotShare;
    [signal subscribeNext:^(NSArray* shareList) {
        self.modeList = [NSArray arrayWithArray:shareList];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  self.commentList.count+3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==self.commentList.count+2||section==self.commentList.count+1){
        return 1;
    }else{
        if ([[self.commentList dictionaryWithIndex:section-1]arrayForKey:@"commentReplys"].count) {
            return 2;
        }else{
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        ArticleTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleTopCell" forIndexPath:indexPath];
        cell.model = self.detailDic;
        cell.lblLike.text = [NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:@"%ld",self.likeCount],@"人想买"];
        if (self.isCollect) {
            [cell.btnlike select];
            [cell.lblLike setTextColor:[UIColor colorWithHexString:@"FF4A4A"]];
        }else{
            [cell.btnlike deselect];
            [cell.lblLike setTextColor:[UIColor colorWithHexString:@"666666"]];
        }
        return cell;
    }else if (indexPath.section==self.commentList.count+1){
        AllCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AllCommentCell" forIndexPath:indexPath];
        cell.total = self.totalRow;
        return cell;
    }else if (indexPath.section==self.commentList.count+2){
        HotShareCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotShareCell" forIndexPath:indexPath];
        cell.modelList = self.modeList;
        return cell;
    }else{
        if (indexPath.row==0) {
            CommentTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentTopCell" forIndexPath:indexPath];
            cell.model = [self.commentList objectAtIndex:indexPath.section-1];
            return cell;
        }else{
            CommentSonCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentSonCell" forIndexPath:indexPath];
            cell.model = [[[self.commentList dictionaryWithIndex:indexPath.section-1]arrayForKey:@"commentReplys"] dictionaryWithIndex:0];
            cell.replyCount = [[self.commentList dictionaryWithIndex:indexPath.section-1] stringForKey:@"replyNum"];
            return cell;
        }
    }
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==self.commentList.count+1){
        return 50;
    }else{
        return UITableViewAutomaticDimension;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"精彩评论";
        return header;
    }else if (section==self.commentList.count+2){
        CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
        header.lblTitle.text = @"热门分享";
        return header;
    }else{
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1||section==self.commentList.count+2) {
        return 30;
    }else{
        return 0.01;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (indexPath.section==self.commentList.count+1&&self.totalRow>0){
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
            AllReplyList *vc = [storyboard instantiateViewControllerWithIdentifier:@"AllReplyList"];
            vc.shareId = [self.detailDic stringForKey:@"shareId"];
            [self.navigationController pushViewController:vc animated:YES];
        }
}

-(void)setUpPictureheader
{
        self.header = [[PictureHeader alloc]initWithmodel:[self setUpHeader:self.detailDic]];
        self.tableView.tableHeaderView = self.header;
        __weak typeof(self) weakSelf = self;
        self.header.changeHeightBlock = ^(CGSize passedValue){
            weakSelf.header.frame = CGRectMake(0, 0, SCREEN_WIDTH, passedValue.height);
            weakSelf.tableView.tableHeaderView = weakSelf.header;
            [weakSelf viewIfLoaded];
        };
}
#pragma mark_填充页面数据
-(void)setViewData
{
    if (self.isCollect) {
        self.btnWant.topImage =[UIImage imageNamed:@"want_select"];
        self.btnWant.selected =YES;
    }else{
        self.btnWant.topImage =[UIImage imageNamed:@"want_normal"];
        self.btnWant.selected =NO;
    }
}

- (IBAction)wantCommand:(UIButton*)sender
{
    if (kDefaultLoginAlready) {
        NSMutableArray * list = [NSMutableArray arrayWithObject:self.shareId];
        FavoriteAPI *req = [FavoriteAPI new];
        req.collectionType =   [NSString stringWithFormat:@"%d",self.isCollect];
        req.articleIdList =[list componentsJoinedByString:@","];
        [req startWithCompletionBlockWithSuccess:^(FavoriteAPI* request) {
            if ([request isSuccess]) {
                if (self.isCollect) {
                      self.btnWant.topImage =[UIImage imageNamed:@"want_normal"];
                    self.likeCount = self.likeCount-1;
                } else {
                    self.btnWant.topImage =[UIImage imageNamed:@"want_select"];
                    self.likeCount = self.likeCount+1;
                }
                self.isCollect = !self.isCollect;
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:0];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.navigationController presentViewController:navi animated:NO completion:nil];
    }
}

#pragma mark - textView data source
- (void)keyboardChange:(NSNotification *)note {
    CGRect endFrame = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [note.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGFloat screenH = [UIScreen mainScreen].bounds.size.height;
    
    self.bottomViewBottom.constant = endFrame.origin.y != screenH ? endFrame.size.height:0;
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){
        __weak typeof(self) wself = self;
        wself.bottomViewH.constant =50;
        [UIView animateWithDuration:0.25 animations:^{
            [wself.view layoutIfNeeded];
        }];
        
        self.viewModel.comment = self.commentText.text;
        RACSignal * signal = self.viewModel.sendComment;
        
        [signal subscribeNext:^(NSString* send) {
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            textView.text = nil;
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"评论发布成功";
            [hud hideAnimated:YES afterDelay:1.f];
            self.totalRow = self.totalRow+1;
            NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:self.commentList.count+1];
            [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        return NO;
    }
    return YES;
}

-(void)dealloc
{
    
}
@end

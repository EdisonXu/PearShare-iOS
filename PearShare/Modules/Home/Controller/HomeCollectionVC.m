//
//  HomeCollectionVC.m
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeCollectionVC.h"
#import "HomeItem.h"
#import "HomeVM.h"
#import "HomeAPI.h"
#import "WaterFlowLayout.h"
#import "GoodsDetail.h"
#import "ArticleDetail.h"
#import "HomeArticleModel.h"
#import "GoodsDetail.h"
#import "DetailMangager.h"
#import "PublishVC.h"

@interface HomeCollectionVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WaterFlowLayoutDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) HomeVM * viewModel;
@property (nonatomic,strong)NSMutableArray * articleList;
@property (nonatomic,strong)NSMutableArray * modeList;
@end

#define Cell_Spacing 9

@implementation HomeCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCollectionView];
    self.viewModel = [[HomeVM alloc]init];
    [self.collectionView.mj_header beginRefreshing];
}


-(void)setUpCollectionView
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];
    self.collectionView.dataSource              = self;
    self.collectionView.delegate                   = self;
    //设置瀑布流布局
    WaterFlowLayout *layout = [WaterFlowLayout new];
    layout.columnCount = 2;
    layout.sectionInset = UIEdgeInsetsZero;
    layout.rowMargin = 9;
    layout.columnMargin = 9;
    layout.sectionInset = UIEdgeInsetsMake(0, 9, 0, 9);
    layout.delegate = self;
    [self.collectionView setCollectionViewLayout:layout];

    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];

    // 设置header
    self.collectionView.mj_header = header;
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.hidden = YES;

        @weakify(self);
    self.collectionView.mj_footer = [MJRefreshBackGifFooter footerWithRefreshingBlock:^{
        @strongify(self);
        if ([self.position isEqualToString:@"left"]) {
             [self moreData];
        }else{
            [self moreEndData];
        }
    }];
}

-(void)loadNewData
{
    if ([self.position isEqualToString:@"left"]) {
        [self refreshData];
    }else{
        [self refreshEndData];
    }
}

-(void)refreshData
{
    self.viewModel.type=self.type;
    self.viewModel.page=1;
    RACSignal * signal = self.viewModel.homeDataCommand;
    [signal subscribeNext:^(HomeAPI* api) {
        if ([api isSuccess]) {
            self.articleList = [[NSMutableArray alloc] initWithArray:[api.responseObject arrayForKey:@"data"]];
            [self setModelListCommand];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)moreData
{
    self.viewModel.type=self.type;
    [self.viewModel addPage];
    RACSignal * signal = self.viewModel.homeDataCommand;
    [signal subscribeNext:^(HomeAPI* api) {
        if ([api isSuccess]) {
            NSMutableArray * moreList =  [[NSMutableArray alloc] initWithArray:[api.responseObject arrayForKey:@"data"]];
            if (moreList.count>0) {
                [self.articleList addObjectsFromArray:moreList];
                [self setModelListCommand];
            }
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
        [self.collectionView.mj_footer endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}


-(void)refreshEndData
{
    [MobClick event:@"0008"];
    self.viewModel.page=1;
    RACSignal * signal = self.viewModel.endDataCommand;
    [signal subscribeNext:^(HomeAPI* api) {
        if ([api isSuccess]) {
            self.articleList = [[NSMutableArray alloc] initWithArray:[api.responseObject arrayForKey:@"data"]];
            [self setModelListCommand];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
        [self.collectionView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_header endRefreshing];
    }];
}

-(void)moreEndData
{
    [self.viewModel addPage];
    NSLog(@"%ld",(long)self.viewModel.page);
    RACSignal * signal = self.viewModel.endDataCommand;
    [signal subscribeNext:^(HomeAPI* api) {
        if ([api isSuccess]) {
            NSMutableArray * moreList =  [[NSMutableArray alloc] initWithArray:[api.responseObject arrayForKey:@"data"]];
            if (moreList.count>0) {
                [self.articleList addObjectsFromArray:moreList];
                [self setModelListCommand];
            }
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [api message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
        [self.collectionView.mj_footer endRefreshing];
    } error:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
    }];
}

-(void)setModelListCommand
{
    self.modeList =[NSMutableArray array];
    if (self.articleList.count>0) {
        for (int i = 0; i<self.articleList.count; i++) {
            HomeArticleModel* model =[[HomeArticleModel alloc] initWithDictionary:[self.articleList objectAtIndex:i] error:nil];
            [self.modeList addObject:model];
        }
        [self.collectionView reloadData];
    }
}


#pragma mark ————— layout 代理 —————
-(CGFloat)waterFlowLayout:(WaterFlowLayout *)WaterFlowLayout heightForWidth:(CGFloat)width andIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==1) {
        return 261;
    }else{
        return 315;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.modeList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
    if (indexPath.row==1) {
        cell.coverHeight.constant = 174;
    }else{
         cell.coverHeight.constant = 228;
    }
     cell.model = [self.modeList objectAtIndex:indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeArticleModel * model = [[HomeArticleModel alloc] init];
    model = [self.modeList objectAtIndex:indexPath.row];
    DetailMangager* vc = [[DetailMangager alloc] init];
    if ([model.groupStatus boolValue] ) {
        vc.type = @"2";
    }else{
        vc.type = @"1";
    }
     vc.shareId = model.shareId;
     vc.goodsId = model.goodsId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)publishCommand:(id)sender
{
    if ([[kDefaultUserData stringForKey:@"isPublish"] boolValue]) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        PublishVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"PublishVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"发布权限陆续开放中"];
        [hud hideAnimated:YES afterDelay:1.f];
    }
}




@end

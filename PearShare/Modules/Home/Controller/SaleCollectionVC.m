//
//  SaleCollectionVC.m
//  PearShare
//
//  Created by Edison on 2018/7/31.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SaleCollectionVC.h"
#import "SaleListAPI.h"
#import "WSLWaterFlowLayout.h"
#import "HomeItem.h"
#import "GoodsDetail.h"
#import "ArticleDetail.h"
#import "HomeArticleModel.h"
#import "SaleTimeHeader.h"
#import "DetailMangager.h"
#import "PublishVC.h"
#define Cell_Spacing 9
@interface SaleCollectionVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,WSLWaterFlowLayoutDelegate>
{
    WSLWaterFlowLayout * _flow;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)NSMutableArray * modeList;
@end

@implementation SaleCollectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modeList = [[NSMutableArray alloc] init];
    [self setUpCollectionView];
    [self.collectionView.mj_header beginRefreshing];
}


-(void)setUpCollectionView
{
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"SaleTimeHeader" bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SaleTimeHeader"];
    //设置瀑布流布局
    _flow = [[WSLWaterFlowLayout alloc] init];
    _flow.delegate = self;
    _flow.flowLayoutStyle = (WSLFlowLayoutStyle)0;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView setCollectionViewLayout:_flow];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    // 设置header
    self.collectionView.mj_header = header;
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    header.stateLabel.hidden = YES;
}

-(void)refreshData
{
    SaleListAPI *req = [SaleListAPI new];
    req.type = self.type;
    
    [req startWithCompletionBlockWithSuccess:^(SaleListAPI * request) {
        if ([request isSuccess]) {
            [self.modeList removeAllObjects];
            NSMutableArray * listOne = [[NSMutableArray alloc] initWithArray:[[request.result dictionaryForKey:@"data"] arrayForKey:@"listOne"]];
            NSMutableArray * listTwo = [[NSMutableArray alloc] initWithArray:[[request.result dictionaryForKey:@"data"] arrayForKey:@"listTwo"]];
            NSMutableArray * listThree = [[NSMutableArray alloc] initWithArray:[[request.result dictionaryForKey:@"data"] arrayForKey:@"listThree"]];
            [self setUpModelList:listOne];
            [self setUpModelList:listTwo];
            [self setUpModelList:listThree];
            [self.collectionView reloadData];
        }
        [self.collectionView.mj_header endRefreshing];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}

-(void)setUpModelList:(NSMutableArray*)list
{
    if (list.count>0) {
        NSMutableArray * modelList = [NSMutableArray array];
        for (int i = 0; i<list.count; i++) {
            HomeArticleModel* model =[[HomeArticleModel alloc] initWithDictionary:[list dictionaryWithIndex:i] error:nil];
            [modelList addObject:model];
        }
        [self.modeList addObject:modelList];
    }
}

#pragma mark - WSLWaterFlowLayoutDelegate
    //返回每个item大小
- (CGSize)waterFlowLayout:(WSLWaterFlowLayout *)waterFlowLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==1) {
        return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2, 261);
    }else{
        return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2, 315);
    }
}

/** 头视图Size */
-(CGSize )waterFlowLayout:(WSLWaterFlowLayout *)waterFlowLayout sizeForHeaderViewInSection:(NSInteger)section{
    return CGSizeMake(40, 30);
}

/** 边缘之间的间距*/
-(UIEdgeInsets)edgeInsetInWaterFlowLayout:(WSLWaterFlowLayout *)waterFlowLayout{
    return UIEdgeInsetsMake(0, Cell_Spacing, 0, Cell_Spacing);
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.modeList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.modeList arrayWithIndex:section].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
    if (indexPath.row==1) {
        cell.coverHeight.constant = 174;
    }else{
        cell.coverHeight.constant = 228;
    }
    cell.model = [[self.modeList arrayWithIndex:indexPath.section]objectAtIndex:indexPath.row];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    SaleTimeHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SaleTimeHeader" forIndexPath:indexPath];
    if (indexPath.section==0) {
        headerView.lblTime.text = @"—  上午 10:00  —";
    }else if (indexPath.section==1){
        headerView.lblTime.text = @"—  下午 14:00  —";
    }else{
         headerView.lblTime.text = @"—  晚上 20:00  —";
    }
        return headerView;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeArticleModel * model = [[HomeArticleModel alloc] init];
    model = [[self.modeList arrayWithIndex:indexPath.section] objectAtIndex:indexPath.row];
    DetailMangager* vc = [[DetailMangager alloc] init];
    if ([model.groupStatus boolValue] ) {
        vc.type = @"2";
        
    }else{
        vc.type = @"1";
    }
    vc.shareId = model.shareId;
     vc.goodsId = model.goodsId;
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)publishCommand:(id)sender
{
    if ([[kDefaults stringForKey:@"isPublish"] boolValue]) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
        PublishVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"PublishVC"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"发布权限陆续开放中"];
        [hud hideAnimated:YES afterDelay:1.f];
    }
}
@end

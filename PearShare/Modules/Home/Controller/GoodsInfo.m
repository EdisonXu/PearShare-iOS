//
//  GoodsInfo.m
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsInfo.h"
#import "GoodsInfoTopCell.h"
#import "GoodsDiscountCell.h"
#import "GoodsInfoTagCell.h"
#import "CollocationCell.h"
#import "GoodsInfoAPI.h"
#import "CommentHeader.h"
#import "InfoImageCell.h"
#import "JPVPNetEasyTableViewCell.h"
#import "GoodsInfoVM.h"
#import "JPVideoPlayerKit.h"
#import <AlibcTradeBiz/AlibcTradeBiz.h>
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <AlibabaAuthSDK/albbsdk.h>
#import "PictureHeader.h"

#define kTabbarHeight (IS_iPhoneX ? (49.f+34.f) : 49.f)
#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
@interface GoodsInfo()<UITableViewDataSource,UITableViewDelegate,JPVideoPlayerDelegate, JPVPNetEasyTableViewCellDelegate>

@property (nonatomic,strong)GoodsInfoVM *viewModel;
@property (nonatomic, strong) JPVPNetEasyTableViewCell *playingCell;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) UIImage *placeholder;
@property (nonatomic,strong)NSDictionary* model;
@property (nonatomic, strong) NSMutableDictionary *heightDict;
@property (nonatomic,strong)NSMutableArray* imageList;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *ToolView;
@property (nonatomic,strong)PictureHeader* header;
@end
@implementation GoodsInfo

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    self.viewModel = [[GoodsInfoVM alloc] init];
    self.viewModel.controller =self;
    [self goodsInfoCommand];
    self.title = @"商品详情";
    

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.playingCell) {
        [self.playingCell.videoPlayView jp_stopPlay];
    }
}
#pragma mark_ 配置初始化页面
-(void)setUpView
{
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.ToolView];
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:GoodsInfoTopCell.nib forCellReuseIdentifier:@"GoodsInfoTopCell"];
    [self.tableView registerNib:GoodsDiscountCell.nib forCellReuseIdentifier:@"GoodsDiscountCell"];
    [self.tableView registerNib:GoodsInfoTagCell.nib forCellReuseIdentifier:@"GoodsInfoTagCell"];
    [self.tableView registerNib:CollocationCell.nib forCellReuseIdentifier:@"CollocationCell"];
    [self.tableView registerNib:InfoImageCell.nib forCellReuseIdentifier:@"InfoImageCell"];
    [self.tableView registerNib:JPVPNetEasyTableViewCell.nib forCellReuseIdentifier:@"JPVPNetEasyTableViewCell"];
}

-(void)goodsInfoCommand
{
    self.viewModel.goodsId = self.goodsId;
    RACSignal * signal = [self.viewModel goodsInfo];
    [signal subscribeNext:^(NSDictionary*data ) {
        self.tableView.delegate =self;
        self.tableView.dataSource =self;
        self.model = [NSDictionary dictionaryWithDictionary:data];
        self.imageList =  [[[data stringForKey:@"imgs"] componentsSeparatedByString:@","] mutableCopy];
        self.url = [data stringForKey:@"video"];
        self.placeholder = [self getScreenShotImageFromVideoURL:self.url];
         [self setUpPictureheader];
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0) {
        return 1;
    }else if (section==1){
        return [self.model arrayForKey:@"goodsExplains"].count?1:0;
    }else if (section==2){
         return [self.model arrayForKey:@"goodsOffers"].count;
    }else if (section==3){
         return   [self.model arrayForKey:@"goodsRelateds"].count;
    }else if (section==4){
           return [self.model stringForKey:@"video"].length?1:0;
    }else{
         return self.imageList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        GoodsInfoTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsInfoTopCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section==1){
        GoodsInfoTagCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsInfoTagCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section==2){
        GoodsDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsDiscountCell" forIndexPath:indexPath];
        cell.model = [[self.model arrayForKey:@"goodsOffers"] dictionaryWithIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==3){
        CollocationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CollocationCell" forIndexPath:indexPath];
        cell.model = [[self.model arrayForKey:@"goodsRelateds"] dictionaryWithIndex:indexPath.row];
        return cell;
    }else if (indexPath.section==4){
        NSString *reuseIdentifier = NSStringFromClass([JPVPNetEasyTableViewCell class]);
        JPVPNetEasyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
        [cell.videoPlayView setImage:self.placeholder];
        cell.delegate = self;
        cell.playButton.hidden = NO;
        return cell;
    }else{
        InfoImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoImageCell" forIndexPath:indexPath];
        [cell.imageInfo sd_setImageWithURL:[NSURL URLWithString: [_imageList stringWithIndex:indexPath.row]] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height) {
                CGFloat imageW = [UIScreen mainScreen].bounds.size.width;
                CGFloat ratio = image.size.height / image.size.width;
                CGFloat imageH = ratio * imageW;
                if (![[self.heightDict allKeys] containsObject:@(indexPath.row)]) {
                    [self.heightDict setObject:@(imageH) forKey:@(indexPath.row)];
                    [self.tableView beginUpdates];
                    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    [self.tableView endUpdates];
                }
            }
        }];
        return cell;
    }
}


#pragma mark - JPVPNetEasyTableViewCellDelegate

- (void)cellPlayButtonDidClick:(JPVPNetEasyTableViewCell *)cell {
    if (self.playingCell) {
        [self.playingCell.videoPlayView jp_stopPlay];
        self.playingCell.playButton.hidden = NO;
    }
    self.playingCell = cell;
    self.playingCell.playButton.hidden = YES;
    self.playingCell.videoPlayView.jp_videoPlayerDelegate = self;
    [self.playingCell.videoPlayView jp_playVideoWithURL:[NSURL URLWithString:self.url]
                                     bufferingIndicator:[JPVideoPlayerBufferingIndicator new]
                                            controlView:[[JPVideoPlayerControlView alloc] initWithControlBar:nil blurImage:nil]
                                           progressView:nil
                                          configuration:nil];
}

-(void)setUpPictureheader
{
    self.header = [[PictureHeader alloc]initWithmodel:[self setUpHeader:self.model]];
    self.tableView.tableHeaderView = self.header;
    __weak typeof(self) weakSelf = self;
    self.header.changeHeightBlock = ^(CGSize passedValue){
        weakSelf.header.frame = CGRectMake(0, 0, SCREEN_WIDTH, passedValue.height);
        weakSelf.tableView.tableHeaderView = weakSelf.header;
        [weakSelf viewIfLoaded];
    };
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0||indexPath.section==1||indexPath.section==2) {
        return UITableViewAutomaticDimension;
    }else if (indexPath.section==3){
        return 131;
    }else if (indexPath.section==4){
        return 213;
    }else{
      return [[self.heightDict objectForKey:@(indexPath.row)] floatValue];
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section==0||section==1||section==2||section==3) {
        return 8;
    }else{
        return 0.01;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return nil;
    }else if (section==1){
        return nil;
    }else if (section==2){
        if ([self.model arrayForKey:@"goodsExplains"].count) {
            CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
            header.lblTitle.text = @"优惠信息";
            header.lblTitle.textColor = [UIColor colorWithHexString:@"999999"];
            return header;
        }else{
            return nil;
        }
    }else if (section==3){
        if ([self.model arrayForKey:@"goodsRelateds"].count) {
            CommentHeader * header = [[NSBundle mainBundle] loadNibNamed: @"CommentHeader" owner:self options:nil].firstObject;
            header.lblTitle.text = @"相关搭配";
            header.lblTitle.textColor = [UIColor colorWithHexString:@"999999"];
            return header;
        }else{
            return nil;
        }
    }else{
        return nil;
    }
}

//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section==3) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
//        GoodsInfo *vc = [sb instantiateViewControllerWithIdentifier:@"GoodsInfo"];
//        vc.goodsId = [[[self.model arrayForKey:@"goodsRelateds"] dictionaryWithIndex:indexPath.row] stringForKey:@"relatedGoodsId"];
//        [self.navigationController pushViewController:vc animated:YES];
//    }
//}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0) {
        return 0.01;
    }else if (section==1){
        return 0.01;
    }else if (section==2){
        if ([self.model arrayForKey:@"goodsExplains"].count) {
            return 30;
        }else{
            return 0.01;
        }
    }else if (section==3){
        if ([self.model arrayForKey:@"goodsRelateds"].count) {
            return 30;
        }else{
            return 0.01;
        }
    }else{
        return 0.01;
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.playingCell) {
        return;
    }
    if (cell.hash == self.playingCell.hash) {
        [self.playingCell.videoPlayView jp_stopPlay];
        self.playingCell.playButton.hidden = NO;
        self.playingCell = nil;
    }
}


- (NSMutableDictionary *)heightDict {
    if (!_heightDict) {
        _heightDict = @{}.mutableCopy;
    }
    return _heightDict;
}

#pragma mark - getter / setter
- (UITableView *)tableView
{
    if (_tableView == nil) {
        CGRect frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-kTabbarHeight);
        _tableView = [[UITableView alloc] initWithFrame:frame
                                                  style:UITableViewStylePlain];
    }
    return _tableView;
}

- (UIView *)ToolView
{
    if (_ToolView == nil) {
        CGRect frame = CGRectMake(0, SCREEN_HEIGHT-kTabbarHeight, self.view.bounds.size.width, 49);
        _ToolView = [[UIView alloc]initWithFrame:frame];
        _ToolView.backgroundColor = [UIColor whiteColor];
        UIButton * btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 49)];
        [btn setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:54.0f/255.0f blue:54.0f/255.0f alpha:1.0f]];
        [btn setTitle:@"立即购买" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(buyCommand) forControlEvents:UIControlEventTouchUpInside];
        [_ToolView addSubview:btn];
    }
    return _ToolView;
}

-(void)buyCommand
{
    id<AlibcTradePage> page = [AlibcTradePageFactory page:[self.model stringForKey:@"url"]];
    //淘客信息
    AlibcTradeTaokeParams *taoKeParams=[[AlibcTradeTaokeParams alloc] init];
    taoKeParams.pid= @"mm_44889980_43066955_279234503";
    //打开方式
    AlibcTradeShowParams* showParam = [[AlibcTradeShowParams alloc] init];
    showParam.openType = AlibcOpenTypeNative;
    [[AlibcTradeSDK sharedInstance] .tradeService show:self.navigationController page:page showParams:showParam taoKeParams:taoKeParams trackParam:nil tradeProcessSuccessCallback:nil tradeProcessFailedCallback:nil];
}

#pragma mark - JPVideoPlayerDelegate

- (BOOL)shouldShowBlackBackgroundWhenPlaybackStart {
    return YES;
}

- (BOOL)shouldShowBlackBackgroundBeforePlaybackStart {
    return YES;
}

- (BOOL)shouldAutoHideControlContainerViewWhenUserTapping {
    return YES;
}

- (BOOL)shouldShowDefaultControlAndIndicatorViews {
    return NO;
}
@end

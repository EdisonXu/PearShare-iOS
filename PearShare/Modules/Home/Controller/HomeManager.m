//
//  HomeManager.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeManager.h"
#import "HomeCollectionVC.h"
#import "SearchView.h"
#import "MessageCenter.h"
#import "GoodsDetail.h"
#import "SearchHotAPI.h"
#import "SearchReultVC.h"
#import "SearchResultAPI.h"
#import "ClassifyManager.h"
#import "SaleTimeMangager.h"
#import "CXSearchController.h"

#define KDeviceSystemVersion [UIDevice currentDevice].systemVersion.doubleValue
@interface HomeManager ()
@property (nonatomic,strong)NSArray * itemList;
@property (nonatomic,strong)NSMutableArray * hotList;
@property (nonatomic,strong)SearchView * searchView;
@end

@implementation HomeManager

-(instancetype)init
{
    if (self = [super init]) {
        self.menuViewStyle = WMMenuViewStyleLine;
        self.showOnNavigationBar = NO;
        self.titleColorSelected = [UIColor colorWithHexString:@"333333"];
        self.titleColorNormal = [UIColor colorWithHexString:@"828282"];
        self.progressColor = [UIColor colorWithHexString:@"fed000"];
        self.titleFontName = @"Courier-Bold";
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
        self.progressHeight = 2;
        self.progressWidth = 40;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpNavBar];
    
    [MobClick event:@"0001"];

    
    @weakify(self);
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName: Qnotifiction_PushSucess object:nil] subscribeNext:^(NSNotification * x) {
        @strongify(self);
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"您的内容发送成功啦～"];
        [hud hideAnimated:YES afterDelay:1.f];
    }];
    
    
    if(KDeviceSystemVersion>=11.0){
        
                [self.navigationController.navigationBar setShadowImage:[UIImage new]];
        
            }else{
            
                    NSArray *list=self.navigationController.navigationBar.subviews;
            
                    for (id obj in list) {
                
                            UIImageView *imageView=(UIImageView *)obj;
                
                            NSArray *list2=imageView.subviews;
                
                            for (id obj2 in list2) {
                    
                                    UIImageView *imageView2=(UIImageView *)obj2;
                    
                                    if (imageView2.frame.size.height < 1.0) {
                        
                                            imageView2.hidden=YES;
                        
                                        }
                    
                                }
                
                        }
            
                }
    
}



- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.searchView.size =CGSizeMake(SCREEN_WIDTH*0.8, 30);
}

-(SearchView*)searchView
{
    if (!_searchView) {
        _searchView=  [[[NSBundle mainBundle]loadNibNamed:@"SearchView" owner:self options:nil]objectAtIndex:0];
    }
    return _searchView;
}
-(NSArray*)itemList
{
    if (!_itemList) {
        _itemList = [[NSArray alloc] initWithObjects:@"全部",@"在团", nil];
    }
    return _itemList;
}

-(void)setUpNavBar
{
    UIButton *btnMessage = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [btnMessage setImage:[UIImage imageNamed:@"home_message"] forState:UIControlStateNormal];
    [btnMessage addTarget:self action:@selector(mesageCommand) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightCunstomButtonView = [[UIBarButtonItem alloc] initWithCustomView:btnMessage];
    self.navigationItem.rightBarButtonItem = rightCunstomButtonView;
    
    self.navigationItem.titleView = nil;
    self.searchView = [[SearchView alloc]initWithFrame:CGRectZero];
     self.searchView.intrinsicContentSize = CGSizeMake(SCREEN_WIDTH*0.8, 30);//iOS11 更新重要！！！
    [self.searchView.btnSearch addTarget:self action:@selector(goSearch) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView=self.searchView;
    
    
}

-(void)mesageCommand
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
    MessageCenter *vc = [sb instantiateViewControllerWithIdentifier:@"MessageCenter"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)goSearch
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"HomeStoryBoard" bundle:nil];
    CXSearchController *vc = [sb instantiateViewControllerWithIdentifier:@"CXSearchController"];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark-dataSouce
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.itemList.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return [self.itemList stringWithIndex:index];
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index==0) {
       ClassifyManager* vc = [[ClassifyManager alloc] init];
         return vc;
    }else{
        SaleTimeMangager* vc = [[SaleTimeMangager alloc] init];
        [MobClick event:@"0005"];
         return vc;
    }
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width ;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGFloat originY = self.showOnNavigationBar ? 0 : CGRectGetMaxY(self.navigationController.navigationBar.frame);
    return CGRectMake(leftMargin, originY, self.view.frame.size.width - 2*leftMargin, 30);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}
@end

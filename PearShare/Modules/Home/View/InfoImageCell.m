//
//  InfoImageCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "InfoImageCell.h"

@implementation InfoImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setImage:(NSString *)image
{
    self.imageInfo.contentMode = UIViewContentModeScaleAspectFit;
    [self.imageInfo sd_setImageWithURL:[NSURL URLWithString:[image stringByAppendingString:@"/480"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        _imageHeight.constant = image.size.height;
    }];
}

@end

//
//  GoodsInfoTopCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "UICustomLineLabel.h"
@interface GoodsInfoTopCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SDCycleScrollView *adView;
@property (weak, nonatomic) IBOutlet UILabel *lblTItle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceOrgin;
@property (nonatomic,strong)NSDictionary* model;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblPriceOld;
@end

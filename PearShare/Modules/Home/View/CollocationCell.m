//
//  CollocationCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "CollocationCell.h"

@implementation CollocationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(NSDictionary *)model
{
    NSMutableArray * imageList =[[[model stringForKey:@"coverRelated"] componentsSeparatedByString:@","] mutableCopy];
    if (imageList.count) {
        NSString *url = [imageList stringWithIndex:0];
        [self.coverImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    }
    self.lblTitle.text = [model stringForKey:@"titleRelated"];
    
    NSString * price = [NSString stringWithFormat:@"%@",[model stringForKey:@"activityAmtRelated"]];
    float floatString = [price floatValue];
    floatString = floatString/100;
    self.lblNew.text =[NSString stringWithFormat:@"%@%@",@"¥ ",[NSString stringWithFormat:@"%@",[self formatFloat:floatString]]];
    
    NSString * priceold = [NSString stringWithFormat:@"%@",[model stringForKey:@"originalAmtRelated"]];
    float floatStringold = [priceold floatValue];
    floatStringold = floatStringold/100;
    NSString* odl =[NSString stringWithFormat:@"%@%@",@"¥ ",[NSString stringWithFormat:@"%@",[self formatFloat:floatStringold]]];
    self.lblOld.text = odl;
    self.lblOld.lineType = LineTypeMiddle;
    
    
    NSString *str=[model stringForKey:@"beginTimeRelated"];//时间戳
    NSTimeInterval time=[str doubleValue]+28800;//因为时差问题要加8小时 == 28800 sec
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    //实例化一个NSDateFormatter对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设定时间格式,这里可以设置成自己需要的格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    self.lblTime.text =[NSString stringWithFormat:@"%@%@",@"活动时间", [dateFormatter stringFromDate: detaildate]];
    
    
}

- (NSString *)formatFloat:(float)f
{
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}

@end

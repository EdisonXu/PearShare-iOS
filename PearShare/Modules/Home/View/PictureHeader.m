//
//  ViewController.m
//  PictureScrollDemo
//
//  Created by 吴展图 on 2017/3/20.
//  Copyright © 2017年 wuzhantu. All rights reserved.
//

#import "PictureHeader.h"
#import "ImageModel.h"
#import "UIView+KGViewExtend.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface PictureHeader ()<UIScrollViewDelegate>
{
    CGFloat _lastPosition;
    int _currentPage;
}

@property(nonatomic,strong)NSArray *imageNameArr;
@property(nonatomic,strong)NSArray *imageSizeArr;
@property(nonatomic,strong)NSMutableArray *imageModelArr;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIScrollView *imageScrollView;
@end

@implementation PictureHeader

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        _scrollView.bounces = NO;
    }
    return _scrollView;
}

- (UIScrollView *)imageScrollView {
    if (!_imageScrollView) {
        _imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0)];
        _imageScrollView.delegate = self;
        _imageScrollView.pagingEnabled = YES;
        _imageScrollView.bounces = NO;
        _imageScrollView.showsVerticalScrollIndicator = FALSE;
        _imageScrollView.showsHorizontalScrollIndicator = FALSE;
    }
    return _imageScrollView;
}


- (instancetype)initWithmodel:(NSMutableArray *)list
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.imageModelArr = list;
        [self initViews];
    }
    return self;
}

- (void)initViews{
    if (@available(iOS 11.0, *)) {
        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    [self addSubview:self.scrollView];
    [self addSubview:self.imageScrollView];
    
    CGFloat firstHeight = 0.0f;
    for (NSInteger i = 0; i < self.imageModelArr.count;i++) {
        ImageModel *model = [self.imageModelArr objectAtIndex:i];
        
        CGFloat width = SCREEN_WIDTH;
        CGFloat scale = [model.imgWidth floatValue] / width;
        CGFloat height = 0.0f;
        if (!([model.imgWidth integerValue] == 0)) {
            height =  [model.imgHeight floatValue] / scale;
        }else{
            height = width;
        }
        
        if (i == 0) {
            firstHeight = height;
        }
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH*i, 0, width/height*firstHeight, firstHeight)];
        imgView.clipsToBounds = YES;
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.backgroundColor = [UIColor lightGrayColor];
        imgView.tag = 100+i;
        [imgView sd_setImageWithURL:[NSURL URLWithString:model.imgName]];
        
        UILabel * page = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-65, height-40 , 50, 25)];
        NSLog(@"page%f",SCREEN_WIDTH*(i+1)-65);
        page.text =[NSString stringWithFormat:@"%ld%@%lu",i+1,@"/",(unsigned long)self.imageModelArr.count];
        page.textAlignment = NSTextAlignmentCenter;
        page.font = [UIFont systemFontOfSize:12];
        page.cornerRadius = 11;
        page.backgroundColor =[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.5f];
        page.textColor = [UIColor whiteColor];
        [imgView addSubview:page];
        [self.imageScrollView addSubview:imgView];
        firstHeight = height;
    }
    
    UIImageView *imgView = (UIImageView *)[self.imageScrollView viewWithTag:100];
    self.imageScrollView.contentSize = CGSizeMake(self.imageModelArr.count*SCREEN_WIDTH, imgView.height);
    self.imageScrollView.height = imgView.height;
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.imageScrollView.contentSize.height );
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, self.scrollView.contentSize.height);
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat currentPostion = offsetX;
    
    int page = offsetX / SCREEN_WIDTH;
    
    BOOL isleft;
    if (currentPostion > _lastPosition) {
        isleft = YES;
        if (page > 0 && offsetX - page*SCREEN_WIDTH < 0.01) {
            page = page-1;
        }
    }
    else
    {
        isleft = NO;
    }
    
    if (page < 0 || page >= self.imageNameArr.count - 1) {
        return;
    }
    
    UIImageView *firstImgView = (UIImageView *)[self.imageScrollView viewWithTag:100+page];
    UIImageView *secondImgView = (UIImageView *)[self.imageScrollView viewWithTag:100+page+1];
    ImageModel *firstModel = [self.imageModelArr objectAtIndex:page];
    ImageModel *secondModel = [self.imageModelArr objectAtIndex:page+1];
    CGFloat firstImgHeight = [self heightformodel:firstModel];
    CGFloat secondImgHeight = [self heightformodel:secondModel];
    
    CGFloat distanceY = isleft ? secondImgHeight-firstImgView.height : firstImgHeight-firstImgView.height;
    CGFloat leftDistanceX = (page+1)*SCREEN_WIDTH-_lastPosition;
    CGFloat rightDistanceX = SCREEN_WIDTH-leftDistanceX;
    CGFloat distanceX = isleft ? leftDistanceX : rightDistanceX;
    
    CGFloat movingDistance = 0.0;
    if (distanceX != 0 && fabs(_lastPosition-currentPostion) > 0) {
        movingDistance = distanceY/distanceX*(fabs(_lastPosition-currentPostion));
    }
    
    CGFloat firstScale = [firstModel.imgWidth floatValue] / [firstModel.imgHeight floatValue];
    CGFloat secondScale = [secondModel.imgWidth floatValue] / [secondModel.imgHeight floatValue];
    
    firstImgView.frame = CGRectMake((firstImgView.frame.origin.x-movingDistance*firstScale), 0, (firstImgView.height+movingDistance)*firstScale, firstImgView.height+movingDistance);
    secondImgView.frame = CGRectMake(SCREEN_WIDTH*(page+1), 0, firstImgView.height*secondScale, firstImgView.height);
    self.imageScrollView.contentSize = CGSizeMake(self.imageScrollView.contentSize.width, firstImgView.height);
    self.imageScrollView.height = firstImgView.height;
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.imageScrollView.contentSize.height );
    if (self.changeHeightBlock) {
        self.changeHeightBlock(self.scrollView.contentSize);
    }
    int newpage = offsetX / SCREEN_WIDTH;
    if (offsetX - newpage*SCREEN_WIDTH < 0.01) {
        _currentPage = newpage+1;
    }
    
    _lastPosition = currentPostion;
}

- (CGFloat)heightformodel:(ImageModel *)model{
    CGFloat width = SCREEN_WIDTH;
    CGFloat scale = [model.imgWidth floatValue] / width;
    CGFloat height =  [model.imgHeight floatValue] / scale;
    return height;
}
@end


//
//  HotShareCell.m
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HotShareCell.h"
#import "HomeItem.h"
#import "HomeArticleModel.h"
#import "UserDesireHeader.h"
#import "DetailMangager.h"
#define NAVBAR_COLORCHANGE_POINT (IMAGE_HEIGHT - NAV_HEIGHT*2)
#define IMAGE_HEIGHT 220
#define NAV_HEIGHT 64
#define Cell_Spacing 9

@interface HotShareCell()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSArray * _list;
}
@end
@implementation HotShareCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModelList:(NSArray *)modelList
{
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection                             = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:flowLayout];
        [self.collectionView registerNib:[UINib nibWithNibName:@"HomeItem" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"HomeItem"];   
    
    _list = [NSArray arrayWithArray:modelList];
    
    self.frame = [UIScreen mainScreen].bounds;
    
    [self setNeedsLayout];
    
    [self layoutIfNeeded];
    
    [self.collectionView reloadData];
    

    
    self.height.constant =self.collectionView.collectionViewLayout.collectionViewContentSize.height;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _list.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
        HomeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeItem" forIndexPath:indexPath];
        HomeArticleModel *model =[[HomeArticleModel alloc]init];
        model = [_list objectAtIndex:indexPath.row];
        cell.model =model;
        return cell;
}


#pragma mark <UICollectionViewDelegate>
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
        return CGSizeMake((SCREEN_WIDTH-3*Cell_Spacing)/2,322);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return Cell_Spacing;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(Cell_Spacing, Cell_Spacing, Cell_Spacing,Cell_Spacing);//分别为上、左、下、右
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeArticleModel * model = [[HomeArticleModel alloc] init];
    model = [_list objectAtIndex:indexPath.row];
    DetailMangager* vc = [[DetailMangager alloc] init];
    if ([model.groupStatus boolValue] ) {
        vc.type = @"2";
    }else{
        vc.type = @"1";
    }
    vc.shareId = model.shareId;
    vc.goodsId = model.goodsId;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}
@end

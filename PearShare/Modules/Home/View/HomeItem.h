//
//  HomeItem.h
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseCollectionCell.h"
#import "HomeArticleModel.h"
#import "SPScrollNumLabel.h"
@interface HomeItem : BaseCollectionCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverHeight;
@property (nonatomic,strong)HomeArticleModel*model;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblShowAnimation;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet SPScrollNumLabel *lblLike;
@property (weak, nonatomic) IBOutlet UIImageView *imageUser;
@end

//
//  GoodsInfoTagCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsInfoTagCell.h"

@implementation GoodsInfoTagCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(NSDictionary *)model
{
//    goodsExplains
    [self.tagView removeAllTags];
    
    self.tagView.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width;
    self.tagView.padding = UIEdgeInsetsMake(8, 8, 8, 8);
    self.tagView.lineSpacing = 8;
    self.tagView.interitemSpacing = 8;
    self.tagView.singleLine = NO;
    
    
    NSMutableArray * tags = [[NSMutableArray arrayWithArray:[model arrayForKey:@"goodsExplains"]] mutableCopy];
    
    if (tags.count>0) {
        NSMutableArray * tagList = [NSMutableArray array];
        [tags enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [tagList addObject:[obj stringForKey:@"explainDesc"]];
        }];
        
        [tagList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            SKTag *tag = [[SKTag alloc] initWithText:tagList[idx]];
            
            tag.font = [UIFont systemFontOfSize:12];
            tag.textColor = [UIColor colorWithHexString:@"A9A9A9"];
            tag.bgColor =[UIColor whiteColor];
            tag.borderWidth = 1;
            tag.borderColor = [UIColor clearColor];
            tag.enable = NO;
            tag.padding = UIEdgeInsetsMake(5, 10, 5, 10);
            [self.tagView addTag:tag];
        }];
    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

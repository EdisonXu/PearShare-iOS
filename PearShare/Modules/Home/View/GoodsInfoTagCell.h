//
//  GoodsInfoTagCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKTagView.h"
@interface GoodsInfoTagCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SKTagView *tagView;
@property (nonatomic,strong)NSDictionary* model;
@end

//
//  GoodsTopCell.h
//  PearShare
//
//  Created by Edison on 2018/8/6.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface GoodsTopCell : BaseTableViewCell
@property (nonatomic,strong)NSDictionary* data;
@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblHour;
@property (weak, nonatomic) IBOutlet UILabel *lblMin;
@property (weak, nonatomic) IBOutlet UILabel *lblsecond;
@end

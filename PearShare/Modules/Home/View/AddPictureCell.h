//
//  AddPictureCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Demo9Model.h"

@interface AddPictureCell : UITableViewCell
@property (nonatomic,assign)NSInteger maxNumber;
@property (strong, nonatomic) Demo9Model *model;
@property (copy, nonatomic) void (^photoViewChangeHeightBlock)(UITableViewCell *myCell);

typedef void (^AddToCartsBlock) (AddPictureCell *);
@property(nonatomic, copy) AddToCartsBlock addToCartsBlock;
@end

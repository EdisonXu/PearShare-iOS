//
//  GoodsDiscountCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsDiscountCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDiscount;
@property (nonatomic,strong)NSDictionary* model;
@end

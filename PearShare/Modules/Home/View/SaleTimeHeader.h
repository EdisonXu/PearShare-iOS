//
//  SaleTimeHeader.h
//  PearShare
//
//  Created by Edison on 2018/8/1.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleTimeHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end

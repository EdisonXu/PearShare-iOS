//
//  ArticleTopCell.h
//  PearShare
//
//  Created by Destiny on 2018/6/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
#import "SKTagView.h"
#import "LEECoolButton.h"
#import "SPScrollNumLabel.h"
#import "OtherUserCenter.h"
#import "ZanLittleButton.h"
@interface ArticleTopCell : UITableViewCell
typedef void (^AddToCartsBlock) (ArticleTopCell *);

@property(nonatomic, copy) AddToCartsBlock addToCartsBlock;
@property (weak, nonatomic) IBOutlet UILabel *lblLike;
@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet SKTagView *tagView;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet ZanLittleButton *btnlike;
@property (nonatomic,strong)NSDictionary* model;
@property (weak, nonatomic) IBOutlet UIButton *btnWantCount;
@property (weak, nonatomic) IBOutlet UIButton *btnTuan;
@end

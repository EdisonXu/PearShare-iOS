//
//  HomeItem.m
//  PearShare
//
//  Created by Destiny on 2018/5/28.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeItem.h"
#import "LEECoolButton.h"
#import "FavoriteAPI.h"
#import "OtherUserCenter.h"
#import "LoginVC.h"
@interface HomeItem ()
@property (weak, nonatomic) IBOutlet LEECoolButton *btnLike;
@property (nonatomic,strong)HomeArticleModel * itemModel;
@property (nonatomic,strong)NSArray * favList;
@property (nonatomic ,assign) BOOL  isCollection;
@end
@implementation HomeItem

-(void)awakeFromNib
{
    [super awakeFromNib];
    _btnLike.imageColorOn = [UIColor colorWithHexString:@"FF4A4A"];
    _btnLike.imageColorOff =[UIColor whiteColor];
    self.btnLike.circleColor = [UIColor colorWithHexString:@"FF4A4A"];
    self.btnLike.lineColor = [UIColor colorWithHexString:@"FF4A4A"];
    self.lblShowAnimation.alpha = 0;
}


-(void)setModel:(HomeArticleModel *)model
{
    self.favList = [NSArray arrayWithObject:model.shareId];
    
    self.itemModel = [[HomeArticleModel alloc] init];
    self.itemModel = model;
    
    [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/800",model.cover]]];
    [self.imageUser sd_setImageWithURL:[NSURL URLWithString:model.avatar]];
    self.lblPrice.text =[NSString stringWithFormat:@"¥ %@",[self formatFloat:model.goodsAmt]];
    self.lblLike.text =  model.collectionsNum;
    NSString * title = [NSString stringWithFormat:@"%@%@",@" ",model.title];
    
    if (![model.groupStatus boolValue]) {
        self.lblTitle.text = model.title;
        self.lblPriceTitle.hidden = YES;
        self.lblPrice.hidden = YES;
    }else{
        self.lblTitle.attributedText = [self AttributedString:title content:@""];
        self.lblPriceTitle.hidden = NO;
        self.lblPrice.hidden = NO;
    }

     [model.isCollections isEqualToString:@"1"]?[self.btnLike select]:[self.btnLike deselect];
     self.isCollection = [_itemModel.isCollections boolValue];
}

- (IBAction)userCommand:(id)sender
{
        if (kDefaultLoginAlready) {
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
            OtherUserCenter *vc = [storyboard instantiateViewControllerWithIdentifier:@"OtherUserCenter"];
            vc.userId = self.itemModel.userId;
            [self.viewController.navigationController pushViewController:vc animated:YES ];
        }else{
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
            LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
            navi.navigationBarHidden=YES;
            [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
        }
}


- (IBAction)FavCommand:(UIButton*)sender
{
    if (kDefaultLoginAlready) {
        sender.enabled = NO;
        FavoriteAPI *req = [FavoriteAPI new];
        req.collectionType =   [NSString stringWithFormat:@"%d",self.isCollection];
        req.articleIdList =[_favList componentsJoinedByString:@","];
        [req startWithCompletionBlockWithSuccess:^(FavoriteAPI* request) {
             sender.enabled = YES;
            if (self.isCollection) {
                [self.btnLike deselect];
                [self.lblLike decreaseNumber:1];
                self.isCollection  =NO;
            }else{
                [self animationShow];
                [self.btnLike select];
                [self.lblLike increaseNumber:1];
                self.isCollection  =YES;
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
             sender.enabled = YES;
        }];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
    }
}

-(void)animationShow
{
        CGRect frameold = self.lblShowAnimation.frame;
        self.lblShowAnimation.alpha=1;
        [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            // 执行动画
            CGRect frame = self.lblShowAnimation.frame;
            frame.origin.y -= 30;
            self.lblShowAnimation.frame = frame;
        } completion:^(BOOL finished) {
            // 动画完成做什么事情
            [UIView animateWithDuration:0 animations:^{
                 self.lblShowAnimation.frame = frameold;
                self.lblShowAnimation.alpha -=1;
            }];
        }];
}

@end

//
//  ArticleTopCell.m
//  PearShare
//
//  Created by Destiny on 2018/6/11.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ArticleTopCell.h"
#import "FoucsAPI.h"
#import "LoginVC.h"
#import "FavoriteAPI.h"
@interface ArticleTopCell()
@property (nonatomic,strong)NSString * otherId;
@property (nonatomic,strong)NSDictionary * itemModel;
@property (nonatomic,assign)BOOL  isFollow;
@end
@implementation ArticleTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnlike.imageColorOn = [UIColor colorWithHexString:@"FF4A4A"];
    _btnlike.imageColorOff =[UIColor colorWithHexString:@"CCCCCC"];
}

-(void)setModel:(NSDictionary *)model
{
    self.itemModel = [NSDictionary dictionaryWithDictionary:model];
    self.isFollow =  [[model stringForKey:@"isFollow"] boolValue];
    
    
    [model boolForKey:@"groupStatus"]?[self.btnTuan setTitle:@"  已开团" forState:UIControlStateNormal]:[self.btnTuan setTitle:@"  未开团" forState:UIControlStateNormal];
    
    
    [self.imageAvatar sd_setImageWithURL:[NSURL URLWithString:[model stringForKey:@"avatar"]] placeholderImage:[UIImage imageNamed:@"mall_empty"]];
    self.lblName.text = [model stringForKey:@"nickname"];
    self.lblTitle.text = [NSString stringWithFormat:@"%@",[model stringForKey:@"title"]];
    
    //Tag
    [self.tagView removeAllTags];
    self.tagView.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width;
    self.tagView.padding = UIEdgeInsetsMake(8, 8, 8, 8);
    self.tagView.lineSpacing = 8;
    self.tagView.interitemSpacing = 8;
    self.tagView.singleLine = NO;
    NSMutableArray * tags = [[NSMutableArray arrayWithArray:[model arrayForKey:@"shareTags"]] mutableCopy];
    if (tags.count>0) {
        NSMutableArray * tagList = [NSMutableArray array];
        [tags enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [tagList addObject:[obj stringForKey:@"tagsDesc"]];
        }];
        [tagList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            SKTag *tag = [[SKTag alloc] initWithText:tagList[idx]];
            tag.font = [UIFont systemFontOfSize:12];
            tag.textColor = [UIColor colorWithHexString:@"A9A9A9"];
            tag.bgColor =[UIColor whiteColor];
            tag.borderWidth = 1;
            tag.borderColor = [UIColor colorWithHexString:@"A9A9A9"];
            tag.cornerRadius = 11;
            tag.enable = YES;
            tag.padding = UIEdgeInsetsMake(5, 10, 5, 10);
            [self.tagView addTag:tag];
        }];
    }
    //描述
    
    self.lblText.text = [model stringForKey:@"contentTop"];
    NSString *str=[model stringForKey:@"createTime"];
    NSTimeInterval time=[str doubleValue];
    NSDate *detaildate=[NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM月dd日 HH:mm"];
    self.lblTime.text = [dateFormatter stringFromDate: detaildate];
    
}

- (IBAction)otherUser:(id)sender
{
    if (kDefaultLoginAlready) {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        OtherUserCenter *vc = [storyboard instantiateViewControllerWithIdentifier:@"OtherUserCenter"];
        vc.userId = self.otherId;
        [self.viewController.navigationController pushViewController:vc animated:YES ];
    }else{
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UserStoryboard" bundle:nil];
        LoginVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        UINavigationController * navi = [[UINavigationController alloc]initWithRootViewController:vc];
        navi.navigationBarHidden=YES;
        [self.viewController.navigationController presentViewController:navi animated:NO completion:nil];
    }
}

@end

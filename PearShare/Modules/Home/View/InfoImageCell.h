//
//  InfoImageCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoImageCell : UITableViewCell
@property (nonatomic,strong)NSString * image;
@property (weak, nonatomic) IBOutlet UIImageView *imageInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@end

//
//  CollocationCell.h
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICustomLineLabel.h"
@interface CollocationCell : UITableViewCell
@property (nonatomic,strong)NSDictionary* model;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UILabel *lblNew;
@property (weak, nonatomic) IBOutlet UICustomLineLabel *lblOld;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@end

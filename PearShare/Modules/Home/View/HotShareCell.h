//
//  HotShareCell.h
//  PearShare
//
//  Created by Destiny on 2018/6/29.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotShareCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong)NSArray* modelList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *height;
@end

//
//  PictureHeader.h
//  PearShare
//
//  Created by Destiny on 2018/7/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureHeader : UIView
typedef void (^ReturnChangeHeightBlock) (CGSize size);
/**
 *  声明一个ReturnValueBlock属性，这个Block是获取传值的界面传进来的
 */
@property(nonatomic, copy) ReturnChangeHeightBlock changeHeightBlock;

- (instancetype)initWithmodel:(NSMutableArray *)list;
@end

//
//  GoodsInfoTopCell.m
//  PearShare
//
//  Created by Destiny on 2018/7/2.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsInfoTopCell.h"

@implementation GoodsInfoTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(NSDictionary *)model
{
    //轮播
    NSMutableArray * imageList =[[[model stringForKey:@"cover"] componentsSeparatedByString:@","] mutableCopy];
    self.adView.placeholderImage = [UIImage imageNamed:@"mall_empty"];
    self.adView .imageURLStringsGroup = imageList;
    NSMutableArray * labelList = [NSMutableArray array];
    if (imageList.count>0) {
        for (int i = 0; i<imageList.count; i++) {
            NSString* son = [NSString stringWithFormat:@"%d",i+1];
            NSString * number = [NSString stringWithFormat:@"%@%@%@",son,@"/",[NSString stringWithFormat:@"%lu",(unsigned long)imageList.count]];
            [labelList addObject:number];
        }
    }
    self.adView.titlesGroup = labelList;
    self.adView.currentPageDotColor = [UIColor clearColor];
    self.adView.showPageControl = NO;
    self.lblTItle.text = [model stringForKey:@"title"];
    self.lblSubTitle.text = [model stringForKey:@"subTitle"];
    
    NSString * price = [NSString stringWithFormat:@"%@",[model stringForKey:@"activityAmt"]];
    float floatString = [price floatValue];
    floatString = floatString/100;
    self.lblPriceOrgin.text =[NSString stringWithFormat:@"%@%@",@"¥ ",[NSString stringWithFormat:@"%@",[self formatFloat:floatString]]];
    
    NSString * priceold = [NSString stringWithFormat:@"%@",[model stringForKey:@"originalAmt"]];
    float floatStringold = [priceold floatValue];
    floatStringold = floatStringold/100;
    NSString* odl =[NSString stringWithFormat:@"%@%@",@"¥ ",[NSString stringWithFormat:@"%@",[self formatFloat:floatStringold]]];
    self.lblPriceOld.text = odl;
     self.lblPriceOld.lineType = LineTypeMiddle;
    
}

- (NSString *)formatFloat:(float)f
{
    if (fmodf(f, 1)==0) {//如果有一位小数点
        return [NSString stringWithFormat:@"%.0f",f];
    } else if (fmodf(f*10, 1)==0) {//如果有两位小数点
        return [NSString stringWithFormat:@"%.1f",f];
    } else {
        return [NSString stringWithFormat:@"%.2f",f];
    }
}
@end

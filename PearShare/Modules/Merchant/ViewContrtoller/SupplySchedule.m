//
//  SupplySchedule.m
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SupplySchedule.h"
#import "SupplyScheduleVM.h"
#import "FailureAudit.h"
#import "ScheduleAPI.h"
#import "DemoRegisterVC.h"
#import "SaleApply.h"
@interface SupplySchedule ()
@property (nonatomic,strong) SupplyScheduleVM * viewModel;
@property (weak, nonatomic) IBOutlet UIButton *btn0;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;

@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;

@property (nonatomic,strong)NSDictionary* data;

@end

@implementation SupplySchedule

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"查看进度";
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setUpSchedule];
}

-(void)setUpSchedule
{
    self.viewModel = [[SupplyScheduleVM alloc]init];
    self.viewModel.applyId = self.applyId;
    RACSignal * signal = self.viewModel.scheduleCommand;
    [signal subscribeNext:^(ScheduleAPI* api) {
        [self setUpWith:[api.responseObject dictionaryForKey:@"data"]];
        self.data = [[NSDictionary alloc] initWithDictionary:[api.responseObject dictionaryForKey:@"data"]];
    } error:^(NSError *error) {
        
    }];
}



-(void)showEorr:(NSString*)Message
{
    NSString * title =[NSString stringWithFormat:@"审核失败原因"];
    NSString * message =[NSString stringWithFormat:@"%@",Message];
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * signAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertController addAction:signAction];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)setUpWith:(NSDictionary*)status
{
    NSInteger process = [status integerForKey:@"process"];
    NSInteger statusId = [status integerForKey:@"status"];
    
    if (statusId==2) {
        [self showEorr:[status stringForKey:@"failReason"]];
    }
    

    if (process==128) {
        if (statusId==0) {
        }else if (statusId==1){
            [self.btn0 setSelected:YES];
            [self.btn1 setTitle:@"  点击提交快递信息" forState:UIControlStateNormal];
            [self.btn1 setTitleColor:[UIColor colorWithHexString:@"007AFF"] forState:UIControlStateNormal];
        }else{
            [self.btn0 setEnabled:NO];
        }
    }else if (process==129){
        if (statusId==0) {
            [self.btn0 setSelected:YES];
            [self.btn1 setTitle:@"  样品邮寄中 请耐心等待" forState:UIControlStateNormal];
        }else if (statusId==1){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setTitle:@"  等待样品审核 请耐心等待" forState:UIControlStateNormal];
        }else{
            [self.btn0 setSelected:YES];
            [self.btn1 setEnabled:NO];
        }
    }else if (process==130){
        if (statusId==0) {

        }else if (statusId==1){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setTitle:@"  点击提交开团信息" forState:UIControlStateNormal];
            [self.btn3 setTitleColor:[UIColor colorWithHexString:@"007AFF"] forState:UIControlStateNormal];
        }else{
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setEnabled:NO];
        }
    }else if (process==131){
        if (statusId==0) {
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setTitle:@"  等待保证金审核 请耐心等待" forState:UIControlStateNormal];
        }else if (statusId==1){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setSelected:YES];
        }else{
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setEnabled:NO];
        }
    }else if (process==132){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setSelected:YES];
            [self.btn4 setSelected:YES];
    }else if (process==133){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setSelected:YES];
            [self.btn4 setSelected:YES];
            [self.btn5 setSelected:YES];
    }else if (process==134){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setSelected:YES];
            [self.btn4 setSelected:YES];
            [self.btn5 setSelected:YES];
            [self.btn6 setSelected:YES];
    }else if (process==135){
            [self.btn0 setSelected:YES];
            [self.btn1 setSelected:YES];
            [self.btn2 setSelected:YES];
            [self.btn3 setSelected:YES];
            [self.btn4 setSelected:YES];
            [self.btn5 setSelected:YES];
            [self.btn6 setSelected:YES];
            [self.btn7 setSelected:YES];
    }
}

- (IBAction)expressCommand:(id)sender
{
    if ([[self.data stringForKey:@"process"] isEqualToString:@"128"]) {
        if ([[self.data stringForKey:@"status"] isEqualToString:@"1"]) {
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
            DemoRegisterVC *vc = [storyboard instantiateViewControllerWithIdentifier:@"DemoRegisterVC"];
            vc.applyId =self.applyId;
            [self.navigationController pushViewController:vc animated:YES ];
        }
    }
}

- (IBAction)beginCommand:(id)sender
{
    if ([[self.data stringForKey:@"process"] isEqualToString:@"130"]) {
        if ([[self.data stringForKey:@"status"] isEqualToString:@"1"]) {
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
            SaleApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"SaleApply"];
            vc.applyId = self.applyId;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}


@end

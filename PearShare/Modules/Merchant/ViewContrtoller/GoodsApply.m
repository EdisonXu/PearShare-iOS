//
//  GoodsApply.m
//  PearShare
//
//  Created by Destiny on 2018/6/21.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsApply.h"
#import "LEEAlert.h"
#import "SelectedListView.h"
#import "PPNetworkHelper.h"
#import "BEMCheckBox.h"
#import "UITextView+ZWPlaceHolder.h"
#import "HXPhotoPicker.h"
#import "TestCell.h"
@interface GoodsApply ()<UITextFieldDelegate,BEMCheckBoxDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *textCategory;
@property (weak, nonatomic) IBOutlet UITextField *textExpressType;
@property (weak, nonatomic) IBOutlet UITextField *textChannel;
@property (weak, nonatomic) IBOutlet UITextField *textGoodsName;
@property (weak, nonatomic) IBOutlet UITextField *textGoodsUrl;
@property (weak, nonatomic) IBOutlet UITextField *textOriginalPrice;
@property (weak, nonatomic) IBOutlet UITextField *textDiscountPrice;
@property (weak, nonatomic) IBOutlet UITextField *textGoodsNumber;
@property (weak, nonatomic) IBOutlet UITextField *textExpressPrice;
@property (weak, nonatomic) IBOutlet UITextField *textExpressTip;
@property (weak, nonatomic) IBOutlet UITextField *textCommission;
@property (weak, nonatomic) IBOutlet UITextField *textCommissionTip;
@property (weak, nonatomic) IBOutlet UITextField *textReplenishment;
@property (weak, nonatomic) IBOutlet UITextField *textTryNumber;
@property (weak, nonatomic) IBOutlet UITextField *textDeliver;

@property (weak, nonatomic) IBOutlet BEMCheckBox *boxLeft;
@property (weak, nonatomic) IBOutlet BEMCheckBox *boxRight;

@property (weak, nonatomic) IBOutlet UITextView *textRetrun;
@property (weak, nonatomic) IBOutlet UITextView *textReappearance;

@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableArray *uploadArray;
@property (nonatomic, strong) NSMutableArray *waitArray;
@property (nonatomic, strong) NSMutableArray *completeArray;
@property (strong, nonatomic) HXDatePhotoToolManager *toolManager;


@property (nonatomic,assign)BOOL expressHide;
@property (nonatomic,strong)BEMCheckBoxGroup* group;

@property (nonatomic,strong)Demo9Model* detailModel;
@property (nonatomic,strong)Demo9Model* coverModel;
@property (nonatomic,strong)Demo9Model* qualityModel;
@property (nonatomic,strong)Demo9Model* contrastModel;

@property (nonatomic, strong) NSMutableArray *photoList;

@property (nonatomic,strong)NSMutableDictionary *param;
@end

@implementation GoodsApply

- (void)viewDidLoad {
    [super viewDidLoad];
    self.param = [[NSMutableDictionary alloc] init];
    self.title = @"供货申请";
    self.photoList =[NSMutableArray array];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.textCategory.delegate =self;
    self.textExpressType.delegate =self;
    self.textChannel.delegate =self;
    
    [self setUpTextView];
    [self setUpBox];
    
    self.detailModel = [[Demo9Model alloc] init];
    self.coverModel = [[Demo9Model alloc] init];
    self.qualityModel = [[Demo9Model alloc] init];
    self.contrastModel = [[Demo9Model alloc] init];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        if ([self.backType isEqualToString:@"sucess"]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    [super viewWillDisappear:animated];
}


-(void)setUpTextView
{
    self.textReappearance.delegate = self;
    self.textRetrun.delegate = self;
    self.textRetrun.zw_placeHolder = @"最多可输入100字";
    self.textReappearance.zw_placeHolder = @"最多可输入100字";
}

- (void)textViewDidChange:(UITextView *)textView{
    
    if (textView.markedTextRange == nil && textView.text.length > 100) {
        //提示语
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"输入限制100个字符以内"];
        [hud hideAnimated:YES afterDelay:1.f];
        //截取
        textView.text = [textView.text substringToIndex:100];
    }
}


-(void)setUpBox
{
    self.boxLeft.delegate = self;
    self.boxRight.delegate = self;
    self.boxLeft.onAnimationType = BEMAnimationTypeFill;
    self.boxRight.onAnimationType = BEMAnimationTypeFill;
    
    self.group = [BEMCheckBoxGroup groupWithCheckBoxes:@[self.boxLeft, self.boxRight]];
    self.group.selectedCheckBox = self.boxRight; // 可选择设置哪个复选框被预选
    self.group.mustHaveSelection = YES;
    
    //    BEMCheckBox *selection = self.group.selectedCheckBox;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.textCategory) {
        [self setUpPamam:@"4"];
    }
    
    if (textField==self.textExpressType) {
        [self setUpPamam:@"115"];
    }
    
    if (textField==self.textChannel) {
        [self setUpPamam:@"121"];
    }
}

-(void)setUpPamam:(NSString*)type
{
    [PPNetworkHelper GET:[[URL_main stringByAppendingString:@"/system/dics/"]stringByAppendingString:type] parameters:nil success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
            [self categoryToast:type list:[responseObject arrayForKey:@"data"]];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)categoryToast:(NSString*)type list:(NSArray*)list
{
    SelectedListView *view = [[SelectedListView alloc] initWithFrame:CGRectMake(0, 0, 280, 0) style:UITableViewStylePlain];
    view.isSingle = YES;
    
    NSMutableArray* arry = [NSMutableArray array];
    [list enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arry addObject:[[SelectedListModel alloc] initWithSid:[obj integerForKey:@"id"] Title:[obj stringForKey:@"name"]]];
    }];
    
    view.array =  arry;
    view.selectedBlock = ^(NSArray<SelectedListModel *> *array) {
        [LEEAlert closeWithCompletionBlock:^{
            if ([type isEqualToString:@"4"]) {
                self.textCategory.text =  [NSString stringWithFormat:@"%@", [array objectAtIndex:0].title];
                [self.param setString:[NSString stringWithFormat:@"%ld", (long)[array objectAtIndex:0].sid] forKey:@"classify"];
            }
            if ([type isEqualToString:@"115"]) {
                self.textExpressType.text =  [NSString stringWithFormat:@"%@", [array objectAtIndex:0].title];
                self.expressHide = [self.textExpressType.text isEqualToString:@"包邮"]?YES:NO;
                [self.param setString:[NSString stringWithFormat:@"%ld", (long)[array objectAtIndex:0].sid] forKey:@"expressType"];
                [self.tableView reloadData];
            }
            
            if ([type isEqualToString:@"121"]) {
                self.textChannel.text =  [NSString stringWithFormat:@"%@", [array objectAtIndex:0].title];
                [self.param setString:[NSString stringWithFormat:@"%ld", (long)[array objectAtIndex:0].sid] forKey:@"sourceChannel"];
            }
        }];
    };
    
    [LEEAlert alert].config
    .LeeTitle(@"请选择")
    .LeeItemInsets(UIEdgeInsetsMake(20, 0, 20, 0))
    .LeeCustomView(view)
    .LeeItemInsets(UIEdgeInsetsMake(0, 0, 0, 0))
    .LeeHeaderInsets(UIEdgeInsetsMake(10, 0, 0, 0))
    .LeeClickBackgroundClose(YES)
    .LeeShow();
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1){//爱好 （动态cell）
        return 1;
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 15;
        cell.model = self.detailModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self setUpPhotoList];
            [self.tableView reloadData];
        }];
        
        cell.addToCartsBlock = ^(TestCell *cell) {
           [self setUpPhotoList];
        };
        return cell;
    }else if(indexPath.section==3){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 3;
        cell.model = self.coverModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self.tableView reloadData];
        }];
        return cell;
    }else if(indexPath.section==5){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 1;
        cell.model = self.qualityModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self.tableView reloadData];
        }];
        return cell;
    }else if(indexPath.section==7){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 3;
        cell.model = self.contrastModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self.tableView reloadData];
        }];
        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row ==3 ||indexPath.row==5) {
            return UITableViewAutomaticDimension;
        }
    }else if (indexPath.section==1){
        Demo9Model *model = self.detailModel;
        return model.cellHeight+16;
    }else if (indexPath.section==2){
        if (indexPath.row==2) {
            return UITableViewAutomaticDimension;
        }
    }else if (indexPath.section==3){
        Demo9Model *model = self.coverModel;
        return model.cellHeight+16;
    }else if (indexPath.section==4){
        if (indexPath.row==13) {
            return UITableViewAutomaticDimension;
        }
        if (indexPath.row==3) {
            return self.expressHide?0.1:44;
        }
        
    }else if (indexPath.section==5){
        Demo9Model *model = self.qualityModel;
        return model.cellHeight+16;
    }else if (indexPath.section==6){
        if (indexPath.row==0) {
            return UITableViewAutomaticDimension;
        }
    }else if (indexPath.section==7){
        Demo9Model *model = self.contrastModel;
        return model.cellHeight+16;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

//cell的缩进级别，动态静态cell必须重写，否则会造成崩溃
- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1||indexPath.section==3||indexPath.section==5||indexPath.section==7){
        return [super tableView:tableView indentationLevelForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:1]];
    }
    return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (IBAction)applyCommand:(id)sender
{
        [self.param setString:self.textGoodsName.text forKey:@"goodsName"];
        [self.param setString:self.textGoodsUrl.text forKey:@"goodsUrl"];
        [self.param setString:self.textOriginalPrice.text forKey:@"originalAmt"];
    
        [self.param setString:self.textDiscountPrice.text forKey:@"activityAmt"];
        BEMCheckBox *selection = self.group.selectedCheckBox;
        if (selection==self.boxLeft) {
            [self.param setString:@"0" forKey:@"scheduling"];
        }else{
            [self.param setString:@"1" forKey:@"scheduling"];
        }
    
        [self.param setString:self.textGoodsNumber.text forKey:@"number"];
        self.expressHide? [self.param setString:@"0" forKey:@"expressAmt"]:[self.param setString:self.textExpressPrice.text forKey:@"expressAmt"];
        [self.param setString:self.textExpressTip.text forKey:@"expressComm"];
        [self.param setString:self.textDeliver.text forKey:@"deliverCycle"];
    
        [self.param setString:self.textCommission.text forKey:@"singelCommision"];
        [self.param setString:self.textCommissionTip.text forKey:@"commisionComm"];
        [self.param setString:self.textReplenishment.text forKey:@"replenishmentCycle"];
        [self.param setString:self.textRetrun.text forKey:@"comm"];
        [self.param setString:self.textReappearance.text forKey:@"amonutBackComm"];
        [self.param setString:self.textTryNumber.text forKey:@"trialNum"];
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/batchuploadFile"]parameters:nil name:@"files" images:self.photoList fileNames:nil imageScale:0.8 imageType:nil progress:^(NSProgress *progress) {
        NSLog(@"%@",progress);
    } success:^(NSDictionary* responseObject) {
        NSLog(@"%@",responseObject);
        [hud hideAnimated:YES];
        [self.param setString:[[[responseObject dictionaryForKey:@"data"] arrayForKey:@"urls"] componentsJoinedByString:@","] forKey:@"goodsImgs" ];
        [self.param setString:[[[responseObject dictionaryForKey:@"data"] arrayForKey:@"urls"] componentsJoinedByString:@","] forKey:@"cover" ];
        [self push];
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"图片上传失败，请稍后重试"];
        [hud hideAnimated:YES afterDelay:1.f];
    }];

}

-(void)push
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.param options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[URL_main stringByAppendingString:@"/user/applyGoods"] parameters:nil error:nil];
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
        if ([[responseObject stringForKey:@"success"] boolValue]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"提交成功，请等待审核"];
            [hud hideAnimated:YES afterDelay:1.f];
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            [self.navigationController popViewControllerAnimated:YES];
                        });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"%@",[responseObject stringForKey:@"errMsg"]];
            [hud hideAnimated:YES afterDelay:1.f];
        }
    }] resume];
}


-(void)setUpPhotoList
{
    if (_detailModel.endSelectedList.count) {
        [self.photoList removeAllObjects];
        NSMutableArray * list =[NSMutableArray array];
        __weak typeof(self) weakSelf = self;
        [_detailModel.endSelectedList enumerateObjectsUsingBlock:^(HXPhotoModel* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [list addObject:obj.asset];
        }];
        
        [list enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [HXPhotoTools getHighQualityFormatPhoto:obj size:CGSizeMake(750, 750) succeed:^(UIImage *image) {
                [weakSelf.photoList addObject:image];
            } failed:^{
                
            }];
        }];
    }else{
        [self.photoList removeAllObjects];
    }
}

@end

//
//  DemoRegisterVC.m
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "DemoRegisterVC.h"
#import "ExpressAPI.h"
@interface DemoRegisterVC ()

@end

@implementation DemoRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"样品登记";
}


- (IBAction)sureCommand:(id)sender
{
    ExpressAPI *req = [ExpressAPI new];
    req.applyId = self.applyId;
    req.name = self.textName.text;
    req.expressNo = self.textOrder.text;
    req.num = self.textNumber.text;
    
    [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        if ([req isSuccess]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"提交成功！"];
            [hud hideAnimated:YES afterDelay:2.f];
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [req message];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
    
}
@end

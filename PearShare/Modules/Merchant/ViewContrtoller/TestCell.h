//
//  TestCell.h
//  PearShare
//
//  Created by Destiny on 2018/6/21.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Demo9Model.h"
@interface TestCell : UITableViewCell
@property (strong, nonatomic) Demo9Model *model;
@property (nonatomic,assign)NSInteger maxNumber;
@property (copy, nonatomic) void (^photoViewChangeHeightBlock)(UITableViewCell *myCell);

typedef void (^AddToCartsBlock) (TestCell *);
@property(nonatomic, copy) AddToCartsBlock addToCartsBlock;
@end

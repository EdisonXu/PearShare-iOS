//
//  MySupplyVC.m
//  PearShare
//
//  Created by Destiny on 2018/5/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MySupplyVC.h"
#import "MySupplyCell.h"
#import "MySupplyVM.h"
#import "EmptyCell.h"
#import "SupplyListAPI.h"
#import "LEEAlert.h"
#import "SelectedListView.h"
#import "GoodsApply.h"
@interface MySupplyVC ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)MySupplyVM * viewModel;
@property (nonatomic,strong)NSMutableArray * supplyList;
@property (nonatomic,strong)NSString * screenType;

@end

@implementation MySupplyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的供应";
    
    self.viewModel = [[MySupplyVM alloc] init];
    self.viewModel.type = @"1";
    [self setUpTableCommand];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     [self.tableView.mj_header beginRefreshing];
}

-(void)setUpTableCommand
{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    [self.tableView registerNib:MySupplyCell.nib forCellReuseIdentifier:@"MySupplyCell"];
    [self.tableView registerNib:EmptyCell.nib forCellReuseIdentifier:@"EmptyCell"];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self setUpMySupply];
    }];
}

-(void)setUpMySupply
{
    self.viewModel.pageNum = [NSString stringWithFormat:@"1"];
    self.viewModel.pageSize = [NSString stringWithFormat:@"20"];
    RACSignal * signal = self.viewModel.mySupplyListCommand;
    [signal subscribeNext:^(SupplyListAPI* api) {
        self.supplyList = [[NSMutableArray alloc] initWithArray:[api.responseObject arrayForKey:@"data"]];
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    } error:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.supplyList.count>0) {
        return self.supplyList.count;
    }else{
        return 1;
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.supplyList.count>0) {
        MySupplyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"MySupplyCell" forIndexPath:indexPath];
        cell.supplyData = [self.supplyList dictionaryWithIndex:indexPath.row];
        return cell;
    }else{
        EmptyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.supplyList.count>0) {
        return 115;
    }else{
        return self.tableView.frame.size.height;
    }
}
- (IBAction)screenCommand:(id)sender
{
    SelectedListView *view = [[SelectedListView alloc] initWithFrame:CGRectMake(0, 0, 280, 0) style:UITableViewStylePlain];
    
    view.isSingle = YES;
    
    view.array = @[ [[SelectedListModel alloc] initWithSid:1 Title:@"全部"],
                                [[SelectedListModel alloc] initWithSid:128 Title:@"初审中"] ,
                                [[SelectedListModel alloc] initWithSid:129 Title:@"邮寄中"] ,
                                [[SelectedListModel alloc] initWithSid:0 Title:@"未通过"] ,
                                [[SelectedListModel alloc] initWithSid:132 Title:@"排期中"] ,
                                [[SelectedListModel alloc] initWithSid:133 Title:@"开团中"],
                                [[SelectedListModel alloc] initWithSid:134 Title:@"已截团"]
                   ];
    
    view.selectedBlock = ^(NSArray<SelectedListModel *> *array) {
        
        [LEEAlert closeWithCompletionBlock:^{
            NSString * type = [NSString stringWithFormat:@"%ld",(long)[array objectAtIndex:0].sid];
            self.viewModel.type = type;
            [self.tableView.mj_header beginRefreshing];
        }];
        
    };
    
    [LEEAlert alert].config
    .LeeTitle(@"分类查看")
    .LeeItemInsets(UIEdgeInsetsMake(20, 0, 20, 0))
    .LeeCustomView(view)
    .LeeItemInsets(UIEdgeInsetsMake(0, 0, 0, 0))
    .LeeHeaderInsets(UIEdgeInsetsMake(10, 0, 0, 0))
    .LeeClickBackgroundClose(YES)
    .LeeShow();
}
- (IBAction)supplyCommand:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
    GoodsApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoodsApply"];
    [self.navigationController pushViewController:vc animated:YES];
}
@end

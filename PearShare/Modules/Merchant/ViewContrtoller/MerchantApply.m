//
//  MerchantApply.m
//  PearShare
//
//  Created by Destiny on 2018/6/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MerchantApply.h"
#import "HXPhotoView.h"
#import "HXPhotoPicker.h"
#import "MerchantApplyAPI.h"
#import "PPNetworkHelper.h"
#import "GoodsApply.h"
@interface MerchantApply ()<HXPhotoViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTopTitle;
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textPhone;
@property (weak, nonatomic) IBOutlet UITextField *textTenxun;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;
@property (weak, nonatomic) IBOutlet UITextField *textWang;
@property (weak, nonatomic) IBOutlet UITextField *textWeiChat;
@property (weak, nonatomic) IBOutlet UITextField *textWeibo;
@property (weak, nonatomic) IBOutlet UITextField *textShopName;
@property (weak, nonatomic) IBOutlet UITextField *textShopQuality;
@property (weak, nonatomic) IBOutlet UITextField *textShopLink;
@property (weak, nonatomic) IBOutlet UITextField *textShopID;
@property (weak, nonatomic) IBOutlet UITextField *textBoss;
@property (weak, nonatomic) IBOutlet UITextField *textBossID;
@property (weak, nonatomic) IBOutlet UITextField *textAdress;

@property (weak, nonatomic) IBOutlet UIScrollView *licenseScollView;
@property (nonatomic, strong) HXPhotoManager *licenseManager;
@property (strong, nonatomic) HXPhotoView *licensePhotoView;

@property (weak, nonatomic) IBOutlet UIScrollView *idOneScollView;
@property (nonatomic, strong) HXPhotoManager *idOneManager;
@property (strong, nonatomic) HXPhotoView *idOnePhotoView;

@property (weak, nonatomic) IBOutlet UIScrollView *idTwoScollView;
@property (nonatomic, strong) HXPhotoManager *idTwoManager;
@property (strong, nonatomic) HXPhotoView *idTwoPhotoView;

@property (weak, nonatomic) IBOutlet UIScrollView *permitScollView;
@property (nonatomic, strong) HXPhotoManager *permitManager;
@property (strong, nonatomic) HXPhotoView *permitPhotoView;

@property (nonatomic,strong)NSMutableDictionary *param;
@end

@implementation MerchantApply

static const CGFloat kPhotoViewMargin = 12.0;

- (HXPhotoManager *)licenseManager {
    if (!_licenseManager) {
        _licenseManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _licenseManager.configuration.openCamera = YES;
        _licenseManager.configuration.singleJumpEdit = YES;
        _licenseManager.configuration.photoMaxNum = 1;
        _licenseManager.configuration.maxNum = 1;
    }
    return _licenseManager;
}

- (HXPhotoManager *)idOneManager {
    if (!_idOneManager) {
        _idOneManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _idOneManager.configuration.openCamera = YES;
        _idOneManager.configuration.photoMaxNum = 1;
        _idOneManager.configuration.maxNum = 1;
    }
    return _idOneManager;
}

- (HXPhotoManager *)idTwoManager {
    if (!_idTwoManager) {
        _idTwoManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _idTwoManager.configuration.openCamera = YES;
        _idTwoManager.configuration.photoMaxNum = 1;
        _idTwoManager.configuration.maxNum = 1;
    }
    return _idTwoManager;
}

- (HXPhotoManager *)permitManager {
    if (!_permitManager) {
        _permitManager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _permitManager.configuration.openCamera = YES;
        _permitManager.configuration.photoMaxNum = 1;
        _permitManager.configuration.maxNum = 1;
    }
    return _permitManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    [self setUpPhoto];
    
    self.param =[ NSMutableDictionary dictionary];
    NSLog(@"%@",kDefaultUserToken);
    NSLog(@"%@",kDefaultUserId);
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    [super viewWillDisappear:animated];
}

-(void)setUpPhoto
{
    CGFloat width = self.licenseScollView.frame.size.width;
    
    self.licensePhotoView = [[HXPhotoView alloc] initWithFrame:CGRectMake(kPhotoViewMargin, kPhotoViewMargin, width - kPhotoViewMargin * 2, 0) WithManager:self.licenseManager];
    self.licensePhotoView.outerCamera = YES;
    self.licensePhotoView.delegate = self;
    [self.licenseScollView addSubview:self.licensePhotoView];
    
    self.idOnePhotoView = [[HXPhotoView alloc] initWithFrame:CGRectMake(kPhotoViewMargin, kPhotoViewMargin, width - kPhotoViewMargin * 2, 0) WithManager:self.idOneManager];
    self.idOnePhotoView.outerCamera = YES;
    self.idOnePhotoView.delegate = self;
    [self.idOneScollView addSubview:self.idOnePhotoView];

    self.idTwoPhotoView = [[HXPhotoView alloc] initWithFrame:CGRectMake(kPhotoViewMargin, kPhotoViewMargin, width - kPhotoViewMargin * 2, 0) WithManager:self.idTwoManager];
    self.idTwoPhotoView.outerCamera = YES;
    self.idTwoPhotoView.delegate = self;
    [self.idTwoScollView addSubview:self.idTwoPhotoView];

    self.permitPhotoView = [[HXPhotoView alloc] initWithFrame:CGRectMake(kPhotoViewMargin, kPhotoViewMargin, width - kPhotoViewMargin * 2, 0) WithManager:self.permitManager];
    self.permitPhotoView.outerCamera = YES;
    self.permitPhotoView.delegate = self;
    [self.permitScollView addSubview:self.permitPhotoView];
}

-(void)setUpView
{
    self.title = @"商户注册";
    NSString *content = @"个人资料 （注：*号为必填类容）";
    NSArray *number = @[@"*"];
    NSMutableAttributedString *attributeString  = [[NSMutableAttributedString alloc]initWithString:content];
    for (int i = 0; i < content.length; i ++) {
        NSString *a = [content substringWithRange:NSMakeRange(i, 1)];
        if ([number containsObject:a]) {
            [attributeString setAttributes:@{NSForegroundColorAttributeName:[UIColor redColor],NSFontAttributeName:[UIFont systemFontOfSize:12]} range:NSMakeRange(i, 1)];
        }
    }
    self.lblTopTitle.attributedText = attributeString;
    
    [self.textPhone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.textPhone) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal
{
    NSMutableArray * list = [NSMutableArray array];
    if (allList.count>0) {
            [list addObject:[allList firstObject].thumbPhoto];
             [self updateFile:list photo:photoView];
    }
}

- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
//    self.licenseScollView.contentSize = photoView.frame.size;
}


- (IBAction)applyCommand:(id)sender
{
    [self.param setString:self.textName.text forKey:@"trueName"];
    [self.param setString:self.textPhone.text forKey:@"phone"];
    [self.param setString:self.textTenxun.text forKey:@"qq"];
    [self.param setString:self.textEmail.text forKey:@"email"];
    [self.param setString:self.textWang.text forKey:@"tbAccount"];
    [self.param setString:self.textWeiChat.text forKey:@"wxAccount"];
    [self.param setString:self.textWeibo.text forKey:@"wbAccount"];
    [self.param setString:self.textShopName.text forKey:@"shopName"];
    [self.param setString:self.textShopQuality.text forKey:@"shopCredibility"];
    [self.param setString:self.textShopLink.text forKey:@"shopUrl"];
    [self.param setString:self.textAdress.text forKey:@"returnAddress"];
    [self.param setString:self.textShopID.text forKey:@"enterpriseId"];
    [self.param setString:self.textBoss.text forKey:@"legalName"];
    [self.param setString:self.textBossID.text forKey:@"legalID"];
    [self  postJsonToServerwith:self.param];
}



- (void)postJsonToServerwith:(NSDictionary*)param{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    hud.mode = MBProgressHUDModeText;
//    hud.label.text =[NSString stringWithFormat:@"提交成功！"];
//    [hud hideAnimated:YES afterDelay:1.f];
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
//        GoodsApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoodsApply"];
//        [self.navigationController pushViewController:vc animated:YES];
//    });
//
//
//
    

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[URL_main stringByAppendingString:@"/user/applyMerchant"] parameters:nil error:nil];
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
    [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
         [hud hideAnimated:YES];
        if (!error) {
            if ([responseObject boolForKey:@"success"]) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text =[NSString stringWithFormat:@"提交成功！"];
                [hud hideAnimated:YES afterDelay:1.f];
                            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
                            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
                                GoodsApply *vc = [storyboard instantiateViewControllerWithIdentifier:@"GoodsApply"];
                                vc.backType = [NSString stringWithFormat:@"sucess"];
                                [self.navigationController pushViewController:vc animated:YES];
                            });
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text =[NSString stringWithFormat:@"%@",[responseObject stringForKey:@"errMsg"]];
                [hud hideAnimated:YES afterDelay:2.f];
            }
        } else {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text =[NSString stringWithFormat:@"请求失败，请稍后重试"];
            [hud hideAnimated:YES afterDelay:2.f];
        }
    }] resume];
}

-(void)updateFile:(NSArray*)images photo:(HXPhotoView*)photo
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/uploadFile"]parameters:nil name:@"file" images:images fileNames:nil imageScale:1 imageType:nil progress:^(NSProgress *progress) {
        NSLog(@"%@",progress);
    } success:^(NSDictionary* responseObject) {
        [hud hideAnimated:YES];
        if (photo==self.licensePhotoView) {
            [self.param setString:[[[responseObject dictionaryForKey:@"data"]arrayForKey:@"urls"]stringWithIndex:0] forKey:@"license"];
        }else if (photo==self.idOnePhotoView){
            [self.param setString:[[[responseObject dictionaryForKey:@"data"]arrayForKey:@"urls"]stringWithIndex:0] forKey:@"positive"];
        }else if (photo==self.idTwoPhotoView){
            [self.param setString:[[[responseObject dictionaryForKey:@"data"]arrayForKey:@"urls"]stringWithIndex:0] forKey:@"opposite"];
        }else if (photo==self.permitPhotoView){
            [self.param setString:[[[responseObject dictionaryForKey:@"data"]arrayForKey:@"urls"]stringWithIndex:0] forKey:@"charter"];
        }
        
    } failure:^(NSError *error) {
         [hud hideAnimated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"图片上传失败，请稍后重试"];
        [hud hideAnimated:YES afterDelay:2.f];
    }];
}
@end

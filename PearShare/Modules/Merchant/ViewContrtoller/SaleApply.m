//
//  SaleApply.m
//  PearShare
//
//  Created by Destiny on 2018/7/18.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SaleApply.h"
#import "Demo9Model.h"
#import "HXPhotoPicker.h"
#import "TestCell.h"
#import "BirthdayView.h"
#import "SelectedListView.h"
#import "LEEAlert.h"
#import "PPNetworkHelper.h"

#define kWeakSelf(A)          __weak  typeof(self) A = self;
#define kStrongSelf(A,B)     __strong typeof(self) A = B;
@interface SaleApply ()<UITextFieldDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textUrl;
@property (weak, nonatomic) IBOutlet UITextField *textyuanjia;
@property (weak, nonatomic) IBOutlet UITextField *textyongjinng;
@property (weak, nonatomic) IBOutlet UITextField *textfanxian;
@property (weak, nonatomic) IBOutlet UITextField *texthong;
@property (weak, nonatomic) IBOutlet UITextField *textbaozhengjin;
@property (weak, nonatomic) IBOutlet UITextView *textjiesao;
@property (weak, nonatomic) IBOutlet UITextField *textguige;
@property (weak, nonatomic) IBOutlet UITextField *textbuhuo;
@property (weak, nonatomic) IBOutlet UITextField *textnumber;
@property (weak, nonatomic) IBOutlet UITextView *textwenan;




@property (nonatomic,strong)Demo9Model* fristModel;
@property (nonatomic,strong)Demo9Model* scendModel;

@property (nonatomic, strong) LVDatePickerModel *birth;
@property (nonatomic, strong) NSMutableArray *fristPhotoList;
@property (nonatomic, strong) NSMutableArray *scendPhotoList;
@property (weak, nonatomic) IBOutlet UITextField *textBeginTime;
@property (weak, nonatomic) IBOutlet UITextField *textMoneyTime;
@property (weak, nonatomic) IBOutlet UITextField *textCergry;
@property (weak, nonatomic) IBOutlet UITextField *textPushTime;
@property (weak, nonatomic) IBOutlet UITextField *textWays;
@property (nonatomic,strong)NSMutableDictionary*param;
@end

@implementation SaleApply

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"开团资料";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.fristModel = [[Demo9Model alloc] init];
    self.scendModel = [[Demo9Model alloc] init];
    
    self.fristPhotoList =[NSMutableArray array];
    self.scendPhotoList =[NSMutableArray array];
    
    self.textBeginTime.delegate = self;
    self.textMoneyTime.delegate = self;
    self.textCergry.delegate = self;
    self.textPushTime.delegate = self;
    self.textWays.delegate = self;
    self.param = [NSMutableDictionary dictionary];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1){//爱好 （动态cell）
        return 1;
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 1;
        cell.model = self.fristModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self setUpFristPhotoList];
            [self.tableView reloadData];
        }];
        
        cell.addToCartsBlock = ^(TestCell *cell) {
            [self setUpFristPhotoList];
        };
        return cell;
    }else if(indexPath.section==3){
        TestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TestCell"];
        cell.maxNumber = 3;
        cell.model = self.scendModel;
        if(!cell){
            cell = [[TestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TestCell"];
        }
        [cell setPhotoViewChangeHeightBlock:^(UITableViewCell *mycell) {
            [self setUpscendPhotoList];
            [self.tableView reloadData];
        }];
        
        cell.addToCartsBlock = ^(TestCell *cell) {
            [self setUpscendPhotoList];
        };
        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        if (indexPath.row ==3) {
            return UITableViewAutomaticDimension;
        }
    }
    if (indexPath.section==4) {
        if (indexPath.row ==0 ||indexPath.row==7) {
            return UITableViewAutomaticDimension;
        }
    }
    
     if (indexPath.section==1){
        Demo9Model *model = self.fristModel;
        return model.cellHeight+16;
    }
    
      if (indexPath.section==3){
         Demo9Model *model = self.scendModel;
         return model.cellHeight+16;
     }

    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

//cell的缩进级别，动态静态cell必须重写，否则会造成崩溃
- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==1||indexPath.section==3){
        return [super tableView:tableView indentationLevelForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:1]];
    }
    return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField==self.textBeginTime) {
        BirthdayView *vc = [[BirthdayView alloc] init];
        [self.view addSubview:vc];
        vc.birthDay = self.birth;
        
        kWeakSelf(weakSelf)
        vc.timeBlock = ^(LVDatePickerModel *birth) {
            weakSelf.birth = birth;
//            NSString *timeSp = [NSString stringWithFormat:@"%d", (long)[datenow timeIntervalSince1970]];
                    weakSelf.textBeginTime.text = [NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day];
//            [self.param setString:[NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day] forKey:@"beginTime"];
        };
        [vc showBirthView];
    }
    
    if (textField==self.textMoneyTime) {
        BirthdayView *vc = [[BirthdayView alloc] init];
        [self.view addSubview:vc];
        vc.birthDay = self.birth;
        
        kWeakSelf(weakSelf)
        vc.timeBlock = ^(LVDatePickerModel *birth) {
            weakSelf.birth = birth;
            [self.param setString:self.textMoneyTime.text forKey:@"transferTime"];
                    weakSelf.textMoneyTime.text = [NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day];
//            [self.param setString:[NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day] forKey:@"beginTime"];
        };
        [vc showBirthView];
    }
    
    if (textField==self.textCergry) {
        [self setUpPamam:@"4"];
    }
    
    if (textField==self.textPushTime) {
        BirthdayView *vc = [[BirthdayView alloc] init];
        [self.view addSubview:vc];
        vc.birthDay = self.birth;
        
        kWeakSelf(weakSelf)
        vc.timeBlock = ^(LVDatePickerModel *birth) {
            weakSelf.birth = birth;
//            [self.param setString:self.textMoneyTime.text forKey:@"deliveryTime"];
                    weakSelf.textPushTime.text = [NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day];
//            [self.param setString:[NSString stringWithFormat:@"%@-%@-%@",birth.year,birth.month,birth.day] forKey:@"beginTime"];
        };
        [vc showBirthView];
        
    }
    
    if (textField==self.textWays) {
        
        
        NSMutableDictionary* dic1 = [NSMutableDictionary dictionary];
        [dic1 setString:@"0" forKey:@"id"];
        [dic1 setString:@"团期内拍下系统自动改价" forKey:@"name"];
        
        NSMutableDictionary* dic2 = [NSMutableDictionary dictionary];
        [dic2 setString:@"1" forKey:@"id"];
        [dic2 setString:@"拍下订单备注梨子联系客服改价" forKey:@"name"];
        
        NSMutableDictionary* dic3 = [NSMutableDictionary dictionary];
        [dic3 setString:@"2" forKey:@"id"];
        [dic3 setString:@"使用优惠券（仅限天猫店）" forKey:@"name"];
        
        NSMutableArray * list = [NSMutableArray array];
        [list addObject:dic1];
        [list addObject:dic2];
        [list addObject:dic3];
        
        [self categoryToast:@"2" list:list];
        
    }
}

-(void)setUpPamam:(NSString*)type
{
    [PPNetworkHelper GET:[[URL_main stringByAppendingString:@"/system/dics/"]stringByAppendingString:type] parameters:nil success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
            [self categoryToast:type list:[responseObject arrayForKey:@"data"]];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)categoryToast:(NSString*)type list:(NSArray*)list
{
    SelectedListView *view = [[SelectedListView alloc] initWithFrame:CGRectMake(0, 0, 280, 0) style:UITableViewStylePlain];
    view.isSingle = YES;
    
    NSMutableArray* arry = [NSMutableArray array];
    [list enumerateObjectsUsingBlock:^(NSDictionary* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [arry addObject:[[SelectedListModel alloc] initWithSid:[obj integerForKey:@"id"] Title:[obj stringForKey:@"name"]]];
    }];
    
    view.array =  arry;
    view.selectedBlock = ^(NSArray<SelectedListModel *> *array) {
        [LEEAlert closeWithCompletionBlock:^{
            if ([type isEqualToString:@"4"]) {
                self.textCergry.text =  [NSString stringWithFormat:@"%@", [array objectAtIndex:0].title];
                [self.param setString:[NSString stringWithFormat:@"%ld", (long)[array objectAtIndex:0].sid] forKey:@"type"];
            }
            if ([type isEqualToString:@"2"]) {
                self.textWays.text =  [NSString stringWithFormat:@"%@", [array objectAtIndex:0].title];
                [self.param setString:[NSString stringWithFormat:@"%ld", (long)[array objectAtIndex:0].sid] forKey:@"groupType"];
            }
        }];
    };
    
    [LEEAlert alert].config
    .LeeTitle(@"请选择")
    .LeeItemInsets(UIEdgeInsetsMake(20, 0, 20, 0))
    .LeeCustomView(view)
    .LeeItemInsets(UIEdgeInsetsMake(0, 0, 0, 0))
    .LeeHeaderInsets(UIEdgeInsetsMake(10, 0, 0, 0))
    .LeeClickBackgroundClose(YES)
    .LeeShow();
}

-(void)setUpFristPhotoList
{
    if (_fristModel.endSelectedList.count) {
        [self.fristPhotoList removeAllObjects];
        NSMutableArray * list =[NSMutableArray array];
        __weak typeof(self) weakSelf = self;
        [_fristModel.endSelectedList enumerateObjectsUsingBlock:^(HXPhotoModel* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [list addObject:obj.asset];
        }];
        
        [list enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [HXPhotoTools getHighQualityFormatPhoto:obj size:CGSizeMake(750, 750) succeed:^(UIImage *image) {
                [weakSelf.fristPhotoList addObject:image];
                
                [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/batchuploadFile"]parameters:nil name:@"files" images:self.fristPhotoList fileNames:nil imageScale:0.8 imageType:nil progress:^(NSProgress *progress) {
                    NSLog(@"%@",progress);
                } success:^(NSDictionary* responseObject) {
                    NSLog(@"%@",responseObject);
                    [self.param setString:[[[responseObject dictionaryForKey:@"data"] arrayForKey:@"urls"] componentsJoinedByString:@","] forKey:@"transferImg" ];
                } failure:^(NSError *error) {
 
                }];
            } failed:^{
                
            }];
        }];
    }else{
        [self.fristPhotoList removeAllObjects];
    }
}



-(void)setUpscendPhotoList
{
    if (_scendModel.endSelectedList.count) {
        [self.scendPhotoList removeAllObjects];
        NSMutableArray * list =[NSMutableArray array];
        __weak typeof(self) weakSelf = self;
        [_scendModel.endSelectedList enumerateObjectsUsingBlock:^(HXPhotoModel* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [list addObject:obj.asset];
        }];
        
        [list enumerateObjectsUsingBlock:^(PHAsset* obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [HXPhotoTools getHighQualityFormatPhoto:obj size:CGSizeMake(750, 750) succeed:^(UIImage *image) {
                [weakSelf.scendPhotoList addObject:image];
            } failed:^{
                
            }];
        }];
    }else{
        [self.scendPhotoList removeAllObjects];
    }
}
- (IBAction)push:(id)sender
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    [PPNetworkHelper uploadImagesWithURL:[URL_main stringByAppendingString:@"/storge/batchuploadFile"]parameters:nil name:@"files" images:self.scendPhotoList fileNames:nil imageScale:0.8 imageType:nil progress:^(NSProgress *progress) {
        NSLog(@"%@",progress);
    } success:^(NSDictionary* responseObject) {
        NSLog(@"%@",responseObject);
        [hud hideAnimated:YES];
        [self.param setString:[[[responseObject dictionaryForKey:@"data"] arrayForKey:@"urls"] componentsJoinedByString:@","] forKey:@"salesImg" ];
        
        [self.param setString:@"1531895493" forKey:@"beginTime"];
        [self.param setString:@"1531895493" forKey:@"transferTime"];
        [self.param setString:@"1531895493" forKey:@"deliveryTime"];
        [self.param setString:self.applyId forKey:@"applyId"];
        [self.param setString:self.textName.text forKey:@"name"];
        [self.param setString:self.textUrl.text forKey:@"url"];
        [self.param setString:self.textyuanjia.text forKey:@"originalAmt"];
        [self.param setString:self.textyongjinng.text forKey:@"commission"];
        [self.param setString:self.textfanxian.text forKey:@"cashBack"];
        [self.param setString:self.texthong.text forKey:@"trialNum"];
        [self.param setString:self.textbaozhengjin.text forKey:@"transferAmt"];
        [self.param setString:self.textjiesao.text forKey:@"introduce"];
        [self.param setString:self.textguige.text forKey:@"size"];
        [self.param setString:self.textbuhuo.text forKey:@"replenishmentCycle"];
        [self.param setString:self.textnumber.text forKey:@"spotNum"];
        [self.param setString:self.textwenan.text forKey:@"description"];
        
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.param options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[URL_main stringByAppendingString:@"/user/applyGroup"] parameters:nil error:nil];
        req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setValue:kDefaultUserToken forHTTPHeaderField:@"token"];
        [req setValue:kDefaultUserId forHTTPHeaderField:@"userId"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary* responseObject, NSError * _Nullable error) {
            if ([[responseObject stringForKey:@"success"] boolValue]) {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text =[NSString stringWithFormat:@"提交成功，请等待审核"];
                [hud hideAnimated:YES afterDelay:1.f];
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.label.text =[NSString stringWithFormat:@"%@",[responseObject stringForKey:@"errMsg"]];
                [hud hideAnimated:YES afterDelay:1.f];
            }
        }] resume];
        
    } failure:^(NSError *error) {
        [hud hideAnimated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.label.text =[NSString stringWithFormat:@"图片上传失败，请稍后重试"];
        [hud hideAnimated:YES afterDelay:1.f];
    }];
}
@end

//
//  DemoRegisterVC.h
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewController.h"

@interface DemoRegisterVC : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *textName;
@property (weak, nonatomic) IBOutlet UITextField *textOrder;
@property (weak, nonatomic) IBOutlet UITextField *textNumber;
@property (nonatomic,strong)NSString * applyId;
@end

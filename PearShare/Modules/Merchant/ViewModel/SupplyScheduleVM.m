//
//  SupplyScheduleVM.m
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SupplyScheduleVM.h"
#import "ScheduleAPI.h"
@implementation SupplyScheduleVM
-(RACSignal*)scheduleCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        ScheduleAPI *req = [ScheduleAPI new];
        req.applyId = self.applyId;
        
        [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendNext:req];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}
@end

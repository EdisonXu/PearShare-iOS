//
//  MerchantApplyAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/19.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MerchantApplyAPI.h"

@implementation MerchantApplyAPI
-(NSString *)requestUrl{
    return @"user/applyMerchant/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.textName forKey:@"trueName"];
    [param setString:self.textPhone forKey:@"phone"];
    [param setString:self.textTenxun forKey:@"qq"];
    [param setString:self.textEmail forKey:@"email"];
    [param setString:self.textWang forKey:@"tbAccount"];
    [param setString:self.textWeiChat forKey:@"wxAccount"];
    [param setString:self.textWeibo forKey:@"wbAccount"];
    [param setString:self.textShopName forKey:@"shopName"];
    [param setString:self.textShopQuality forKey:@"shopCredibility"];
    [param setString:self.textShopLink forKey:@"shopUrl"];
    [param setString:self.textBossID forKey:@"enterpriseId"];
    [param setString:self.textBoss forKey:@"legalName"];
    [param setString:self.textAdress forKey:@"returnAddress"];
    
    
    return [self convertToJsonData:param];
}

-(NSString *)convertToJsonData:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
    
}


@end

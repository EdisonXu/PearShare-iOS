//
//  ExpressAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface ExpressAPI : BaseRequestAPI
@property (nonatomic,strong)NSString *applyId;
@property (nonatomic,strong)NSString *name;
@property (nonatomic,strong)NSString *expressNo;
@property (nonatomic,strong)NSString *num;
@end

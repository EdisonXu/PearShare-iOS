//
//  SupplyScheduleVM.h
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"

@interface SupplyScheduleVM : BaseViewModel
@property (nonatomic,strong)NSString *applyId;

-(RACSignal*)scheduleCommand;
@end

//
//  ExpressAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ExpressAPI.h"

@implementation ExpressAPI
-(NSString *)requestUrl{
    return @"user/express/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.applyId forKey:@"applyId"];
    [param setString:self.name forKey:@"name"];
    [param setString:self.expressNo forKey:@"expressNo"];
    [param setString:self.num forKey:@"num"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

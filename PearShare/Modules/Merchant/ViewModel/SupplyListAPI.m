//
//  SupplyListAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "SupplyListAPI.h"

@implementation SupplyListAPI
-(NSString *)requestUrl{
    return @"user/applyList/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.type forKey:@"type"];
    [param setString:self.pageNum forKey:@"pageNum"];
    [param setString:self.pageSize forKey:@"pageSize"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

//
//  MerchantCellModel.h
//  PearShare
//
//  Created by Destiny on 2018/6/15.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    CreateShopCellType_TF = 0, // textfield
    CreateShopCellType_TV, // textView
    CreateShopCellType_PICK, // picker
    CreateShopCellType_PIC, // upload picture
} CreateShopCellType;

@interface MerchantCellModel : NSObject
@property (nonatomic, copy)NSString                 *title;  // 所要填写的项目名称
@property (nonatomic, copy)NSString                 *placeholder;
@property (nonatomic, copy)NSString                 *key; // 表单对应的字段
@property (nonatomic, copy)NSString                 *errText; // 校验出错时的提示信息
@property (nonatomic, strong)UIImage                    *image;  // 所选取的图片
@property (nonatomic, assign)CreateShopCellType      cellType; // cell的类型
@property (nonatomic, assign)NSInteger               maxInputLength; // 最大输入长度限制
@end

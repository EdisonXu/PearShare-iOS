//
//  MerchantApplyAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/19.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface MerchantApplyAPI : BaseRequestAPI

@property (nonatomic,strong)NSString* textName;
@property (nonatomic,strong)NSString* textPhone;
@property (nonatomic,strong)NSString* textTenxun;
@property (nonatomic,strong)NSString* textEmail;
@property (nonatomic,strong)NSString* textWang;
@property (nonatomic,strong)NSString* textWeiChat;
@property (nonatomic,strong)NSString* textWeibo;
@property (nonatomic,strong)NSString* textShopName;
@property (nonatomic,strong)NSString* textShopQuality;
@property (nonatomic,strong)NSString* textShopLink;
@property (nonatomic,strong)NSString* textShopID;
@property (nonatomic,strong)NSString* textBoss;
@property (nonatomic,strong)NSString* textBossID;
@property (nonatomic,strong)NSString* textAdress;
@end

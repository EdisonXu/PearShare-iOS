//
//  MySupplyVM.h
//  PearShare
//
//  Created by Destiny on 2018/5/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseViewModel.h"
#import "SupplyListAPI.h"
@interface MySupplyVM : BaseViewModel
@property (nonatomic,strong)NSString *type;
@property (nonatomic,strong)NSString *pageNum;
@property (nonatomic,strong)NSString *pageSize;

-(RACSignal*)mySupplyListCommand;

@end

//
//  MySupplyVM.m
//  PearShare
//
//  Created by Destiny on 2018/5/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MySupplyVM.h"

@implementation MySupplyVM

-(RACSignal*)mySupplyListCommand
{
    RACSignal * signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        SupplyListAPI *req = [SupplyListAPI new];
        req.type = self.type;
        req.pageSize =self.pageSize;
        req.pageNum =self.pageNum;
        
        [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendNext:req];
            [subscriber sendCompleted];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            [subscriber sendError:request.error];
            [subscriber sendCompleted];
        }];
        
        return nil;
    }];
    return signal;
}
@end

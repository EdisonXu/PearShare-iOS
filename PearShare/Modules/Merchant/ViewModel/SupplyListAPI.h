//
//  SupplyListAPI.h
//  PearShare
//
//  Created by Destiny on 2018/6/13.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "BaseRequestAPI.h"

@interface SupplyListAPI : BaseRequestAPI
@property (nonatomic,strong) NSString* pageNum;
@property (nonatomic,strong) NSString* type;
@property (nonatomic,strong) NSString* pageSize;
@end

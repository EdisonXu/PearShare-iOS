//
//  ScheduleAPI.m
//  PearShare
//
//  Created by Destiny on 2018/6/14.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ScheduleAPI.h"

@implementation ScheduleAPI
-(NSString *)requestUrl{
    return @"user/process/";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (id)requestArgument {
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setString:self.applyId forKey:@"applyId"];
    return param;
}

- (NSDictionary *)requestHeaderFieldValueDictionary {
    NSMutableDictionary* param =[NSMutableDictionary dictionary];
    [param setString:kDefaultUserId forKey:@"userId"];
    [param setString:kDefaultUserToken forKey:@"token"];
    return param;
}
@end

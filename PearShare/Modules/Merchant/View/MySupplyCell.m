//
//  MySupplyCell.m
//  PearShare
//
//  Created by Destiny on 2018/5/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MySupplyCell.h"
#import "SupplySchedule.h"
 @interface MySupplyCell()
@property (nonatomic,strong) NSMutableDictionary *data;
@end
@implementation MySupplyCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(void)setSupplyData:(NSDictionary *)supplyData
{
    if ([[supplyData stringForKey:@"process"] isEqualToString:@"128"]) {
        self.lblStaus.text = @"初审中";
    }else if ([[supplyData stringForKey:@"process"] isEqualToString:@"129"]){
        self.lblStaus.text = @"邮寄中";
    }else if ([[supplyData stringForKey:@"process"] isEqualToString:@"132"]){
        self.lblStaus.text = @"排期中";
    }else if ([[supplyData stringForKey:@"process"] isEqualToString:@"133"]){
        self.lblStaus.text = @"开团中";
    }else if ([[supplyData stringForKey:@"process"] isEqualToString:@"134"]){
        self.lblStaus.text = @"已截团";
    }
    
    if ([[supplyData stringForKey:@"status"] isEqualToString:@"2"]){
        self.lblStaus.text = @"未通过";
    }
    
     NSArray  *array = [[supplyData stringForKey:@"cover"] componentsSeparatedByString:@","];
    
    [self.imageCover sd_setImageWithURL:[NSURL URLWithString:[array stringWithIndex:0]] placeholderImage:[UIImage imageNamed:@"mall_empty"] ];
    self.lblTitle.text = [supplyData stringForKey:@"name"];
    self.lblPrice.text =[NSString stringWithFormat:@"%@%@",@" ¥  ", [supplyData stringForKey:@"activityAmt"]];
    self.lblSubtitle.text = [supplyData stringForKey:@"description"];
    
    self.data = [NSMutableDictionary dictionaryWithDictionary:supplyData];
}

- (IBAction)scheduleCommand:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MerchantStoryboard" bundle:nil];
    SupplySchedule *vc = [storyboard instantiateViewControllerWithIdentifier:@"SupplySchedule"];
    vc.applyId =[self.data stringForKey:@"applyId"];
    [self.viewController.navigationController pushViewController:vc animated:YES ];
}


#pragma mark  获取当前控制器
- (UIViewController *)getCurrentVC{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    //app默认windowLevel是UIWindowLevelNormal，如果不是，找到UIWindowLevelNormal的
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
        
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    return result;
}
@end

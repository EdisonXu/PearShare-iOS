//
//  FailureAudit.m
//  PearShare
//
//  Created by Destiny on 2018/5/7.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "FailureAudit.h"

@interface FailureAudit ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (nonatomic,strong)NSString * failurUrl;
@end

@implementation FailureAudit

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"审核失败";
//    [self wr_setNavBarBackgroundAlpha:1];
//    //    // 设置导航栏按钮和标题颜色
//    [self wr_setNavBarTintColor:[UIColor colorWithRed:42/255.0 green:42/255.0 blue:42/255.0 alpha:1.0]];
}

#pragma mark - 相册代理
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *cutImage=info[UIImagePickerControllerEditedImage];
    [self.imageFruile setImage:cutImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    MBProgressHUD *hudPrce = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HYBNetworking uploadWithImage:cutImage url:[baseRequestUrl stringByAppendingString:@"imgupload/imgUpload"] filename:@"header.png" name:@"upfile" mimeType:@"image/jpeg" parameters:nil progress:^(int64_t bytesWritten, int64_t totalBytesWritten) {
    } success:^(NSDictionary* response) {
        [hudPrce hideAnimated:YES];
        self.failurUrl = [NSString stringWithFormat:@"%@", [PinImageUrl stringByAppendingString:[response stringForKey:@"data"]]];
    } fail:^(NSError *error) {
          [hudPrce hideAnimated:YES];
    }];
}

-(void) picClick{
    UIAlertController* actionSheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* weiboAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* vc = [UIImagePickerController new];
        vc.delegate = self;
        vc.allowsEditing = YES;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    UIAlertAction* weixinAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UIImagePickerController* vc = [UIImagePickerController new];
        vc.allowsEditing = YES;
        vc.delegate = self;
        // 选择照片数据源 --- 默认是相册
        vc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:vc animated:YES completion:nil];
    }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [actionSheetController addAction:weiboAction];
    [actionSheetController addAction:weixinAction];
    [actionSheetController addAction:cancelAction];
    
    [cancelAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    [self presentViewController:actionSheetController animated:YES completion:nil];
}

- (IBAction)addImage:(id)sender {
    [self picClick];
}
- (IBAction)sureCommand:(id)sender {
    MBProgressHUD *hudPrce = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary * param = [[NSMutableDictionary alloc]init];
    [param setObj:kDefaultUserId forKey:@"userId"];
    [param setObj:self.applyId forKey:@"applyId"];
    [param setObj:self.failurUrl forKey:@"Img"];
    [param setObj:@"3" forKey:@"sampleName"];
    
    [HYBNetworking postWithUrl:[baseRequestUrl stringByAppendingString:@"supplier/addRefund"] refreshCache:YES params:param success:^(NSDictionary* response) {
        [hudPrce hideAnimated:YES];
        if ([[response stringForKey:@"code"] isEqualToString:@"1"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.label.text = [response stringForKey:@"msg"];
            [hud hideAnimated:YES afterDelay:2];
        }
    } fail:^(NSError *error) {
        [hudPrce hideAnimated:YES];
    }];
}

@end

//
//  MySupplyCell.h
//  PearShare
//
//  Created by Destiny on 2018/5/4.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySupplyCell : UITableViewCell
@property (nonatomic,strong)NSDictionary* supplyData;
@property (weak, nonatomic) IBOutlet UIImageView *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblStaus;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@end

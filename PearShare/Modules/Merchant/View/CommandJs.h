//
//  CommandJs.h
//  CCZAPPiOS
//
//  Created by Yang on 16/10/9.
//  Copyright © 2016年 ccz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>

@protocol JSObjextProtocol <JSExport>

-(void)goHisBack;

@end

@interface CommandJs : NSObject <JSObjextProtocol>

@property (nonatomic,strong)NSString * gameId;

@property (nonatomic,strong)NSDictionary * title;


@end

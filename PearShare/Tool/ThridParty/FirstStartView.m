//
//  FirstStartView.m
//  QTFootball
//
//  Created by 汤鹏 on 16/3/21.
//  Copyright © 2016年 quantum. All rights reserved.
//

#import "FirstStartView.h"
#import "SureGuideView.h"
@interface FirstStartView()<UIScrollViewDelegate>
{
            CGFloat _beginDecler;
}
@end
@implementation FirstStartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma  mark - ScrollView 代理方法
-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    _beginDecler = scrollView.contentOffset.x;
}
-(void)scrollViewDidEndDecelerating:(UIScrollView*)scrollView;{
    //判断是否滑到了最右边
    if(_beginDecler>0&& scrollView.contentOffset.x == _beginDecler){
        //跳转到首页
        
        [self toIndex];
    }
}
-(void) toIndex{
    [UIView animateWithDuration:1 animations:^{
        self.alpha=0;
    } completion:^(BOOL finished) {
        //比较特殊，发个消息激活引导页
//        [[NSNotificationCenter defaultCenter]postNotificationName:kMatchNewViewController object:kFirstStart];
        [SureGuideView sureGuideViewWithImageName:@"guide" imageCount:2];
            [self removeFromSuperview];
//        [SureGuideView sureGuideViewWithImageName:@"guide" imageCount:2];
    }];

}

@end

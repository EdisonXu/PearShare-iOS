//
//  MPSValidator.m
//  mplus
//
//  Created by QFish on 10/22/14.
//  Copyright (c) 2014 geek-zoo. All rights reserved.
//

#import "MQValidator.h"

@implementation MQValidator

+ (BOOL)isPhoneNumber:(NSString *)string {
    NSString *MOBILE = @"^((13[0-9])|(15[0-9])|(17[0-9])|(18[0-9]))\\d{8}$";
    NSPredicate *regextestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    return [regextestMobile evaluateWithObject:string];
}

+ (BOOL)isUserName:(NSString *)string {
    //TODO(名称限制规则?)

    if (string.length < 17 && string.length > 0) {
        return YES;
    }

    return NO;
}

+ (BOOL)isEmail:(NSString *)string {
    NSString *regx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regx];
    return [pred evaluateWithObject:string];
}

+ (BOOL)isPassword:(NSString *)string {
    NSString *regex = @"([\\w|!|@|#|$|%|^|&|*|+|-|/|_|{|}|[|]|=|~|<|>|?|.|,|(|)|:]{6,20}$)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

    return [pred evaluateWithObject:string];
}

+ (BOOL)isVerifyCode:(NSString *)string {
    NSString *regex = @"(^[A-Za-z0-9]{4,}$)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

    return [pred evaluateWithObject:string];
}

+ (BOOL)isNumOrLetter:(NSString *)string {
    NSString *regex = @"(^[A-Za-z0-9]{0,}$)";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

    return [pred evaluateWithObject:string];
}

+ (BOOL)isNum:(NSString *)string {
    NSString *regex = @"^[0-9]*$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];

    return [pred evaluateWithObject:string];
}
+(BOOL)isCarNo:(NSString*)string
{
    NSString *carRegex = @"^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{5}$";
    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",carRegex];
    return [carTest evaluateWithObject:string];
}
+ (BOOL) isNickname:(NSString *)nickname
{
    NSString *nicknameRegex = @".*[\\ ,|\\.|\\`|\\~|\\!|\\@|\\#|\\$|\\%|\\^|\\+|\\*|\\&|\\\\|\\/|\\?|\\||\\:|\\.|\\|\\{|\\}|\\(|\\)|\\''|\\;|\\=|\\\\]+.*";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nicknameRegex];
    return ![passWordPredicate evaluateWithObject:nickname];
}
@end

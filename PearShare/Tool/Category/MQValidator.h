//
//  MPSValidator.h
//  mplus
//
//  Created by QFish on 10/22/14.
//  Copyright (c) 2014 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MQValidator : NSObject

// 11位数字
+ (BOOL)isPhoneNumber:(NSString *)string;

// 邮箱
+ (BOOL)isEmail:(NSString *)string;

// 长度大于2
+ (BOOL)isUserName:(NSString *)string;

// 6-16 字母数字
+ (BOOL)isPassword:(NSString *)string;

// 验证码
+ (BOOL)isVerifyCode:(NSString *)string;

// 是否是数字和字母
+ (BOOL)isNumOrLetter:(NSString *)string;

// 是否是数字
+ (BOOL)isNum:(NSString *)string;

/*车牌号验证*/
+(BOOL)isCarNo:(NSString*)string;
//昵称
+ (BOOL) isNickname:(NSString *)nickname;
@end

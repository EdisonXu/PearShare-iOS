//
//  UIView+Extension.h
//  MJRefreshExample
//
//  Created by MJ Lee on 14-5-28.
//  Copyright (c) 2014年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GradientDirection) {
    GradientDirectionTopToBottom,
    GradientDirectionLeftToRight,
    GradientDirectionUpleftTolowRight,
    GradientDirectionLowLeftToUpright,
};
@interface UIView (TP)
@property (assign, nonatomic) CGFloat x;
@property (assign, nonatomic) CGFloat y;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat bottom;
@property (assign, nonatomic) CGFloat right;
@property (assign, nonatomic) CGSize size;
@property (assign, nonatomic) CGPoint origin;

-(void)removeAllSubViews;
-(void) gradientColor: (NSArray *)colors direction :(GradientDirection)direction;

-(void) removeAllSubLayers;
@end

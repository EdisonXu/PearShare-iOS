//
//  YXProgress.m
//  QTFootBall_2.6.0
//
//  Created by Edison on 2017/6/27.
//  Copyright © 2017年 Edison. All rights reserved.
//

#import "YXProgress.h"

@implementation YXProgress

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        // 背景图像
        
        _trackView =
        
        [[UIImageView alloc] initWithFrame:CGRectMake (0, 0, frame.size.width, frame.size.height)];
        
        [_trackView setImage:[UIImage imageNamed:@"match_awaytrack.png"]];
        _trackView.contentMode = UIViewContentModeScaleAspectFill;
        
        //当前view的主要作用是将出界了的_progressView剪切掉，所以需将clipsToBounds设置为YES
        
        _trackView.clipsToBounds = YES;
        
        [self addSubview:_trackView];
        
        // 填充图像
        
        _progressView = [[UIImageView alloc]
                         
                         initWithFrame:CGRectMake (0 - frame.size.width, 0, frame.size.width, frame.size.height)];
        
        [_progressView setImage:[UIImage imageNamed:@"match_homeprogress.png"]];
        
        [_trackView addSubview:_progressView];
        
        UIImageView * i = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"match_ scale"]];
      i.frame= CGRectMake (0, 0, frame.size.width, frame.size.height);
        
        [self addSubview:i];
    }
    
    return self;
    
}

//设置进度条的值

- (void)setProgress:(CGFloat)progress

{
    
    _targetProgress = progress;
    
    [self changeProgressViewFrame];
    
}

//修改显示内容

- (void)changeProgressViewFrame

{
    
    _progressView.frame = CGRectMake (self.frame.size.width * _targetProgress - self.frame.size.width,
                                      
                                      0, self.frame.size.width, self.frame.size.height);
    
}

@end

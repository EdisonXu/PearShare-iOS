//
//  NSMutableArray+InsertArray.h
//  
//
//  Created by Edison on 2017/7/6.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (InsertArray)
- (void)insertArray:(NSArray *)newAdditions atIndex:(NSUInteger)index;
@end

//
//  UIImage+Resize.m
//
//  Created by Olivier Halligon on 12/08/09.
//  Copyright 2009 AliSoftware. All rights reserved.
//

#import "UIImage+Resize.h"
@implementation UIImage (ResizeCategory)

-(UIImage*)resizedImageToRefrenceWidth:(CGFloat)refWidth
{
    if (refWidth<=0) {
        return self;
    }
	CGImageRef imgRef = self.CGImage;
	// the below values are regardless of orientation : for UIImages from Camera, width>height (landscape)
	CGSize  srcSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef)); // not equivalent to self.size
	CGFloat scaleRatio = refWidth / srcSize.width;
    CGFloat refHeight = srcSize.height*scaleRatio;
	UIImageOrientation orient = self.imageOrientation;
	CGAffineTransform transform = CGAffineTransformIdentity;
	switch(orient) {
		case UIImageOrientationUp: //EXIF = 1
            if (refWidth-srcSize.width>=0) {
                return self;
            }
			transform = CGAffineTransformIdentity;
			break;
			
		case UIImageOrientationUpMirrored: //EXIF = 2
            if (refWidth-srcSize.width>=0) {
                return self;
            }
			transform = CGAffineTransformMakeTranslation(srcSize.width, 0.0);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			break;
			
		case UIImageOrientationDown: //EXIF = 3
            if (refWidth-srcSize.width>=0) {
                return self;
            }
			transform = CGAffineTransformMakeTranslation(srcSize.width, srcSize.height);
			transform = CGAffineTransformRotate(transform, M_PI);
			break;
			
		case UIImageOrientationDownMirrored: //EXIF = 4
            if (refWidth-srcSize.width>=0) {
                return self;
            }
			transform = CGAffineTransformMakeTranslation(0.0, srcSize.height);
			transform = CGAffineTransformScale(transform, 1.0, -1.0);
			break;
			
		case UIImageOrientationLeftMirrored: //EXIF = 5
            if (refWidth-srcSize.height>=0) {
                return self;
            }
//			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(srcSize.height, srcSize.width);
			transform = CGAffineTransformScale(transform, -1.0, 1.0);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
            refHeight = srcSize.width*scaleRatio;
             scaleRatio=refWidth/srcSize.height;
			break;
		case UIImageOrientationLeft: //EXIF = 6
              if (refWidth-srcSize.height>=0) {
                return self;
            }
//			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(0.0, srcSize.width);
			transform = CGAffineTransformRotate(transform, 3.0 * M_PI_2);
                        refHeight = srcSize.width*scaleRatio;
             scaleRatio=refWidth/srcSize.height;
			break;  
			
		case UIImageOrientationRightMirrored: //EXIF = 7
              if (refWidth-srcSize.height>=0) {
                return self;
            }
//			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeScale(-1.0, 1.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
                        refHeight = srcSize.width*scaleRatio;
             scaleRatio=refWidth/srcSize.height;
			break;  
			
		case UIImageOrientationRight: //EXIF = 8
              if (refWidth-srcSize.height>=0) {
                  return self;
            }
//			dstSize = CGSizeMake(dstSize.height, dstSize.width);
			transform = CGAffineTransformMakeTranslation(srcSize.height, 0.0);
			transform = CGAffineTransformRotate(transform, M_PI_2);
            scaleRatio=refWidth/srcSize.height;
            refHeight = srcSize.width*scaleRatio;
			break;  
			
		default:  
			[NSException raise:NSInternalInconsistencyException format:@"未知的图片格式"];
			
	}  
	
	/////////////////////////////////////////////////////////////////////////////
	// The actual resize: draw the image on a new context, applying a transform matrix
    CGSize drawSize = CGSizeMake(refWidth, refHeight);
	UIGraphicsBeginImageContextWithOptions(drawSize, NO, self.scale);
	CGContextRef context = UIGraphicsGetCurrentContext();
       if (!context) {
           return nil;
       }
	if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
		CGContextScaleCTM(context, -scaleRatio, scaleRatio);
		CGContextTranslateCTM(context, -srcSize.height, 0);
	} else {
		CGContextScaleCTM(context, scaleRatio, -scaleRatio);
		CGContextTranslateCTM(context, 0, -srcSize.height);
	}
		CGContextConcatCTM(context, transform);

	
	// we use srcSize (and not dstSize) as the size to specify is in user space (and we use the CTM to apply a scaleRatio)
	CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, srcSize.width, srcSize.height), imgRef);
	UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return resizedImage;
}
+ (NSData *)compressImage:(UIImage *)image
{
    CGFloat compression = 0.3f;
    CGFloat maxCompression = 0.1f;
    int maxFileSize = 125  * 1024;
    
    NSData *imageData = UIImageJPEGRepresentation(image, compression);
     NSLog(@"压缩前%zd",imageData.length);
    
    while ([imageData length] > maxFileSize && compression > maxCompression)
    {
        compression -= 0.2;
        imageData = UIImageJPEGRepresentation(image, compression);
        NSLog(@"压缩后%zd",imageData.length);
    }
    return imageData;
}


/////////////////////////////////////////////////////////////////////////////



//-(UIImage*)resizedImageToFitInSize:(CGSize)boundingSize scaleIfSmaller:(BOOL)scale
//{
//	// get the image size (independant of imageOrientation)
//	CGImageRef imgRef = self.CGImage;
//	CGSize srcSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef)); // not equivalent to self.size (which depends on the imageOrientation)!
//
//	// adjust boundingSize to make it independant on imageOrientation too for farther computations
//	UIImageOrientation orient = self.imageOrientation;  
//	switch (orient) {
//		case UIImageOrientationLeft:
//		case UIImageOrientationRight:
//		case UIImageOrientationLeftMirrored:
//		case UIImageOrientationRightMirrored:
//			boundingSize = CGSizeMake(boundingSize.height, boundingSize.width);
//			break;
//        default:
//            // NOP
//            break;
//	}
//
//	// Compute the target CGRect in order to keep aspect-ratio
//	CGSize dstSize;
//	
//	if ( !scale && (srcSize.width < boundingSize.width) && (srcSize.height < boundingSize.height) ) {
//		//NSLog(@"Image is smaller, and we asked not to scale it in this case (scaleIfSmaller:NO)");
//		dstSize = srcSize; // no resize (we could directly return 'self' here, but we draw the image anyway to take image orientation into account)
//	} else {		
//		CGFloat wRatio = boundingSize.width / srcSize.width;
//		CGFloat hRatio = boundingSize.height / srcSize.height;
//		
//		if (wRatio < hRatio) {
//			//NSLog(@"Width imposed, Height scaled ; ratio = %f",wRatio);
//			dstSize = CGSizeMake(boundingSize.width, floorf(srcSize.height * wRatio));
//		} else {
//			//NSLog(@"Height imposed, Width scaled ; ratio = %f",hRatio);
//			dstSize = CGSizeMake(floorf(srcSize.width * hRatio), boundingSize.height);
//		}
//	}
//		
//	return [self resizedImageToSize:dstSize];
//}

@end

//
//  YXProgress.h
//  QTFootBall_2.6.0
//
//  Created by Edison on 2017/6/27.
//  Copyright © 2017年 Edison. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXProgress : UIView
// 进度条背景图片

@property (retain, nonatomic) UIImageView *trackView;

// 进图条填充图片

@property (retain, nonatomic) UIImageView *progressView;

//进度

@property (nonatomic) CGFloat targetProgress;

//设置进度条的值

- (void)setProgress:(CGFloat)progress;
@end

//
//  UIView+SLYExtension.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/3.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import "UIView+SLYExtension.h"

@implementation UIView (SLYExtension)
@dynamic viewController;

- (UIViewController *)viewController
{
    UIViewController *viewController=nil;
    UIView* next = [self superview];
    UIResponder *nextResponder = [next nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]])
    {
        viewController = (UIViewController *)nextResponder;
    }
    else
    {
        viewController = [next viewController];
    }
    return viewController;
}

@dynamic cornerRadius,boardColor,borderWidth;
- (void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.masksToBounds = YES;
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = cornerRadius;
}

- (void)setBoardColor:(UIColor *)boardColor
{
    if (self.layer.borderWidth == 0) {
        self.layer.borderWidth = 1;
    }
    self.layer.borderColor = boardColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth = borderWidth;
}


@end

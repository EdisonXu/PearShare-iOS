//
//  UIButton+RightImage.h
//  MeiQiReferrer
//
//  Created by neil on 15/3/25.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (SLYExtension)

@property(nonatomic, strong) IBInspectable UIImage *rightImage;

@property(nonatomic, strong) IBInspectable UIImage *topImage;

@property(nonatomic, strong) IBInspectable UIColor *backgroundColorAtNormal;

@property(nonatomic, assign) IBInspectable BOOL txtColor;

/**
 *   @brief  行间距
 */
@property(nonatomic, assign) IBInspectable CGFloat lineSpacing;
@end

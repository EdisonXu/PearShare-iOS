//
//  NSMutableArray+InsertArray.m
//  
//
//  Created by Edison on 2017/7/6.
//
//

#import "NSMutableArray+InsertArray.h"

@implementation NSMutableArray (InsertArray)
- (void)insertArray:(NSArray *)newAdditions atIndex:(NSUInteger)index
{
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    for(int i = index ;i < newAdditions.count+index;i++)
    {
        [indexes addIndex:i];
    }
    [self insertObjects:newAdditions atIndexes:indexes];
}
@end

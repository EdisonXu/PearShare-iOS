//
//  UIView+Extension.m
//  MJRefreshExample
//
//  Created by MJ Lee on 14-5-28.
//  Copyright (c) 2014年 itcast. All rights reserved.
//

 

@implementation UIView (TP)
- (void)setX:(CGFloat)x
{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

- (CGFloat)x
{
    return self.frame.origin.x;
}

- (void)setY:(CGFloat)y
{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setWidth:(CGFloat)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setHeight:(CGFloat)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGFloat)height
{
    return self.frame.size.height;
}
- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}
- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}
- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}
-(void)setRight:(CGFloat)right{
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}
- (void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}

- (CGPoint)origin
{
    return self.frame.origin;
}

-(void)removeAllSubViews{
    for (UIView *view in [self subviews]) {
        [view removeFromSuperview];
    }
}
-(void) removeAllSubLayers{
    for (CALayer * l in [self.layer sublayers]) {
        [l removeFromSuperlayer];
    }
}
//#pragma 渐变色
//-(void)gradientColor:(NSArray *)colors direction:(GradientDirection)direction{
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.bounds;
//    gradient.colors = colors;
//    switch (direction) {
//        case GradientDirectionLeftToRight:
//            gradient.startPoint=CGPointMake(0, 0.5);
//            gradient.endPoint=CGPointMake(1.0, 0.5);
//            break;
//        case GradientDirectionUpleftTolowRight:
//            gradient.startPoint=CGPointMake(0, 0);
//            gradient.endPoint=CGPointMake(1.0, 1.0);
//            break;
//        case GradientDirectionTopToBottom:
//            gradient.startPoint=CGPointMake(0.5, 0);
//            gradient.endPoint=CGPointMake(0.5, 1.0);
//            break;
//        case GradientDirectionLowLeftToUpright:
//            gradient.startPoint=CGPointMake(0, 1);
//            gradient.endPoint=CGPointMake(1.0, 0);
//            break;
//        default:
//            break;
//    }
//
////    [self.layer addSublayer:gradient];
//    [self.layer insertSublayer:gradient atIndex:0];
//}
@end

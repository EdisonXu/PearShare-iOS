//
//  NSString+StringCommon.m
//  蓝莓
//
//  Created by 汤鹏 on 14-9-11.
//  Copyright (c) 2014年 lhxm. All rights reserved.
//

#import "NSString+StringCommon.h"


@implementation NSString (StringCommon)

+ (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

@end

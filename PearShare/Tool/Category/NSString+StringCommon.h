//
//  NSString+StringCommon.h
//  蓝莓
//
//  Created by 汤鹏 on 14-9-11.
//  Copyright (c) 2014年 lhxm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringCommon)
+ (BOOL) isBlankString:(NSString * )string;

@end

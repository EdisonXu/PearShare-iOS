//
//  MoreTextModel.m
//  PearShare
//
//  Created by Destiny on 2018/6/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "MoreTextModel.h"

@implementation MoreTextModel
-(CGFloat)heightForRowWithisShow:(BOOL)isShow
{//105
    if (!isShow) {
        //展开
        if ([self heightForString:_workInfo fontSize:14 andWidth:SCREEN_WIDTH - 24] > 55) {
            _showBtn = YES;
            return 100;
        }
        else
        {
            _showBtn = NO;
            return [self heightForString:_workInfo fontSize:14 andWidth:SCREEN_WIDTH - 24] +60;
        }
    }else
    {
        //        收起
        return [self heightForString:_workInfo fontSize:14 andWidth:SCREEN_WIDTH - 24] + 60;
    }
}

- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width{
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 0)];
    detailLabel.font = [UIFont systemFontOfSize:fontSize];
    detailLabel.text = value;
    detailLabel.numberOfLines = 0;
    CGSize deSize = [detailLabel sizeThatFits:CGSizeMake(width,1)];
    NSLog(@" labelHight== %f",deSize.height);
    return deSize.height;
}

@end

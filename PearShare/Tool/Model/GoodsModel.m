//
//  GoodsModel.m
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "GoodsModel.h"

@implementation GoodsModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"articleId": @"articleId",
                                                                  @"basePrice": @"basePrice",
                                                                  @"bgImg": @"bgImg",
                                                                  @"buyCount": @"buyCount",
                                                                  @"createTime": @"createTime",
                                                                  @"currentPrice": @"currentPrice",
                                                                  @"dictId": @"dictId",
                                                                  @"enable": @"enable",
                                                                  @"endTime": @"endTime",
                                                                  @"face": @"face",
                                                                  @"goodsId": @"goodsId",
                                                                  @"goodsTags": @"goodsTags",
                                                                  @"imageStr": @"imageStr",
                                                                  @"isHot": @"isHot",
                                                                  @"isRecommend": @"isRecommend",
                                                                  @"isReturn": @"isReturn",
                                                                  @"likeCount": @"likeCount",
                                                                  @"modifiedTime": @"modifiedTime",
                                                                  @"name": @"name",
                                                                  @"orders": @"orders",
                                                                  @"originalPrice": @"originalPrice",
                                                                  @"reFollow": @"reFollow",
                                                                  @"readNumber": @"readNumber",
                                                                  @"sales": @"sales",
                                                                  @"status": @"status",
                                                                  @"subTitle": @"subTitle",
                                                                  @"supplyId": @"supplyId",
                                                                  @"tags": @"tags",
                                                                  @"title": @"title",
                                                                  @"url": @"url"
                                                                  }];
}
@end

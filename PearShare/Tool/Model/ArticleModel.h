//
//  ArticleModel.h
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@interface ArticleModel : JSONModel
//@property (nonatomic,strong)NSString *id
@property (nonatomic,strong)NSString *cover;
@property (nonatomic,strong)NSString *groupStatus;
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *keywords;
@property (nonatomic,strong)NSString *collectionsNum;
@property (nonatomic,strong)NSString *userId;
@property (nonatomic,strong)NSString *avatar;
@property (nonatomic,strong)NSString *nickname;
@property (nonatomic,strong)NSString *isBind;
@property (nonatomic,strong)NSString *goodsAmt;
@end

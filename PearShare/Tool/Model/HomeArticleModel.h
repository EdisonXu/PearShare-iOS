//
//  HomeArticleModel.h
//  PearShare
//
//  Created by Destiny on 2018/6/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface HomeArticleModel : JSONModel
@property (nonatomic,strong)NSString *avatar;
@property (nonatomic,strong)NSString *collectionsNum;
@property (nonatomic,strong)NSString *cover;
@property (nonatomic,strong)NSString *goodsAmt;
@property (nonatomic,strong)NSString *groupStatus;
@property (nonatomic,strong)NSString *userId;
@property (nonatomic,strong)NSString *nickname;
@property (nonatomic,strong)NSString *shareId;
@property (nonatomic,strong)NSString *subTitle;
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *isCollections;
@property (nonatomic,strong)NSString *goodsId;
@end

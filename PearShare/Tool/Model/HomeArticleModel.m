
//
//  HomeArticleModel.m
//  PearShare
//
//  Created by Destiny on 2018/6/12.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "HomeArticleModel.h"

@implementation HomeArticleModel
+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"avatar": @"avatar",
                                                                  @"collectionsNum": @"collectionsNum",
                                                                  @"cover": @"cover",
                                                                  @"goodsAmt": @"goodsAmt",
                                                                  @"groupStatus": @"groupStatus",
                                                                  @"userId": @"userId",
                                                                  @"nickname": @"nickname",
                                                                  @"shareId": @"shareId",
                                                                  @"subTitle": @"subTitle",
                                                                  @"title": @"title",
                                                                  @"goodsId": @"goodsId",
                                                                  @"isCollections": @"isCollections"
                                                                  }];
}
@end

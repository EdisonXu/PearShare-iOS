//
//  MoreTextModel.h
//  PearShare
//
//  Created by Destiny on 2018/6/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoreTextModel : NSObject
@property (nonatomic, strong)NSString *workInfo;
@property (nonatomic, assign)BOOL isShow;
@property (nonatomic, assign)BOOL showBtn;
-(CGFloat)heightForRowWithisShow:(BOOL)isShow;
@end

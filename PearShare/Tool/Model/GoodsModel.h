//
//  GoodsModel.h
//  PearShare
//
//  Created by 徐源鑫 on 2018/4/17.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface GoodsModel : JSONModel
@property (strong, nonatomic) NSString* articleId;
@property (strong, nonatomic) NSString* basePrice;
@property (strong, nonatomic) NSString* bgImg;
@property (strong, nonatomic) NSString* buyCount;
@property (strong, nonatomic) NSString* createTime;
@property (strong, nonatomic) NSString* currentPrice;
@property (strong, nonatomic) NSString* dictId;
@property (strong, nonatomic) NSString* enable;
@property (strong, nonatomic) NSString* endTime;
@property (strong, nonatomic) NSString* face;
@property (strong, nonatomic) NSString* goodsId;
@property (strong, nonatomic) NSString* goodsTags;
@property (strong, nonatomic) NSString* imageStr;
@property (strong, nonatomic) NSString* isHot;
@property (strong, nonatomic) NSString* isRecommend;
@property (strong, nonatomic) NSString* isReturn;
@property (strong, nonatomic) NSString* likeCount;
@property (strong, nonatomic) NSString* modifiedTime;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* orders;
@property (strong, nonatomic) NSString* originalPrice;
@property (strong, nonatomic) NSString* reFollow;
@property (strong, nonatomic) NSString* readNumber;
@property (strong, nonatomic) NSString* sales;
@property (strong, nonatomic) NSString* status;
@property (strong, nonatomic) NSString* subTitle;
@property (strong, nonatomic) NSString* supplyId;
@property (strong, nonatomic) NSString* tags;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSString* url;
@property (nonatomic,assign)BOOL isRemind;
@end

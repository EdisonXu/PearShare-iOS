//
//  ArticleModel.m
//  PearShare
//
//  Created by Destiny on 2018/6/9.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "ArticleModel.h"

@implementation ArticleModel

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"cover": @"cover",
                                                                  @"groupStatus": @"groupStatus",
                                                                  @"title": @"title",
                                                                  @"keywords": @"keywords",
                                                                  @"collectionsNum": @"collectionsNum",
                                                                  @"userId": @"userId",
                                                                  @"avatar": @"avatar",
                                                                  @"nickname": @"nickname",
                                                                  @"isBind": @"isBind",
                                                                  @"goodsAmt": @"goodsAmt"
                                                                  }];
}
@end

//
//  PearShareManager.m
//  PearShare
//
//  Created by Destiny on 2018/5/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import "PearShareManager.h"
#import "PPNetworkHelper.h"
@implementation PearShareManager

#pragma mark - life cycle
+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSLog(@"APPManager  init");
        //网络配置
        [HYBNetworking enableInterfaceDebug:YES];
        [self homeClass];
    }
    return self;
}



-(void)homeClass
{
    NSDictionary*param = [NSDictionary dictionaryWithObject:@"1" forKey:@"type"];
    [PPNetworkHelper GET:[[URL_main stringByAppendingString:@"/system/dics/"]stringByAppendingString:@"4"] parameters:param success:^(NSDictionary* responseObject) {
        if ([responseObject boolForKey:@"success"]) {
           NSMutableArray*list = [NSMutableArray arrayWithArray:[responseObject arrayForKey:@"data"]];
            NSMutableDictionary* all = [NSMutableDictionary dictionaryWithObject:@"全部" forKey:@"name"];
            [list insertObject:all atIndex:0];
            if (list.count) {
                [kDefaults setObject:list forKey:@"homeClass"];
                [kDefaults synchronize];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark  获取当前控制器
- (UIViewController *)getCurrentVC{
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal){
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows){
            if (tmpWin.windowLevel == UIWindowLevelNormal){
                window = tmpWin;
                break;
            }
        }
    }
    id  nextResponder = nil;
    UIViewController *appRootVC=window.rootViewController;
    //    如果是present上来的appRootVC.presentedViewController 不为nil
    if (appRootVC.presentedViewController) {
        nextResponder = appRootVC.presentedViewController;
    }else{
        UIView *frontView = [[window subviews] objectAtIndex:0];
        nextResponder = [frontView nextResponder];
    }
    if ([nextResponder isKindOfClass:[UITabBarController class]]){
        UITabBarController * tabbar = (UITabBarController *)nextResponder;
        UINavigationController * nav = (UINavigationController *)tabbar.viewControllers[tabbar.selectedIndex];
        //        UINavigationController * nav = tabbar.selectedViewController ; 上下两种写法都行
        result=nav.childViewControllers.lastObject;
    }else if ([nextResponder isKindOfClass:[UINavigationController class]]){
        UIViewController * nav = (UIViewController *)nextResponder;
        result = nav.childViewControllers.lastObject;
    }else{
        result = nextResponder;
    }
    return result;
}
@end

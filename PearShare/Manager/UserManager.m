//
//  UserManager.m
//  MiAiApp
//
//  Created by 徐阳 on 2017/5/22.
//  Copyright © 2017年 徐阳. All rights reserved.
//

#import "UserManager.h"
#import "LoginAPI.h"
@implementation UserManager

SINGLETON_FOR_CLASS(UserManager);

-(instancetype)init{
    self = [super init];
    if (self) {
        //被踢下线
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onKick)
                                                     name:KNotificationOnKick
                                                   object:nil];
    }
    return self;
}

#pragma mark ————— 三方登录 —————
-(void)login:(UserLoginType )loginType completion:(loginBlock)completion{
    [self login:loginType params:nil completion:completion];
}

#pragma mark ————— 带参数登录 —————
-(void)login:(UserLoginType )loginType params:(NSDictionary *)params completion:(loginBlock)completion{
    if (loginType==kUserLoginTypeWeChat) {
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
            if (error) {
                
            } else {
                UMSocialUserInfoResponse *resp = result;
                LoginAPI *req = [LoginAPI new];
                req.loginType =@"2";
                req.openId = resp.openid;
                req.nickname = resp.name;
                req.avatar = resp.iconurl;
                
                [req startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                    if ([req isSuccess]) {

                    }else{
                        NSLog(@"%@",[req message]);
                        if ([[req code] isEqualToString:@"1006"]) {
                            
                        }
                    }
                } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

                }];
            }
        }];
    }else{
        
    }

}

-(void)loginSucess:(NSDictionary*)info
{
//    self.curUserInfo = [UserInfo modelWithDictionary:info];
//     [self saveUserInfo];
}

#pragma mark ————— 储存用户信息 —————
-(void)saveUserInfo{
    if (self.curUserInfo) {
//        YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
//        NSDictionary *dic = [self.curUserInfo modelToJSONObject];
//        [cache setObject:dic forKey:KUserModelCache];
//        self.isLogined = YES;
    }
}

#pragma mark ————— 加载缓存的用户信息 —————
-(BOOL)loadUserInfo{
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    NSDictionary * userDic = (NSDictionary *)[cache objectForKey:KUserModelCache];
    if (userDic) {
//        self.curUserInfo = [UserInfo modelWithJSON:userDic];
        return YES;
    }
    return NO;
}

#pragma mark ————— 退出登录 —————
- (void)logout:(void (^)(BOOL, NSString *))completion{
    self.isLogined = NO;
    //移除缓存
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    [cache removeAllObjectsWithBlock:^{
        if (completion) {
            completion(YES,nil);
        }
    }];
    KPostNotification(KNotificationLoginStateChange, @NO);
}

-(void)loginOut
{
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    [cache removeAllObjects];
}
@end

//
//  PearShareManager.h
//  PearShare
//
//  Created by Destiny on 2018/5/26.
//  Copyright © 2018年 Edison. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PearShareManager : NSObject

+ (instancetype)sharedInstance;

- (UIViewController *)getCurrentVC;

@end

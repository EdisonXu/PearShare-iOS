//
//  UserInfo.h
//  MiAiApp
//
//  Created by 徐阳 on 2017/5/23.
//  Copyright © 2017年 徐阳. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GameInfo;

typedef NS_ENUM(NSInteger,UserGender){
    UserGenderUnKnow = 0,
    UserGenderMale, //男
    UserGenderFemale //女
};

@interface UserInfo : NSObject

@property(nonatomic,copy)  NSString* userId;//用户ID
@property(nonatomic,copy)  NSString * nickname;//昵称
@property (nonatomic,copy) NSString * phone;//手机
@property (nonatomic,copy) NSString * areaId;//区域
@property (nonatomic,copy) NSString * avatar;//头像
@property (nonatomic,copy) NSString * type;//用户状态
@property (nonatomic, assign) UserGender sex;//性别
@property (nonatomic,copy) NSString * openidTb;//淘宝openid
@property (nonatomic,copy) NSString * openidWb;//微博openid
@property (nonatomic,copy) NSString * openidWx;//微信openid
@property (nonatomic,copy) NSString * signature;//签名
@property (nonatomic,copy) NSString * focusNum;//关注数量
@property (nonatomic,copy) NSString * fansNum;//粉丝数量
@property (nonatomic,copy) NSString * point;//积分
@property (nonatomic,copy) NSString * status;//状态
@property (nonatomic,copy) NSString * sharesLength;//分享个数
@property (nonatomic,copy) NSArray * shares;//用户收藏分享列表
@property (nonatomic,copy) NSString * token;//token
@property (nonatomic,copy) NSString * isFollow;//token
@end
